import React from 'react';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import PeopleOutlineRoundedIcon from '@material-ui/icons/PeopleOutlineRounded';
import RoomServiceRoundedIcon from '@material-ui/icons/RoomServiceRounded';
import MeetingRoomRoundedIcon from '@material-ui/icons/MeetingRoomRounded';
import StarRoundedIcon from '@material-ui/icons/StarRounded';
import RedeemRoundedIcon from '@material-ui/icons/RedeemRounded';
import LocalOfferRoundedIcon from '@material-ui/icons/LocalOfferRounded';
import AssessmentIcon from '@material-ui/icons/Assessment';
import Card from '@material-ui/core/Card';
import PersonIcon from '@material-ui/icons/Person';
import { useRouter } from 'next/router'
import { useSession } from 'next-auth/client'

function MainMenu() {
    const [ session] = useSession()
    const [value, setValue] = React.useState();
    const router = useRouter()
    const {tab} = router.query

    return (
        <Card variant="outlined">
        <Paper square>
          <Tabs
            value={parseInt(tab ? tab : '0')}
            textColor="primary"
            indicatorColor="primary"
            aria-label="disabled tabs example"
          >
            <a href="/profile" className="hover:text-indigo-600"><Tab icon={<PersonIcon />} label={"welcome, " + session?.user?.fullName} className="text-indigo-600"/></a>
            <a href="/account?tab=1" className="hover:text-indigo-600"><Tab label="Account" icon={<PeopleOutlineRoundedIcon />}/></a>
            <a href="/booking?tab=2" className="hover:text-indigo-600"><Tab label="Booking" icon={<RoomServiceRoundedIcon />}/></a>
            <a href="/room?tab=3" className="hover:text-indigo-600"><Tab label="Room" icon={<MeetingRoomRoundedIcon />} /></a>
            <a href="/combo?tab=4" className="hover:text-indigo-600"><Tab label="Combo" icon={<LocalOfferRoundedIcon />}/></a>
            <a href="/promotion?tab=5" className="hover:text-indigo-600"><Tab label="Promotion" icon={<RedeemRoundedIcon />}/></a>
            <a href="/event?tab=6" className="hover:text-indigo-600"><Tab label="Event" icon={<StarRoundedIcon />} /></a>
            {/* <a href="/report?tab=7" className="hover:text-indigo-600"><Tab label="Report" icon={<AssessmentIcon />} /></a> */}
          </Tabs>
        </Paper>
        </Card>
      );

}

export default MainMenu