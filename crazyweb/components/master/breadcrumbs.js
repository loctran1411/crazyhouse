import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import NavigateNextIcon from '@material-ui/icons/NavigateNext'

function BreadCrumb({data}){
    return (
        <div className="py-2 ml-4">
            <Breadcrumbs separator="›" aria-label="breadcrumb">
                <Link color="inherit" href="/">
                Home
                </Link>
                {data && <Typography color="textPrimary">{data}</Typography>}
            </Breadcrumbs>
        </div>
    )
}

export default BreadCrumb