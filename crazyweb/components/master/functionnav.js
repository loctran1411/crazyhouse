import React from 'react';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
    root: {
      backgroundColor: "transparent",
      display: 'flex',
      justifyContent: 'center'
    },
  });

function FunctionNav({children}) {

    const classes = useStyles();
    

    return (
        <div className="sticky bottom-0 left-1 bg-gray-100 border">
            {children}
        </div>
      );
}

export default FunctionNav