import { useState } from "react";
import PetsRoundedIcon from '@material-ui/icons/PetsRounded'
import { signOut } from 'next-auth/client'
import Link from '@material-ui/core/Link';
import { Button } from '@material-ui/core';



function Navbar() {
  // let [isShowMenu, setIsShowMenu] = useState(false);

  const logoutHandle = (e) => {
    e.preventDefault()
    signOut()
  }

  return (
    <nav class="flex items-center justify-between flex-wrap bg-green-500 px-6 py-1">
      <Link href="/" color="inherit" underline="none">
        <div class="flex items-center flex-shrink-0 text-white mr-6">
          <PetsRoundedIcon className="mr-2"/>
          <span class="font-semibold text-xl tracking-tight">Crazy House</span>
        </div>
      </Link>
      <div class="block lg:hidden">
        <button class="flex items-center px-3 py-2 border rounded text-green-200 border-teal-400 hover:text-white hover:border-white">
          <svg
            class="fill-current h-3 w-3"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <title>Menu</title>
            <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
          </svg>
        </button>
      </div>
      {/* <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
        <div class="flex-grow"></div> */}
        {/* <div class="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0"> */}
          {/* <button
            class="p-1 border-2 border-transparent text-white rounded-full hover:text-white focus:outline-none focus:text-white focus:bg-gray-700 transition duration-150 ease-in-out"
            aria-label="Notifications"
          >
            <svg
              class="h-6 w-6"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
              />
            </svg>
          </button> */}

          {/* <div class="ml-3 relative"> */}
            {/* <div> */}
            {/* <Button color="white"  onClick={(e) => logoutHandle(e)}>Sign out</Button> */}
            {/* <a
                    href="#"
                    className="block px-4 py-2 text-md leading-5 text-gray-700  hover:text-white"
                    role="menuitem"
                    onClick={(e) => logoutHandle(e)}
                  >
                  Sign out  
            </a> */}
              {/* <button
                class="flex text-sm border-2 border-transparent rounded-full hover:border-white focus:outline-none focus:border-white transition duration-150 ease-in-out"
                id="user-menu"
                onFocus={() => setIsShowMenu(true)}
                // onBlur={() => setIsShowMenu(false)}
                aria-label="User menu"
                aria-haspopup="true"
              >
                <img
                  class="h-8 w-8 rounded-full"
                  src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                  alt=""
                />
              </button> */}
            {/* </div> */}

            {/* {isShowMenu && (
              <div class="origin-top-right absolute right-0  w-48 rounded-md shadow-lg z-10">
                <div
                  class="py-1 rounded-md bg-white shadow-xs"
                  role="menu"
                  aria-orientation="vertical"
                  aria-labelledby="user-menu"
                >
                  <a
                    href="#"
                    class="block px-4 py-2 text-sm leading-5 text-gray-700 hover:text-white hover:bg-green-300 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out"
                    role="menuitem"
                  >
                    Your Profile
                  </a>
                  <a
                    href="#"
                    class="block px-4 py-2 text-sm leading-5 text-gray-700  hover:text-white hover:bg-green-300 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out"
                    role="menuitem"
                  >
                    Settings
                  </a>
                  <a
                    href="#"
                    className="block px-4 py-2 text-sm leading-5 text-gray-700  hover:text-white hover:bg-green-300 focus:outline-none focus:bg-gray-100 transition duration-150 ease-in-out"
                    role="menuitem"
                    onClick={(e) => logoutHandle(e)}
                  >
                    Sign out
                  </a>
                </div>
              </div>
            )} */}
          {/* </div> */}
        {/* </div>
      </div> */}
    </nav>
  );
}

export default Navbar;