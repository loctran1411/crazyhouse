import styles from '../../styles/Home.module.css'
import PetsRoundedIcon from '@material-ui/icons/PetsRounded'

function Footer() {
    return (
      <footer className={styles.footer}>
          <div className="bg-green-200 w-full h-full">
            <a
            href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
            target="_blank"
            rel="noopener noreferrer"
            >
            Powered by{' '}
            <PetsRoundedIcon className="ml-2"/>
            </a>
        </div>
      </footer>
    )
}

export default Footer