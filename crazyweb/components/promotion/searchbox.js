import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import { useSession } from 'next-auth/client'
import EnhancedTable from './table'
import axios from 'axios'

const useStyles = makeStyles((theme) => ({
    root: {
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 400,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
  }));

export default function SearchBox(){
    const [keyword, setKeyword] = React.useState('');
    const classes = useStyles()
    const [ session, loading ] = useSession()
    const [rows,SetRows] = React.useState([]);

    const onSearch = (e) => {
      if(keyword){
          const options = {
            method: 'get',
            headers: { 'Authorization': session.accessToken},
            url: 'http://localhost:8080/api/promotion/search' + "?keyword="  + keyword
          };
        axios(options)
        .then(function (response) {
          console.log(response.data)
          SetRows(response.data)
          })
          .catch(function (error) {
            console.log(error);
          });
           
      }else{
        rows = {}
      }
    }
  return (
    <div>
      <Paper component="form" className={classes.root}>
        <IconButton className={classes.iconButton} aria-label="menu">
          <MenuIcon />
        </IconButton>
        <InputBase
          className={classes.input}
          placeholder="Search Promotion"
          inputProps={{ 'aria-label': 'search Promotion' }}
          onChange={(e)=>setKeyword(e.target.value)}
        />
        <Divider className={classes.divider} orientation="vertical" />
        <IconButton className={classes.iconButton} aria-label="search" onClick={(e)=>onSearch(e)}>
          <SearchIcon />
        </IconButton>
      </Paper>
      <div>
        { rows && <EnhancedTable rows={rows}/> }
      </div>
    </div>
  );
}