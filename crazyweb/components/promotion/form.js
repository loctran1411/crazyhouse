import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { useSession } from 'next-auth/client'
import RefreshIcon from '@material-ui/icons/Refresh';
import Alert from '@material-ui/lab/Alert'
import axios from 'axios'

const rooms = [
    {
      value: '1',
      label: 'Phòng đập phá lớn',
    },
    {
      value: '2',
      label: 'Phòng đấm bốc',
    },
    {
      value: '3',
      label: 'Phòng phi tiêu',
    },
    {
      value: '4',
      label: 'Phòng đập phá nhỏ',
    },
    {
      value: '5',
      label: 'Phòng tư vấn',
    },
    {
      value: '6',
      label: 'Phòng game',
    },
  ];

const discounts = [
    {
      value: '1',
      label: '25%',
    },
    {
      value: '2',
      label: '50%',
    },
    {
      value: '3',
      label: '75%',
    }
  ];

export default function DataForm({promotion,setPromotion,isEdit,setEdit,refresh}){
    const [ session, loading ] = useSession()
    const curr = new Date();
    const beginDate = curr.toISOString().substr(0,10)
    let endDate = new Date();
    endDate.setDate(curr.getDate() + 3)
    endDate = endDate.toISOString().substr(0,10)

    const [begin, setBeginDate] = React.useState(beginDate);
    const [end, setEndDate] = React.useState(endDate);
    const [message, setMessage] = React.useState('');
    const [error, setError] = React.useState(false);
    
    const handleCreate = (e) => {
      e.preventDefault()
      const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data: {
            ...promotion,
          },
          baseURL: 'http://localhost:8080/api/',
          url: 'promotion/new'
        };
      axios(options)
      .then(function (response) {
        if(response.data == 0){
          setError(true)
          setMessage("Enddate must to greater than begindate");
        }else{
          setError(false)
          setMessage("Created successfully!");
          refresh()
          setEdit(false)
        }
      })
        .catch(function (error) {
          setError(true)
          setMessage("Error")
      });
      
    }

    const handleUpdate = (e) => {
      e.preventDefault()
      console.log(promotion)
      const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data: {
            id:promotion.id,
            ...promotion,
          },
          baseURL: 'http://localhost:8080/api/',
          url: 'promotion/new'
        };

      axios(options)
      .then(function (response) {
        if(response.data == 0){
          setError(true)
          setMessage("Enddate must to greater than begindate");
        }else{
          setError(false)
          setMessage("Updated successfully!");
          refresh()
          setEdit(false)
        }
      })
      .catch(function (error) {
          setError(true)
          setMessage("Error")
      });
    }

    const handleRefresh = (e) => {
      e.preventDefault()
      setEdit(false)
      refresh()
    }

    return(
        <div>
          {message && <Alert color={error ? "error" : "success"} severity={error ? "error" : "success"} className="mb-4">{message}</Alert>}
          <form  onSubmit={(e) => { isEdit ? handleUpdate(e): handleCreate(e)}} autoComplete="off">
          <Grid container spacing={3}>
            <Grid item xs={6}>
                <TextField
                    label="Promotion name"
                    id="name"
                    defaultValue="name"
                    variant="outlined"
                    value={promotion?.promotion?.name}
                    onChange={(e) => {
                      // const regex = /^([a-zA-Z0-9]){1,250}$/i;
                      // if (e.target.value === '' || regex.test(e.target.value)) {
                        setPromotion({
                          ...promotion,
                          promotion:{
                            ...promotion?.promotion,
                            name:e.target.value
                          }
                        })
                          
                      // }
                    }}
                    fullWidth
                    required
                />
            </Grid>   
            <Grid item xs={3}>
                <TextField
                    id="beginDate"
                    label="Begin date"
                    type="date"
                    defaultValue={promotion?.promotion?.beginDate}
                    onChange={(e) => {
                      setPromotion({
                        ...promotion,
                        promotion:{
                          ...promotion?.promotion,
                          beginDate:e.target.value
                        }
                      })
                    }}
                    InputLabelProps={{
                    shrink: true,
                    }}
                    fullWidth
                    required
                />
            </Grid>
            <Grid item xs={3}>
                <TextField
                    id="endDate"
                    label="End date"
                    type="date"
                    defaultValue={promotion?.promotion?.endDate}
                    onChange={(e) => {
                      setPromotion({
                        ...promotion,
                        promotion:{
                          ...promotion?.promotion,
                          endDate:e.target.value
                        }
                      })
                    }}
                    InputLabelProps={{
                    shrink: true,
                    }}
                    fullWidth
                    required
                />
            </Grid>
            <Grid item xs={6}>
                <TextField
                id="room"
                select
                label="room"
                value={promotion?.room?.id}
                onChange={(e) => {
                  setPromotion({
                    ...promotion,
                    room:{
                      ...promotion?.room,
                      id:e.target.value
                    }
                  })
                }}
                // helperText="Please select your room"
                fullWidth
                SelectProps={{
                    native: true,
                  }}
                >
                {rooms.map((option) => (
                    <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
                </TextField>
            </Grid>
            <Grid item xs={3}>
                <TextField
                    id="discount"
                    select
                    label="Discount"
                    value={promotion?.discount}
                    onChange={(e) => {
                      setPromotion({
                        ...promotion,
                        discount:e.target.value
                      })
                    }}
                    // helperText="Please select your room"
                    fullWidth
                    SelectProps={{
                        native: true,
                    }}
                    >
                    {discounts.map((option) => (
                        <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                    ))}
                </TextField>
            </Grid>
                
            <Grid item xs={12}>
                <TextField
                    id="desc"
                    label="Description"
                    multiline
                    rows={10}
                    value={promotion?.promotion?.description}
                    onChange={(e) => {
                      setPromotion({
                        ...promotion,
                        promotion:{
                          ...promotion?.promotion,
                          description:e.target.value
                        }
                      })
                    }}
                    variant="outlined"
                    fullWidth
                />
            </Grid>     
          </Grid>
         
          <div class="flex items-center mt-2">
                {!isEdit && <button id="submit"
                          class="mr-2 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                          // onClick={(e) => handleCreate(e)}
                          type="submit">
                          <i class="fab fa-whatsapp"></i> Create new
                      </button>
                }
                {isEdit && 
                  <>
                      
                      <button id="submit"
                        class="mr-4 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        // onClick={(e) => handleUpdate(e)}
                        type="submit">
                        Update
                      </button>

                      <button id="submit"
                        // class="ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={(e) => handleRefresh(e)}
                        type="submit">
                        <RefreshIcon className="text-blue-500"/>
                      </button>
                  </>
                }
          </div>
            </form>
        </div>
    );
}