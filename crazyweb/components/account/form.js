import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { useSession } from 'next-auth/client'
import Alert from '@material-ui/lab/Alert'
import RefreshIcon from '@material-ui/icons/Refresh';
import axios from 'axios'

const roles = [
    {
      value: '1',
      label: 'admin',
    },
    {
      value: '2',
      label: 'Customer',
    },
  ];

  const lststatus = [
    {
      value: '1',
      label: 'active',
    },
    {
      value: '2',
      label: 'inactive',
    },
  ];

export default function DataForm({account,setAccount,isEdit,setEdit,refresh}){
    const [ session, loading ] = useSession()
    const[confirmPwd, setConfirmPwd] = React.useState('');
    const [message, setMessage] = React.useState('');
    const [error, setError] = React.useState(false);
    const [matchErr,SetMatchErr] = React.useState(false)
 
    const validFields = () => {

      if(account?.password != confirmPwd){
          SetMatchErr(true)
          return true
      }else{
          SetMatchErr(false)
      }
      return false
  }
    
    const handleCreate = (e) => {
      e.preventDefault()
      if(!validFields()){
        const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data:{
            ...account
          },
          baseURL: 'http://localhost:8080/api/',
          url: 'user/new'
        };
        console.log(options)

      axios(options)
      .then(function (response) {
          setError(false)
          setMessage("Created successfully !")
          refresh()
        })
        .catch(function (error) {
          setError(true)
          setMessage("Error")
        });

      }
    }
    const handleUpdate = (e) => {
      e.preventDefault()

      const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data:{
            ...account,
          },
          baseURL: 'http://localhost:8080/api/',
          url: 'user/new'
        };

      axios(options)
      .then(function (response) {
        setError(false)
        setMessage("Updated successfully !")
        refresh()
        setConfirmPwd('')
      })
      .catch(function (error) {
          setError(true)
          setMessage("Error")
      });
    }

    const handleRefresh = (e) => {
      e.preventDefault()
      setEdit(false)
      refresh()
      setConfirmPwd('')
    }

    return(
        <div className="mx-auto w-auto">
          {message && <Alert color={error ? "error" : "success"} severity={error ? "error" : "success"} className="pb-2">{message}</Alert>}
          <form onSubmit={(e) => { isEdit ? handleUpdate(e): handleCreate(e)}}  autoComplete="off">
          <Grid container spacing={3}>
            <Grid item xs={3}>
            <TextField
                    label="Last Name"
                    id="lastname"
                    variant="outlined"
                    value={account?.info?.lastName}
                    onChange={(e) => {
                      setAccount({
                        ...account,
                        info:{
                          ...account?.info,
                          lastName:e.target.value
                        }
                      })
                    }}
                    autoFocus
                    fullWidth
                    required
                />
            </Grid>
                
            <Grid item xs={3}>
            <TextField
                    label="First Name"
                    id="firstname"
                    variant="outlined"
                    value={account?.info?.firstName}
                    onChange={(e) => {
                      setAccount({
                        ...account,
                        info:{
                          ...account?.info,
                          firstName:e.target.value
                        }
                      })
                    }}
                    fullWidth
                    required
                />
            </Grid>
            <br/>
            <Grid item xs={3}></Grid>
            <Grid item xs={6}>
            <TextField
                    // error ={validateEmail()}
                  
                    label="email"
                    id="email"
                    variant="outlined"
                    value={account?.email}
                    onChange={(e) => {
                      setAccount({
                        ...account,
                        email:e.target.value
                      })
                    }}
                    type="email"
                    fullWidth
                    required
                />
            </Grid>
            <br/>
            <Grid item xs={6}></Grid>
            {!isEdit && <><Grid item xs={3}>
            <TextField
                    label="Password"
                    id="password"
                    variant="outlined"
                    type="password"
                    value={account?.password}
                    onChange={(e) => {
                      setAccount({
                        ...account,
                        password:e.target.value
                      })
                    }}
                    fullWidth
                    required
                />
            </Grid>
            <Grid item xs={3}>
            <TextField
              error={matchErr}
              label="Confirm Password"
              id="confirmpassword"
              variant="outlined"
              type="password"
              value={confirmPwd}
              helperText={matchErr ? "Confirm not match" : ""}
              onChange={(e) => setConfirmPwd(e.target.value)}
              fullWidth
              required
            />
            </Grid>
            <br/>
            <Grid item xs={6}></Grid></>}
            <Grid item xs={3}>
                <TextField
                    id="role"
                    select
                    label="Role"
                    value={account?.role?.id}
                    onChange={(e) => {
                      setAccount({
                        ...account,
                        role:{
                          ...account?.role,
                          id:e.target.value
                        }
                      })
                    }}
                    // helperText="Please select your room"
                    fullWidth
                    SelectProps={{
                        native: true,
                    }}
                    >
                    {roles.map((option) => (
                        <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                    ))}
                </TextField>
            </Grid>
            <Grid item xs={3}>
                <TextField
                    id="status"
                    select
                    label="Status"
                    value={account?.status}
                    onChange={(e) => {
                      setAccount({
                        ...account,
                        status:{
                          ...account?.role,
                          status:e.target.value
                        }
                      })
                    }}
                    // helperText="Please select your room"
                    fullWidth
                    SelectProps={{
                        native: true,
                    }}
                    >
                    {lststatus.map((option) => (
                        <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                    ))}
                </TextField>
            </Grid>
            
            
          </Grid>
         <br/>
         <div class="flex items-center mt-2">
                {!isEdit && <button id="submit"
                          class="mr-2 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                          // onClick={(e) => handleCreate(e)}
                          type="submit">
                          <i class="fab fa-whatsapp"></i> Create new
                      </button>
                }
                {isEdit && 
                  <>
                      
                      <button id="submit"
                        class="mr-4 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        // onClick={(e) => handleUpdate(e)}
                        type="submit">
                        Update
                      </button>

                      <button id="submit"
                        // class="ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={(e) => handleRefresh(e)}
                        type="submit">
                        <RefreshIcon className="text-blue-500"/>
                      </button>
                  </>
                }
          </div>
            </form>
        </div>
    );
}