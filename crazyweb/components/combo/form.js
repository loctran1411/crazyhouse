import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { useSession } from 'next-auth/client'
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import Alert from '@material-ui/lab/Alert'
import RefreshIcon from '@material-ui/icons/Refresh';
import axios from 'axios'

const rooms = [
    {
      value: '1',
      label: 'Phòng đập phá lớn',
    },
    {
      value: '2',
      label: 'Phòng đấm bốc',
    },
    {
      value: '3',
      label: 'Phòng phi tiêu',
    },
    {
      value: '4',
      label: 'Phòng đập phá nhỏ',
    },
    {
      value: '5',
      label: 'Phòng tư vấn',
    },
    {
      value: '6',
      label: 'Phòng game',
    },
  ];


export default function DataForm({combo,setCombo,isEdit,setEdit,refresh}){
    const [ session, loading ] = useSession()
    const [status, setStatus] = React.useState('');
    const [error, setError] = React.useState(false);

    const handleCreate = (e) => {
      e.preventDefault()
      const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data: {
            ...combo,
            room: {"id": combo.room}
          },
          baseURL: 'http://localhost:8080/api/',
          url: 'combo/new'
        };
      console.log(options)
      axios(options)
      .then(function (response) {
        setError(false)
        setStatus("Created successfully !")
        refresh()
      })
      .catch(function (error) {
          setError(true)
          setStatus("Error")
      });
      
    }

    const handleUpdate = (e) => {
      e.preventDefault()
      e.preventDefault()
      const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data: combo,
          baseURL: 'http://localhost:8080/api/',
          url: 'combo/new'
        };

      axios(options)
      .then(function (response) {
        setError(false)
        setStatus("Update successfully!")
      })
      .catch(function (error) {
          setError(true)
          setStatus("Error")
      });
    }

    const handleRefresh = (e) => {
      e.preventDefault()
      setEdit(false)
      refresh()
    }

    return(
        <div>
           {status && <Alert color={error ? "error" : "success"} severity={error ? "error" : "success"} className="mb-1">{status}</Alert>}
          <form  onSubmit={(e) => { isEdit ? handleUpdate(e): handleCreate(e)}} autoComplete="off">
          <Grid container spacing={3}>
            <Grid item xs={6}>
                <TextField
                    label="Combo name"
                    id="name"
                    variant="outlined"
                    value={combo.comboName}
                    onChange={(e) => {
                      const regex = /^([a-zA-Z0-9]){0,150}$/i;
                      if (!e.target.value === '' || regex.test(e.target.value)) {
                        setCombo({
                          ...combo,
                          comboName:e.target.value
                        })
                      }
                    }}
                    fullWidth
                    autoFocus
                    required
                />
            </Grid>
            <Grid item xs={6} />
            <Grid item xs={6}>
                <TextField
                id="room"
                select
                label="room"
                value={combo.room}
                onChange={(e) => {
                  setCombo({
                    ...combo,
                    room:e.target.value
                  })
                }}
                // helperText="Please select your room"
                fullWidth
                SelectProps={{
                    native: true,
                  }}
                >
                {rooms.map((option) => (
                    <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
                </TextField>
            </Grid>
            <Grid item xs={6} />      
            <Grid item xs={6}>
                <FormControl fullWidth >
                  <InputLabel htmlFor="standard-adornment-amount">Price</InputLabel>
                  <Input
                    id="standard-adornment-amount"
                    value={combo.price}
                    onChange={(e) => {
                      // const regex = /^([0-9]){1,10}$/i;
                      // if (e.target.value === '' || regex.test(e.target.value)) {
                        setCombo({
                          ...combo,
                          price:e.target.value
                        })
                      // }
                    }}
                    startAdornment={<InputAdornment position="start">$</InputAdornment>}
                    type="number"
                    inputProps={{
                      min:1
                    }}
                  />
                </FormControl>
            </Grid>        
                
            <Grid item xs={12}>
                <TextField
                    id="desc"
                    label="Description"
                    multiline
                    rows={10}
                    onChange={(e) => {
                      setCombo({
                        ...combo,
                        description:e.target.value
                      })
                    }}
                    defaultValue=""
                    value={combo.description}
                    variant="outlined"
                    fullWidth
                />
            </Grid>     
          </Grid>
         
          <div class="flex items-center mt-2">
                {!isEdit && <button id="submit"
                          class="mr-2 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                          // onClick={(e) => handleCreate(e)}
                          type="submit">
                          <i class="fab fa-whatsapp"></i> Create new
                      </button>
                }
                {isEdit && 
                  <>
                      
                      <button id="submit"
                        class="mr-4 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        // onClick={(e) => handleUpdate(e)}
                        type="submit">
                        Update
                      </button>

                      <button id="submit"
                        // class="ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={(e) => handleRefresh(e)}
                        type="submit">
                        <RefreshIcon className="text-blue-500"/>
                      </button>
                  </>
                }
          </div>
            </form>
        </div>
    );
}