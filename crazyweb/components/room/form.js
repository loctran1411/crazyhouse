import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { useSession } from 'next-auth/client'
import Alert from '@material-ui/lab/Alert'
import RefreshIcon from '@material-ui/icons/Refresh';
import axios from 'axios'

const rooms = [
    {
      value: '1',
      label: 'Phòng đập phá lớn',
    },
    {
      value: '2',
      label: 'Phòng đấm bốc',
    },
    {
      value: '3',
      label: 'Phòng phi tiêu',
    },
    {
      value: '4',
      label: 'Phòng đập phá nhỏ',
    },
    {
      value: '5',
      label: 'Phòng tư vấn',
    },
    {
      value: '6',
      label: 'Phòng game',
    },
  ];

const statusList = [
    {
      value: 0,
      label: 'active',
    },
    {
      value: 1,
      label: 'inactive',
    }
  ];

export default function DataForm({room,setRoom,isEdit,setEdit,refresh}){
    const [ session, loading ] = useSession()
    const [message, setMessage] = React.useState('');
    const [error, setError] = React.useState(false);
    
    // console.log(room)

    const handleCreate = (e) => {
      e.preventDefault()
      const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data:room,
          baseURL: 'http://localhost:8080/api/',
          url: 'room/new'
        };

      axios(options)
      .then(function (response) {
        setError(false)
        setMessage("Created successfully!")
        refresh()
        })
        .catch(function (error) {
          setError(true)
          setMessage("Error")
        });
      
    }

    const handleUpdate = (e) => {
      e.preventDefault()

      const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data: room,
          baseURL: 'http://localhost:8080/api/',
          url: 'room/new'
        };

      axios(options)
      .then(function (response) {
        setError(false)
        setMessage("Updated successfully !")
        refresh()
      })
      .catch(function (error) {
          setError(true)
          setMessage("Error")
      });
    }

    const handleRefresh = (e) => {
      e.preventDefault()
      setEdit(false)
      refresh()
      setMessage('')
    }

    return(
        <div className="w-96 mx-auto">
          {message && <Alert color={error ? "error" : "success"} severity={error ? "error" : "success"} className="pb-2">{message}</Alert>}
          <form onSubmit={(e) => { isEdit ? handleUpdate(e): handleCreate(e)}}   autoComplete="off">
          <Grid container spacing={3}>
            <Grid item xs={12}>
            <TextField
                    label="Room"
                    id="room"
                    variant="outlined"
                    value={room.name}
                    inputProps={{
                      maxlength:50,
                      // pattern:"^/w+$"
                    }}
                    onChange={(e) => {
                        setRoom({
                          ...room,
                          name:e.target.value
                        })
                      }
                    }
                    fullWidth
                    autoFocus
                    required
                />
            </Grid>
            <Grid item xs={12}>
            <TextField
                    label="Room Number"
                    id="room_number"
                    variant="outlined"
                    value={room.room_number}
                    type="number"
                    inputProps={{
                      pattern:"^[0-9]{3}$",
                      min:100
                    }}
                    onChange={(e) => {
                      // const regex = /^([0-9]){1,3}$/i;
                      // if (e.target.value === '' || regex.test(e.target.value)) {
                        setRoom({
                          ...room,
                          room_number:e.target.value
                        })
                      // }
                    }}
                    fullWidth
                    required
                />
            </Grid>
            
            
            <Grid item xs={12}>
            <TextField
                    label="Size"
                    id="size"
                    variant="outlined"
                    value={room.size}
                    inputProps={{
                      pattern:"^[0-9]{1,2}x[0-9]{1,2}$",
                      placeholder:"5x5"
                    }}
                    onChange={(e) => {
                
                      // if (e.target.value === '' || regex.test(e.target.value)) {
                        setRoom({
                          ...room,
                          size:e.target.value
                        })
                      // }
                    }}
                    fullWidth
                    required
                />
            </Grid>
            <Grid item xs={12}>
                <TextField
                    id="status"
                    select
                    label="Status"
                    value={room.status}
                    onChange={(e) => {
             
                      setRoom({
                        ...room,
                        status: e.target.value
                      })
                    }}
                    // helperText="Please select your room"
                    fullWidth
                    SelectProps={{
                        native: true,
                    }}
                    >
                    {statusList.map((option) => (
                        <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                    ))}
                </TextField>
            </Grid>
                 
          </Grid>
         
          <div class="flex items-center mt-2">
                {!isEdit && <button id="create"
                          class="mr-2 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                          // onSubmit={(e) => handleCreate(e)}
                          // onSubmit={(e) => {e.preventDefault() ;alert("cc"); return false}}
                          type="submit">
                          <i class="fab fa-whatsapp"></i> Create new
                      </button>
                }
                {isEdit && 
                  <>
                      
                      <button id="update"
                        class="mr-4 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        // onClick={(e) => handleUpdate(e)}
                        type="submit">
                        Update
                      </button>

                      <button id="refresh"
                        // class="ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={(e) => handleRefresh(e)}
                        type="submit">
                        <RefreshIcon className="text-blue-500"/>
                      </button>
                  </>
                }
          </div>
            </form>
        </div>
    );
}