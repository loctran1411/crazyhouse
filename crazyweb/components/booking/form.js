import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { useSession } from 'next-auth/client'
import RefreshIcon from '@material-ui/icons/Refresh';
import Alert from '@material-ui/lab/Alert'
import {getDateAsString} from '../../pages/common/datetimeutils'
import axios from 'axios'
import useSWR from 'swr'
const rooms = [
    {
      value: '1',
      label: 'Phòng đập phá lớn',
    },
    {
      value: '2',
      label: 'Phòng đấm bốc',
    },
    {
      value: '3',
      label: 'Phòng phi tiêu',
    },
    {
      value: '4',
      label: 'Phòng đập phá nhỏ',
    },
    {
      value: '5',
      label: 'Phòng tư vấn',
    },
    {
      value: '6',
      label: 'Phòng game',
    },
  ];

const hours = [
    {
      value: '1',
      label: '1 hour',
    },
    {
      value: '2',
      label: '2 hours',
    },
    {
      value: '3',
      label: '3 hours',
    },
    {
      value: '4',
      label: '4 hours',
    },
    {
      value: '5',
      label: '5 hours',
    }
  ];

  // const combos = [
  //   {
  //     value: '1',
  //     label: 'normal',
  //   },
  // ];

const bookstatus = [
    {
      value: '0',
      label: 'booking',
    },
    {
      value: '1',
      label: 'finished',
    },
    {
      value: '2',
      label: 'canceled',
    },
  ];

export default function DataForm({booking,setBooking,isEdit,setEdit,refresh}){
    const [ session, loading ] = useSession()
    const [message, setMessage] = React.useState('');
    const [roomid,SetRoomId] = React.useState(1)
    const [error, setError] = React.useState(false);
    let { data: comboss,error: err } = useSWR(['http://localhost:8080/api/combo/combos/' + roomid, session.accessToken])
    // console.log(comboss.length)
    const handleCreate = (e) => {
      e.preventDefault()
      const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data:{
              "booking":{
                note:booking?.note,
                "createdBy":{
                  id:session?.user?.userid
                },
                "customer":{
                  id:parseInt(booking?.booking?.customer?.id)
                }
              },
              "room":{
                id:roomid
              },
              "combo":{
                "comboId":booking?.combo?.id
              },
              "bookingDate":booking?.bookingDate,
              "hours":booking?.hours,
              "note":booking?.note
            },
          baseURL: 'http://localhost:8080/api/',
          url: 'booking/new'
        };

        axios(options)
        .then(function (response) {
          if(response.data == 0){
            setError(true)
            setMessage("Booking date must to greater than now");
          }else{
            setError(false)
            setMessage("Created successfully!");
            refresh()
            setEdit(false)
          }
        })
        .catch(function (error) {
          setError(true)
          setMessage("Error")
        });
      
    }

    const handleUpdate = (e) => {
      e.preventDefault()

      const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data: { 
            ...booking,
            "room":{
              id:roomid
            }
          },
          baseURL: 'http://localhost:8080/api/',
          url: 'booking/new'
        };

      axios(options)
      .then(function (response) {
        if(response.data == 0){
          setError(true)
          setMessage("Booking date must to greater than now");
        }else{
          setError(false)
          setMessage("Updated successfully!");
          refresh()
          setEdit(false)
        }
      })
      .catch(function (error) {
          setError(true)
          setMessage("Error")
      });
    }

    const handleRefresh = (e) => {
      e.preventDefault()
      setEdit(false)
      refresh()
    }

    return(
        <div>
          {message && <Alert color={error ? "error" : "success"} severity={error ? "error" : "success"} className="mb-4">{message}</Alert>}
          <form  onSubmit={(e) => { isEdit ? handleUpdate(e): handleCreate(e)}} autoComplete="off">
          <Grid container spacing={3}>
            <Grid item xs={6}>
                <TextField
                    label="Customer id"
                    id="customer"
                    variant="outlined"
                    type="number"
                    value={booking?.booking?.customer?.id}
                    inputProps={{
                      min:1
                    }}
                    onChange={(e) => {
                      // const regex = /^([0-9]){1,6}$/i;
                      // if (e.target.value === '' || regex.test(e.target.value)) {
                        setBooking({
                          ...booking,
                          booking:{
                            ...booking?.booking,
                            customer:{
                              id:e.target.value
                            }
                          }
                        })
                      // }
                    }}
                    fullWidth
                    autoFocus
                    required
                />
            </Grid>   
            <Grid item xs={3}>
                <TextField
                    id="bookingDate"
                    label="Booking date"
                    type="datetime-local"
                    defaultValue={isEdit ? getDateAsString(booking?.bookingDate) : booking?.bookingDate}
                    onChange={(e) => {
                      setBooking({
                        ...booking,
                        bookingDate:e.target.value
                      })
                    }}
                    InputLabelProps={{
                    shrink: true,
                    }}
                    fullWidth
                    required
                />
            </Grid>
            <Grid item xs={6}>
                <TextField
                id="room"
                select
                label="room"
                value={roomid}
                onChange={(e) => {
                  SetRoomId(e.target.value)
                  // setBooking({
                  //   ...booking,
                  //   room:{
                  //     id:e.target.value
                  //   }
                  // })
                }}
                // helperText="Please select your room"
                fullWidth
                SelectProps={{
                    native: true,
                  }}
                >
                {rooms.map((option) => (
                    <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
                </TextField>
            </Grid>
            <Grid item xs={3}>
                <TextField
                    id="combo"
                    select
                    label="Select combo"
                    value={booking?.combo?.id}
                    onChange={(e) => {
                      setBooking({
                        ...booking,
                        combo:{
                          id:e.target.value
                        }
                      })
                    }}
                    // helperText="Please select your room"
                    fullWidth
                    SelectProps={{
                        native: true,
                    }}
                    >
                    {comboss?.map((option) => (
                        <option key={option.comboId} value={option.comboId}>
                        {option.comboName}
                    </option>
                    ))}
                </TextField>
            </Grid>
            <Grid item xs={3}></Grid>          
            <Grid item xs={3}>
                <TextField
                    id="hour"
                    select
                    label="Hours"
                    value={booking?.hours}
                    onChange={(e) => {
                      setBooking({
                        ...booking,
                        hours:e.target.value
                      })
                    }}
                    // type="number"
                    // inputProps={{
                    //   max:3
                    //   min:1
                    // }}
                    fullWidth
                    required
                    SelectProps={{
                        native: true,
                    }}
                    >
                    {hours.map((option) => (
                        <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                    ))}
                    
                </TextField>
            </Grid>
            {isEdit && 
            <>
            <Grid item xs={9}></Grid>
            <Grid item xs={3}>
            <TextField
                id="status"
                select
                label="status"
                value={booking?.status}
                onChange={(e) => {
                  setBooking({
                    ...booking,
                    status:e.target.value
                  })
                }}
                // helperText="Please select your room"
                fullWidth
                SelectProps={{
                    native: true,
                  }}
                >
                {bookstatus.map((option) => (
                    <option key={option.value} value={option.value}>
                    {option.label}
                  </option>
                ))}
                </TextField>
            </Grid></>}
                
            <Grid item xs={12}>
                <TextField
                    id="desc"
                    label="Note"
                    multiline
                    rows={5}
                    onChange={(e) => {
                      setBooking({
                        ...booking,
                        note:e.target.value
                      })
                    }}
                    value={booking?.note}
                    variant="outlined"
                    fullWidth
                />
            </Grid>     
          </Grid>
         
          <div class="flex items-center mt-2">
                {!isEdit && <button id="submit"
                          class="mr-2 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                          // onClick={(e) => handleCreate(e)}
                          type="submit">
                          <i class="fab fa-whatsapp"></i> Create new
                      </button>
                }
                {isEdit && 
                  <>
                      
                      <button id="submit"
                        class="mr-4 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        // onClick={(e) => handleUpdate(e)}
                        type="submit">
                        Update
                      </button>

                      <button id="submit"
                        // class="ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={(e) => handleRefresh(e)}
                        type="submit">
                        <RefreshIcon className="text-blue-500"/>
                      </button>
                  </>
                }
          </div>
            </form>
        </div>
    );
}