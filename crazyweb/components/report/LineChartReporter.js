import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const data = [
    {
      name: 'T1',
      uv: 4000,
      pv: 2400,
      amt: 2400,
    },
    {
      name: 'T2',
      uv: 3000,
      pv: 1398,
      amt: 2210,
    },
    {
      name: 'T3',
      uv: 2000,
      pv: 9800,
      amt: 2290,
    },
    {
      name: 'T4',
      uv: 2780,
      pv: 3908,
      amt: 2000,
    },
    {
      name: 'T5',
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: 'T6',
      uv: 2390,
      pv: 3800,
      amt: 2500,
    },
    {
      name: 'T7',
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
      name: 'T8',
      uv: 1890,
      pv: 4800,
      amt: 2181,
    },
    {
      name: 'T9',
      uv: 2390,
      pv: 3800,
      amt: 2500,
    },
    {
      name: 'T10',
      uv: 3490,
      pv: 4300,
      amt: 2100,
    },
    {
        name: 'T11',
        uv: 2390,
        pv: 3800,
        amt: 2500,
      },
      {
        name: 'T12',
        uv: 3490,
        pv: 4300,
        amt: 2100,
      },
  ];

export default function LineChartReporter({data}){
    
    return (
        <ResponsiveContainer width={500} height={400}>
            <LineChart
            width={300}
            height={300}
            data={data}
            margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
            }}
            >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="total"/>
            <YAxis dataKey= {2021} />
            <Tooltip />
            <Legend />
            {data.map((room)=> <Line type="monotone" dataKey={'room ' + room.roomId} stroke="#82ca9d" />)}
            {/* <Line type="monotone" dataKey="roomId" nameKey="psda" stroke="#8884d8" activeDot={{ r: 8 }} /> */}
            
            </LineChart>
        </ResponsiveContainer>              
    )
}