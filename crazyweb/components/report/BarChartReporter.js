import React, { PureComponent } from 'react';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,LabelList } from 'recharts';

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
const renderCustomizedLabel = (props) => {
  const { x, y, width, height, value } = props;
  const radius = 10;

  return (
    <g>
      {/* <circle cx={x + width / 2} cy={y - radius} r={radius} fill="#8884d8" /> */}
      <text x={x + width / 2} y={y - radius} textAnchor="middle" dominantBaseline="middle" className="text-xs antialiased font-semibold">
        {numberWithCommas(value)}
      </text>
    </g>
  );
};
export default function BarChartReporter({data, color, title}){
    return (
        // <ResponsiveContainer width={500} height={400}>
          
          <BarChart
            width={500}
            height={300}
            data={data}
            margin={{ top: 15, right: 30, left: 20, bottom: 5 }}
            barSize={20}
            barGap={8}
            // barCategoryGap="50%"
          >
            {/* <CartesianGrid strokeDasharray="3 3" x={5}/> */}
            <XAxis dataKey="name" scale="point" padding={{ left: 30, right: 30 }} label>
              {/* <Label value="Pages of my website" offset={0} position="insideBottom" /> */}
            </XAxis>
            <YAxis />
            <Tooltip />
            <Legend content={title} verticalAlign="top" height={36} iconType="diamond"/>
            <CartesianGrid strokeDasharray="3 3" />
            <Bar dataKey="value" fill={color} background={{ fill: '#eee' }} >
              <LabelList dataKey="value" position="top" content={renderCustomizedLabel}/>
            </Bar>
          </BarChart>
        // </ResponsiveContainer>
      );
}