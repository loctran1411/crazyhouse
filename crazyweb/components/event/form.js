import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { useSession } from 'next-auth/client'
import Alert from '@material-ui/lab/Alert'
import RefreshIcon from '@material-ui/icons/Refresh';
import {getDateAsString} from '../../pages/common/datetimeutils'
import axios from 'axios'

export default function DataForm({event,setEvent,isEdit,setEdit,refresh}){
    const curr = new Date();

    const [ session, loading ] = useSession()
    const [message, setMessage] = React.useState('');
    const [error, setError] = React.useState(false);
    const handleCreate = (e) => {
        e.preventDefault()
        const options = {
            method: 'POST',
            headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
            data:{
               ...event,
                "createdBy":{
                  "id": session.user.userid
                }
                },
            baseURL: 'http://localhost:8080/api/',
            url: 'event/new'
          };
        axios(options)
        .then(function (response) {
          if(response.data == 0){
            setError(true)
            setMessage("Enddate must to greater than begindate");
          }else{
            setError(false)
            setMessage("Created successfully!");
          }
        })
        .catch(function (error) {
          setError(true)
          setMessage("Error")
        });
    }

    const handleUpdate = (e) => {
      e.preventDefault()
      const options = {
          method: 'POST',
          headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
          data:{
            ...event,
             "createdBy":{
               "id": session.user.userid
             }
             },
          baseURL: 'http://localhost:8080/api/',
          url: 'event/new'
        };

      axios(options)
      .then(function (response) {
        if(response.data == 0){
          setError(true)
          setMessage("Enddate must to greater than begindate");
        }else{
          setError(false)
          setMessage("Updated successfully!");
          refresh()
        }
      })
      .catch(function (error) {
          setError(true)
          setMessage("Error")
      });
    }

    const handleRefresh = (e) => {
      e.preventDefault()
      setEdit(false)
      refresh()
    }

    return(
        <div>
          {message && <Alert color={error ? "error" : "success"} severity={error ? "error" : "success"} className="mb-4">{message}</Alert>}
          <form  onSubmit={(e) => { isEdit ? handleUpdate(e): handleCreate(e)}} autoComplete="off">
          <Grid container spacing={3}>
            <Grid item xs={6}>
                <TextField
                    label="Event name"
                    id="name"
                    variant="outlined"
                    value={event?.event_name}
                    inputProps={{
                      maxlength:200
                    }}
                    onChange={(e) => {
                      // const regex = /^([a-zA-Z0-9]){1,150}$/i;
                      // if (e.target.value === '' || regex.test(e.target.value)) {
                        setEvent({
                          ...event,
                          event_name:e.target.value
                        })
                      // }
                    }}
                    fullWidth
                    autoFocus
                    required
                />

            </Grid>   
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
                <TextField
                    id="beginDate"
                    label="Begin date"
                    type="datetime-local"
                    value={isEdit ? getDateAsString(event?.beginDate) : event?.beginDate}
                    onChange={(e) => {
                      setEvent({
                        ...event,
                        beginDate:e.target.value
                      })
                    }}
                    // className={classes.textField}
                    InputLabelProps={{
                    shrink: true,
                    }}
                    fullWidth
                    required
                />
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
                <TextField
                    id="endDate"
                    label="End date"
                    type="datetime-local"
                    value={isEdit ? getDateAsString(event?.endDate) : event?.endDate}
                    onChange={(e) => {
                      setEvent({
                        ...event,
                        endDate:e.target.value
                      })
                    }}
                    fullWidth
                    required
                />
            </Grid>
            <Grid item xs={6}></Grid>
            <Grid item xs={6}>
                <TextField
                    label="image url"
                    id="image"
                    variant="outlined"
                    value={event?.image}
                    inputProps={{
                      maxlength:255,
                      pattern:"^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$"
                    }}
                    onChange={(e) => {
                        setEvent({
                          ...event,
                          image:e.target.value
                        })
                    }}
                    fullWidth
                    required
                />
            </Grid>
                
            <Grid item xs={12}>
                <TextField
                    id="desc"
                    label="Content"
                    multiline
                    rows={10}
                    value={event?.content}
                    variant="outlined"
                    onChange={(e) => {
                      setEvent({
                        ...event,
                        content:e.target.value
                      })
                    }}
                    fullWidth
                />
            </Grid>     
          </Grid>
         
          <div class="flex items-center mt-2">
                {!isEdit && <button id="submit"
                          class="mr-2 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                          // onClick={(e) => handleCreate(e)}
                          type="submit">
                          <i class="fab fa-whatsapp"></i> Create new
                      </button>
                }
                {isEdit && 
                  <>
                      
                      <button id="submit"
                        class="mr-4 bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        // onClick={(e) => handleUpdate(e)}
                        type="submit">
                        Update
                      </button>

                      <button id="submit"
                        // class="ml-2 bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
                        onClick={(e) => handleRefresh(e)}
                        type="submit">
                        <RefreshIcon className="text-blue-500"/>
                      </button>
                  </>
                }
          </div>
            </form>
        </div>
    );
}