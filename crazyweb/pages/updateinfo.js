import React from 'react';
import Head from 'next/head'
import NavBar from '../components/master/navbar'
import BreadCrumb from '../components/master/breadcrumbs'
import MainMenu from '../components/master/mainmenu'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { useSession } from 'next-auth/client'
import Alert from '@material-ui/lab/Alert'
import { isEmpty, isNumber } from './common/validateutils'
import axios from 'axios'

export default function UpdateInfo(){
    const [ session] = useSession()
    const [message, setMessage] = React.useState('');
    const [error, setError] = React.useState(false);
    const [photoErr,SetPhotoErr] = React.useState(false)
    const [phoneErr,SetPhoneErr] = React.useState(false)
    const [ageErr,SetAgeErr] = React.useState(false)
    const [info,setInfo]=  React.useState({
        photo:"",
        phone:"",
        age:""
    })

    const validFields = () => {
        if(isEmpty(info.photo, SetPhotoErr)){
            return true
        }
        if(isEmpty(info.phone, SetPhoneErr)){
            return true
        }
        if(isEmpty(info.age, SetAgeErr)){
            return true
        }

        return false
    }

    const handleUpdate = (e) => {
        e.preventDefault()
        if(!validFields()) {
            const options = {
                method: 'POST',
                headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
                data:{
                    ...info,
                    id:session?.user?.userid,
                },
                baseURL: 'http://localhost:8080/api/',
                url: 'user/update'
            };
    
            axios(options)
            .then(function (response) {
                setError(false)
                setMessage("Updated successfully!");
            })
            .catch(function (error) {
                setError(true)
                setMessage("Error")
            });
        }
    }
    return(
        <div className="">
            <Head>
                <title>Crazy Web</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main className="h-screen">
                <NavBar />
                <BreadCrumb className="bg-gray-400" data="UpdateInfo"/>
                <MainMenu />
                
                <form  onSubmit ={(e) => handleUpdate(e)} className="w-96 mx-auto border p-4 mt-4">
                {message && <Alert color={error ? "error" : "success"} severity={error ? "error" : "success"} className="pb-2">{message}</Alert>}
                    <Grid item xs={12}>
                    <TextField 
                        id="standard-basic" 
                        label="Age" 
                        onChange={(e) => {
                            setInfo({
                                ...info,
                                age: e.target.value
                            }) 
                        }}
                        type="number"
                        inputProps={{
                            max:100,
                            min:18,
                            maxlength:3
                        }}
                        error={ageErr}
                        helperText={ageErr ? "Age invalid" : ""}
                        autoFocus
                        fullWidth 
                        required
                        />
                    </Grid>
                    <br/>
                    <Grid  item xs={12}>
                    <TextField 
                        fullWidth 
                        id="standard-basic" 
                        label="Phone" 
                        onChange={(e) => setInfo({
                            ...info,
                            phone:e.target.value
                        })}
                        type="number"
                        InputProps={{
                            max:11,
                            min:10
                        }}
                        required
                        />
                    </Grid>
                    <br/>
                    <Grid  item xs={12}>
                    <TextField 
                        fullWidth 
                        id="standard-basic" 
                        label="Photo" 
                        onChange={(e) => setInfo({
                            ...info,
                            photo:e.target.value
                        })}
                        required
                        />
                    </Grid>
                    <br/>
                    <Grid item xs={12}>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        // className={classes.submit}
                    >
                        Update
                    </Button>
                </Grid>
                </form>
                
          

            </main>
        </div>

    )
}