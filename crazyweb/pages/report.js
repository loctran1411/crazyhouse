import Head from 'next/head'
import NavBar from '../components/master/navbar'
import BreadCrumb from '../components/master/breadcrumbs'
import MainMenu from '../components/master/mainmenu'
import FunctionNav from '../components/master/functionnav'
import EnhancedTable from '../components/account/table'
import useSWR from 'swr'
import { useSession, getSession } from 'next-auth/client'
import LineChartReporter from '../components/report/LineChartReporter'
import PieChartReporter from '../components/report/PieChartReporter'
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    '&.Mui-selected': {
      outline: 'none',                                                                   
    },
    '&:hover': {
      outline: 'none',
    },
  },
});

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  
  return (
      <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
      >
      {value === index && (
          <Box p={3}>
          <Typography>{children}</Typography>
          </Box>
      )}
      </div>
  );
}

export default function ReportPage(){
    const session = useSession()
    const [value, setValue] = React.useState(0);
    const classes = useStyles();
    const { data: revenueByYear,error: err } = useSWR(['http://localhost:8080/api/booking/report/year', session.accessToken])
    const handleChange = (event, newValue) => {
      setValue(newValue);
    };
    console.log(revenueByYear)
    return (
        <div className="">
            <Head>
                <title>Crazy Web</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main className="h-screen">
                <NavBar />
                <BreadCrumb className="bg-gray-400"/>
                <MainMenu />
                
                <TabPanel value={value} index={0}>
                  <LineChartReporter />
                </TabPanel> 
                <TabPanel value={value} index={1}>
                  <PieChartReporter data={revenueByYear}/>
                </TabPanel>
                
                {/* { error && <div>Failed to load{console.log(data)}</div> }
                { !error && !data && <div>loading...</div> }
                { !error && data && <div>ok{console.log(data)}</div> } */}
            </main>
            
            <FunctionNav>
              <Tabs
                  value={value}
                  onChange={handleChange}
                  indicatorColor={""}
                  textColor="primary"
                  variant="scrollable"
                  scrollButtons="auto"
                  centered="true"
              >
                  <Tab label="Line report"  wrapped="true" disableRipple="true" classes={classes}/>
                  <Tab label="Pie report" wrapped="true" disableRipple="true" classes={classes}/>
              </Tabs>
            </FunctionNav>
      
        </div>
    )

}

export async function getServerSideProps(context) {
    const _session = await getSession(context)
    if (!_session) {
      return {
        redirect: {
          destination: '/auth/signin',
          permanent: false,
        },
      }
    }
    return {
      props: {
        session: _session
      }
    }
  }