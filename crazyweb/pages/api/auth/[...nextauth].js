import NextAuth from 'next-auth'
import Providers from 'next-auth/providers'
import axios from 'axios'

export default NextAuth({
    providers: [
        Providers.Credentials({
            name: 'Credentials',
            async authorize(credentials) {
              try{
                  const user = await axios.post("http://localhost:8080/api/admin/login",
                  {
                      email: credentials.email,
                      password: credentials.password
                  },
                  {
                      headers:{
                          accept: '*/*',
                          'content-type': 'application/json'
                      }
                  }).then(res => res.data)
                  if (user) {
                    return user
                  } else {
                    throw new Error('email or password invalid' + '&email=' + credentials.email)
                  }

                }catch(e){
                  const error = e.message
                  throw new Error(error + '&email=' + credentials.email)
                }
                
              }
        })
    ],
    pages: {
        signIn: '/auth/signin',
        signOut: '/auth/signin',
        error: '/auth/signin'
    },
    callbacks: {
      async signIn(user, account, profile) {
        return true
      },
      async redirect(url, baseUrl) {
        return baseUrl
      },
      async session(session, token) {
        if(token?.accessToken){
          session.accessToken = token.accessToken
          session.user = token.user
        }
        return session
      },
      async jwt(token, user, account, profile, isNewUser) {
        if(user?.accessToken){
          token.accessToken = user.accessToken
          token.user = user
        }
        return token
      }
    },
    logger: {
        error(code, ...message) {
          console.error(code, message)
        },
        warn(code, ...message) {
          console.error(code, message)
        },
        debug(code, ...message) {
          console.error(code, message)
        }
    }
})

