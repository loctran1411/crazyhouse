import React from 'react';
import Head from 'next/head'
import NavBar from '../components/master/navbar'
import BreadCrumb from '../components/master/breadcrumbs'
import MainMenu from '../components/master/mainmenu'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import { useSession } from 'next-auth/client'
import Alert from '@material-ui/lab/Alert'
import { isEmpty } from './common/validateutils'
import axios from 'axios'

export default function ChangePass(){
    const [ session] = useSession()
    const [message, setMessage] = React.useState('');
    const [errorText, setErrorText] = React.useState('');
    const [error, setError] = React.useState(false);
    const [oldpwd,setOldPwd]=  React.useState("")
    const [newpwd,setNewPwd]=  React.useState("")
    const [confirmpwd,setConfirmPwd]=  React.useState("")
    const [pwdErr,SetPwdErr] = React.useState(false)
    const [newpwdErr,SetNewPwdErr] = React.useState(false)
    const [pwdconfirmErr,SetConfirmPwdErr] = React.useState(false)
    const [matchErr,SetMatchErr] = React.useState(false)

    const validFields = () => {
        if(isEmpty(oldpwd, SetPwdErr)){
            return true
        }
        if(isEmpty(newpwd, SetNewPwdErr)){
            return true
        }
        if(isEmpty(confirmpwd, SetConfirmPwdErr)){
            return true
        }
        if(newpwd != confirmpwd){
            SetMatchErr(true)
            return true
        }else{
            SetMatchErr(false)
        }
        return false
    }
    const handleChangePwd = (e) => {
        e.preventDefault()
        
        if(!validFields()) {
            const options = {
                method: 'POST',
                headers: { 'content-type': 'application/json', 'Authorization': session.accessToken},
                data:{
                    id:session?.user?.userid,
                    oldPassword:oldpwd,
                    newPassword:newpwd
                },
                baseURL: 'http://localhost:8080/api/',
                url: 'user/changepwd'
            };
    
            axios(options)
            .then(function (response) {
                if(response.data == 0){
                    setError(false)
                    SetPwdErr(true)
                    setErrorText("Old password isn't match");
                }else{
                    setError(false)
                    setMessage("Updated successfully!");
                    setErrorText("");
                }
            })
            .catch(function (error) {
                setError(true)
                setMessage("Error")
            });
        }
    }
    return(
        <div className="">
            <Head>
                <title>Crazy Web</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main className="h-screen">
                <NavBar />
                <BreadCrumb className="bg-gray-400" data="ChangePassword"/>
                <MainMenu />
                <form className="w-96 mx-auto border p-4 mt-4">
                    {message && <Alert color={error ? "error" : "success"} severity={error ? "error" : "success"} className="pb-2">{message}</Alert>}
                    <Grid item xs={12}>
                    <TextField 
                        id="standard-basic" 
                        label="Old Password"  
                        type="password"
                        onChange={(e) => setOldPwd(e.target.value)} 
                        error={pwdErr}
                        helperText={pwdErr ? errorText : ""}
                        autoFocus
                        required
                        fullWidth />
                    </Grid>
                    <br/>
                    <Grid  item xs={12}>
                    <TextField 
                        id="standard-basic" 
                        label="New Password"  
                        type="password"
                        error={newpwdErr}
                        helperText={newpwdErr ? "New password invalid" : ""}
                        onChange={(e) => setNewPwd(e.target.value)}
                        required
                        fullWidth />
                    </Grid>
                    
                    <br/>
                    <Grid  item xs={12}>
                    <TextField 
                        id="standard-basic" 
                        label="Confirm Password"  
                        type="password"
                        error={pwdconfirmErr || matchErr}
                        helperText={pwdconfirmErr ? "Confirm password invalid" : matchErr ? "Confirm password not match" : ""}
                        onChange={(e) => setConfirmPwd(e.target.value)}
                        required
                        fullWidth />
                    </Grid>
                    <br/>
                    <Grid item xs={12}>
                    <Button onClick ={(e) => handleChangePwd(e)}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        // className={classes.submit}
                    >
                        Confirm
                    </Button>
                </Grid>
                </form>
                
          

            </main>
        </div>

    )
}