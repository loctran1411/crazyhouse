import Head from 'next/head'
import NavBar from '../components/master/navbar'
import BreadCrumb from '../components/master/breadcrumbs'
import MainMenu from '../components/master/mainmenu'
import FunctionNav from '../components/master/functionnav'
import useSWR from 'swr'
import { useSession, getSession } from 'next-auth/client'
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AssignmentRoundedIcon from '@material-ui/icons/AssignmentRounded';
import PostAddRoundedIcon from '@material-ui/icons/PostAddRounded';
import { makeStyles } from '@material-ui/core/styles';
import EnhancedTable from '../components/combo/table'
import DataForm from '../components/combo/form'
import React, { useState } from 'react';

const useStyles = makeStyles({
  root: {
    '&.Mui-selected': {
      outline: 'none',                                                                   
    },
    '&:hover': {
      outline: 'none',
    },
  },
});

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  
  return (
      <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
      >
      {value === index && (
          <Box p={3}>
          <Typography>{children}</Typography>
          </Box>
      )}
      </div>
  );
  }


export default function ComboPage(){
    const session = useSession()
    let { data,error } = useSWR(['http://localhost:8080/api/combo/combos', session.accessToken])
    const [value, setValue] = useState(0);
    const classes = useStyles();
    const [isEdit, setEdit] = useState(false)
    const [combo,setCombo] = useState({
        comboName:"",
        room:{
          id:"1"
        },
        price:"0",
        description:""
    })
    const handleCombo = (c) => {
      setCombo(c)
    }
    const handleRefresh = () => {
      setCombo({
        comboName:"",
        room:{
          id:"1"
        },
        price:"0",
        description:""
      })
    }
    const handleEdit= (status) => {
      setEdit(status)
    }
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    return (
        <div className="">
            <Head>
                <title>Crazy Web</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main className="h-screen">
                <NavBar />
                <BreadCrumb className="bg-gray-400" data="Combo"/>
                <MainMenu />
                <TabPanel value={value} index={0}>
                    { error && <div>Failed to load</div> }
                    { !error && !data && <div>loading...</div> }
                    { !error && data && <EnhancedTable rows={data} changeTab={handleChange} refresh={handleRefresh} setEdit={setEdit} setCombo={handleCombo}/> }
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <DataForm combo={combo} setCombo={handleCombo} isEdit={isEdit} setEdit={handleEdit} refresh={handleRefresh}/>
                </TabPanel>
            </main>
            <FunctionNav>
              <Tabs
                  value={value}
                  onChange={handleChange}
                  indicatorColor={""}
                  textColor="primary"
                  // variant="scrollable"
                  scrollButtons="auto"
                  centered="true"
              >
                  <Tab label="View" icon={<AssignmentRoundedIcon />} wrapped="true" disableRipple="true" classes={classes}/>
                  <Tab label="Add new" icon={<PostAddRoundedIcon />} wrapped="true" disableRipple="true" classes={classes}/>
              </Tabs>
            </FunctionNav>
      
        </div>
    )

}

export async function getServerSideProps(context) {
    const _session = await getSession(context)
    if (!_session) {
      return {
        redirect: {
          destination: '/auth/signin',
          permanent: false,
        },
      }
    }
    return {
      props: {
        session: _session
      }
    }
  }