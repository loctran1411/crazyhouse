import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { csrfToken, signIn } from "next-auth/client";
import { useState } from "react";
import { useRouter } from "next/router";
import Alert from "@material-ui/lab/Alert";
import { useEffect } from "react";
import InputAdornment from "@material-ui/core/InputAdornment";
import EmailIcon from "@material-ui/icons/Email";
import HttpsIcon from "@material-ui/icons/Https";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import Pass from "../changepass";
import { getCsrfToken } from 'next-auth/client'


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link href="#" className="text-red-500">
        Crazyhouse
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignIn({ csrfToken }) {
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [pwd, setPwd] = useState("");
  const [isRemember, setIsRemember] = useState(false);
  const [loginError, setLoginError] = useState("");
  const router = useRouter();

  useEffect(() => {
    if (router.query.error) {
      setLoginError(router.query.error);
    }
  }, [router]);

  const handleLogin = (e) => {
    e.preventDefault()
    if(email && pwd){
      signIn('credentials', {
        email: email,
        password: pwd
      })
    }
  }
  return (
    <div className="border rounded-md p-2 mt-4 w-96 mx-auto border-gray-300 ring-4">
      <div className={classes.paper}>
        <div className="flex justify-center content-center">
          <AccountCircleIcon className="text-blue-600 mr-2" fontSize="large" />
          <span className="text-2xl text-blue-400">Sign in</span>
        </div>
        <form
          className={classes.form}
          method="POST"
          action="/api/auth/callback/credentials"
          noValidate
        >
          <input name="csrfToken" type="hidden" defaultValue={csrfToken} />
          <TextField
            // variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            type="email"
            autoComplete="email"
            value={email}
            autoFocus
            required
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <EmailIcon className="text-blue-500" />
                </InputAdornment>
              ),
            }}
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            // variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <HttpsIcon className="text-blue-500" />
                </InputAdornment>
              ),
            }}
            onChange={(e) => setPwd(e.target.value)}
            required
          />
          <label>
            <input
              type="checkbox"
              className="mr-2"
              checked={isRemember}
              onChange={(e) => setIsRemember(e.target.value)}
            />
            Remember me
          </label>

          {loginError && (
            <Alert severity="error" variant="outlined" className="my-2">
              {loginError}
            </Alert>
          )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onSubmit={(e) => handleLogin(e) }
          >
            Sign In
          </Button>
        </form>
      </div>
      <div className="my-2">
        <Copyright />
      </div>
    </div>
  );
}

export async function getServerSideProps(context) {
  return {
    props: {
      csrfToken: await getCsrfToken(context)
    }
  }
}