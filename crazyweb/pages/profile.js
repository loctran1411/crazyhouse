import Head from 'next/head'
import NavBar from '../components/master/navbar'
import BreadCrumb from '../components/master/breadcrumbs'
import MainMenu from '../components/master/mainmenu'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import { signOut } from 'next-auth/client'
import MailIcon from '@material-ui/icons/Mail';
import ContactPhoneIcon from '@material-ui/icons/ContactPhone';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AutorenewIcon from '@material-ui/icons/Autorenew';
import Badge from '@material-ui/core/Badge'



export default function ProfilePage(){
    const item_style = "block px-1 py-2 text-center text-md  border bg-gray-100 hover:bg-green-100  cursor-pointer ";
    const animation = "transform-gpu transition duration-500 ease-in-out transform  hover:-translate-x-2";
    const logoutHandle = (e) => {
        e.preventDefault()
        signOut()
      }
    return (
        <div className="">
            <Head>
                <title>Crazy Web</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main className="h-screen">
                <NavBar />
                <BreadCrumb className="bg-gray-400" data="Profile"/>
                <MainMenu />
                <List component="nav" aria-label="main mailbox folders">
                    <a href="#" >   
                        <ListItem className={item_style + animation}>
                            <Badge badgeContent={100} color="primary">
                                <MailIcon className="text-red-500"  />
                            </Badge>
                            <ListItemText primary="Inbox" className="ml-4"/>
                        </ListItem>
                    </a>
                    {/* <Divider /> */}
                    <a href="/updateinfo" >  
                        <ListItem className={item_style + animation}>
                            <ContactPhoneIcon className="mr-4 text-blue-500"/><ListItemText primary="Update Info" />
                        </ListItem>
                    </a>
                    <Divider />
                    <a href="/changepass" >  
                    <ListItem className={item_style + animation}>
                       <AutorenewIcon className="mr-4 text-green-500"/><ListItemText primary="Change password" />
                    </ListItem>
                    </a>
                    <Divider />
                    <a href="#"  onClick={(e) => logoutHandle(e)}>
                        <ListItem className={item_style + animation}>
                            <ExitToAppIcon className="mr-4 text-gray-500"/><ListItemText primary="Sign out" />
                        </ListItem>
                    </a>
                </List>

            </main>
        </div>
    )
}