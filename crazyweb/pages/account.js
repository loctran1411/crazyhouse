import Head from 'next/head'
import NavBar from '../components/master/navbar'
import BreadCrumb from '../components/master/breadcrumbs'
import MainMenu from '../components/master/mainmenu'
import FunctionNav from '../components/master/functionnav'
import useSWR from 'swr'
import { useSession, getSession } from 'next-auth/client'
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AssignmentRoundedIcon from '@material-ui/icons/AssignmentRounded';
import PostAddRoundedIcon from '@material-ui/icons/PostAddRounded';
import SearchRoundedIcon from '@material-ui/icons/SearchRounded';
import { makeStyles } from '@material-ui/core/styles';
import EnhancedTable from '../components/account/table'
import DataForm from '../components/account/form'
import SearchBox from '../components/account/searchbox'
import React, { useState, useEffect } from 'react';
const useStyles = makeStyles({
  root: {
    '&.Mui-selected': {
      outline: 'none',                                                                   
    },
    '&:hover': {
      outline: 'none',
    },
  },
});
function TabPanel(props) {
  const { children, value, index, ...other } = props;
  
  return (
      <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
      >
      {value === index && (
          <Box p={3}>
          <Typography>{children}</Typography>
          </Box>
      )}
      </div>
  );
  }
export default function AccountPage(){
  const session = useSession()
  let { data,error } = useSWR(['http://localhost:8080/api/user/users', session.accessToken])
  const [value, setValue] = useState(0);
  const classes = useStyles();
  const [isEdit, setEdit] = useState(false)
  const [account,setAccount] = useState({
      info:{
        firstName:"",
        lastName: "",
      },
      email:"",
      password:"",
      role:{id:1}
  })
  const handleAccount = (r) => {
    setAccount(r)
  }
  const handleRefresh = () => {
    setAccount({
      info:{
        firstName:"",
        lastName: "",
      },
      email:"",
      password:"",
      role:{id:1}
    })
  }
  const handleEdit= (status) => {
    setEdit(status)
  }
  const handleChange = (event, newValue) => {
      setValue(newValue);
    };
    return (
      <div className="">
      <Head>
          <title>Crazy Web</title>
          <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="h-screen">
          <NavBar />
          <BreadCrumb className="bg-gray-400" data="Account"/>
          <MainMenu />
          <TabPanel value={value} index={0}>
              { error && <div>Failed to load{console.log(data)}</div> }
              { !error && !data && <div>loading...</div> }
              { !error && data && <EnhancedTable rows={data} changeTab={handleChange} refresh={handleRefresh} setEdit={setEdit} setAccount={handleAccount}/> }
          </TabPanel>
          <TabPanel value={value} index={1}>
              <DataForm account={account} setAccount={handleAccount} isEdit={isEdit} setEdit={handleEdit} refresh={handleRefresh}/>
          </TabPanel>
          <TabPanel value={value} index={2}>
              <SearchBox />
          </TabPanel>
      </main>
      <FunctionNav>
        <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor={""}
            textColor="primary"
            centered="true"
        >
            <Tab label="View" icon={<AssignmentRoundedIcon />} wrapped="true" disableRipple="true" classes={classes}/>
            <Tab label="Add new" icon={<PostAddRoundedIcon />} wrapped="true" disableRipple="true" classes={classes}/>
            <Tab label="Search" icon={<SearchRoundedIcon />} wrapped="true" disableRipple="true" classes={classes}/>
        </Tabs>
      </FunctionNav>

  
            
      
        </div>
    )

}

export async function getServerSideProps(context) {
    const _session = await getSession(context)
    if (!_session) {
      return {
        redirect: {
          destination: '/auth/signin',
          permanent: false,
        },
      }
    }
    return {
      props: {
        session: _session
      }
    }
  }