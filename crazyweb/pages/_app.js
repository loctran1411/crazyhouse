import '../styles/globals.css'
import { Provider } from 'next-auth/client'
import { useSession } from 'next-auth/client'
import { SWRConfig } from 'swr'
import axios from 'axios'

function MyApp({ Component, pageProps }) {
  const [ session, loading ] = useSession()

  return (

      <Provider session={pageProps.session}
        options={{ 
        clientMaxAge: 60,    // Re-fetch session if cache is older than 60 seconds
        keepAlive:    5 * 60 // Send keepAlive message every 5 minutes
      }}>
        <SWRConfig 
          value={{
            refreshInterval: 3000,
            fetcher: url => axios(url,{
                     headers:{
                        Authorization: pageProps.session.accessToken
                    }
            }).then(res => res.data)
          }}
        >
          <Component {...pageProps} />
        </SWRConfig>
      </Provider>

  )
}

export default MyApp
