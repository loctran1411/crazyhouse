import Head from 'next/head'
import NavBar from '../components/master/navbar'
import BreadCrumb from '../components/master/breadcrumbs'
import MainMenu from '../components/master/mainmenu'
import FunctionNav from '../components/master/functionnav'
// import jwt from 'next-auth/jwt'
import { useSession, getSession } from 'next-auth/client'
import LineChartReporter from '../components/report/LineChartReporter'
import PieChartReporter from '../components/report/PieChartReporter'
import BarChartReporter from '../components/report/BarChartReporter'
import useSWR from 'swr'


export default function Home() {
  const session = useSession()
  const { data: revenueByYear,error: err } = useSWR(['http://localhost:8080/api/booking/report/year', session.accessToken])
  const { data: revenueByMonth,error: errr } = useSWR(['http://localhost:8080/api/booking/report/month', session.accessToken])
  const currDate = new Date()
  return (
    <div className="">
      <Head>
        <title>Crazy Web</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="h-screen">
        <NavBar />
        <BreadCrumb className="bg-gray-400" />
        <MainMenu />
        <div className="mt-4 flex justify-center">
          {/* <LineChartReporter data={revenueByYear}/> */}
          <BarChartReporter data={revenueByMonth} color='#0088FE' title={() => <h4 className="text-center text-blue-500 font-bold text-lg">Doanh thu tháng {currDate.getUTCMonth()}</h4>}/>
          <BarChartReporter  data={revenueByYear} color='#FF8042' title={() => <h4 className="text-center text-yellow-500 font-bold text-lg">Doanh thu năm {currDate.getFullYear()}</h4>}/>  
        </div>

      </main>
      
      
    </div>
    
  )
}

export async function getServerSideProps(context) {
  const _session = await getSession(context)
  if (!_session) {
    return {
      redirect: {
        destination: '/auth/signin',
        permanent: false,
      },
    }
  }
  return {
    props: {
      session: _session
    }
  }
}

