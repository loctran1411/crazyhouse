import React, { useState, useEffect } from 'react';
import Head from 'next/head'
import NavBar from '../components/master/navbar'
import BreadCrumb from '../components/master/breadcrumbs'
import MainMenu from '../components/master/mainmenu'
import FunctionNav from '../components/master/functionnav'
import useSWR from 'swr'
import { useSession, getSession } from 'next-auth/client'
import EnhancedTable from '../components/event/table'
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import AssignmentRoundedIcon from '@material-ui/icons/AssignmentRounded';
import PostAddRoundedIcon from '@material-ui/icons/PostAddRounded';
import SearchRoundedIcon from '@material-ui/icons/SearchRounded';
import DataForm from '../components/event/form'
import SearchBox from '../components/event/searchbox'
import {getCurrDateAsString, plusDays} from './common/datetimeutils'

const useStyles = makeStyles({
  root: {
    '&.Mui-selected': {
      outline: 'none',                                                                   
    },
    '&:hover': {
      outline: 'none',
    },
  },
});

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  
  return (
      <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
      >
      {value === index && (
          <Box p={3}>
          <Typography>{children}</Typography>
          </Box>
      )}
      </div>
  );
  }

export default function EventPage(){
    const currDate = getCurrDateAsString()
    const session = useSession()
    const [value, setValue] = React.useState(0);
    const { data,error } = useSWR(['http://localhost:8080/api/event/events/active', session.accessToken])
    const classes = useStyles();

    const [isEdit, setEdit] = useState(false)

    const [event,setEvent] = useState({
      event_name:"",
      content: "",
      image: "",
      beginDate: currDate,
      endDate: plusDays(currDate, 1)
    })
    const handleEvent = (e) => {
      setEvent(e)
    }
    const handleRefresh = () => {
      setEvent({
        event_name:"",
        content: "",
        image: "",
        beginDate: currDate,
        endDate: plusDays(currDate, 1)
      })
    }
    const handleEdit= (status) => {
      setEdit(status)
    }

    const handleChange = (event, newValue) => {
        setValue(newValue);
      };
   
    return (
        <div className="">
            <Head>
                <title>Crazy Web</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <main className="">
                <NavBar />
                <BreadCrumb className="bg-gray-400" data="Event"/>
                <MainMenu />
                <TabPanel value={value} index={0}>
                    { error && <div>Failed to load{console.log(data)}</div> }
                    { !error && !data && <div>loading...</div> }
                    { !error && data && <EnhancedTable rows={data} changeTab={handleChange} refresh={handleRefresh} setEdit={setEdit} setEvent={handleEvent}/> }
                </TabPanel>
                <TabPanel value={value} index={1}>
                  <DataForm event={event} setEvent={handleEvent} isEdit={isEdit} setEdit={handleEdit} refresh={handleRefresh}/>
                </TabPanel>
                {/* <TabPanel value={value} index={2}>
                  <SearchBox />
                </TabPanel> */}
            </main>
            <FunctionNav>
              <Tabs
                  value={value}
                  onChange={handleChange}
                  indicatorColor={""}
                  textColor="primary"
                  // variant="scrollable"
                  centered="true"
                  // scrollButtons="auto"
            
              >
                  <Tab label="View" icon={<AssignmentRoundedIcon />} wrapped="true" disableRipple="true" classes={classes}/>
                  <Tab label="New" icon={<PostAddRoundedIcon />} wrapped="true" disableRipple="true" classes={classes}/>
                  {/* <Tab label="Search" icon={<SearchRoundedIcon />} wrapped="true" disableRipple="true" classes={classes}/> */}
              </Tabs>
            </FunctionNav>
      
        </div>
    )

}

export async function getServerSideProps(context) {
    const _session = await getSession(context)
    if (!_session) {
      return {
        redirect: {
          destination: '/auth/signin',
          permanent: false,
        },
      }
    }
    return {
      props: {
        session: _session
      }
    }
  }