const getCurrDateAsString = () => {
    let curr = new Date();
    curr.setHours(curr.getHours() + 7);
    const currString = curr.toISOString().substr(0,16)
    return currString
}

const getCurrDateAsShortString = () => {
    let curr = new Date();
    const currString = curr.toISOString().substr(0,10)
    return currString
}

const getDateAsString = (datetime) => {
    let dt = new Date(datetime);
    const currString = dt.toISOString().substr(0,16)
    return currString
}

const plusHours = (datetime, hours) => {
    let dt = new Date(datetime);
    dt.setHours(dt.getHours() + hours + 7);
    const currString = dt.toISOString().substr(0,16)
    return currString
}

const plusDays = (datetime, days) => {
    let dt = new Date(datetime);
    dt.setHours(dt.getHours() + 7);
    dt.setDate(dt.getDate() + days);
    const currString = dt.toISOString().substr(0,16)
    return currString
}

const plusShortDays = (datetime, days) => {
    let dt = new Date(datetime);
    dt.setDate(dt.getDate() + days);
    const currString = dt.toISOString().substr(0,10)
    return currString
}

const compareDateTime = (dt1, dt2) => {
    const datetime1 = new Date(dt1);
    const datetime2 = new Date(dt2);
    if(datetime1 < datetime2){
        return -1;
    }
    else if(datetime1 > datetime2){
        return 1;
    }
    return 0
}

export { getCurrDateAsString, getDateAsString, plusDays, plusHours, compareDateTime, getCurrDateAsShortString, plusShortDays }