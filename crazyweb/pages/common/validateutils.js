import { FastfoodOutlined } from "@material-ui/icons"

const regexEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g
const regexOnlyNumber = /^[0-9]*$/g

// const isEmpty = (value) => value.trim() == '' ? true : false
const isEmpty = (value, setState) => { 
    if(value.trim() == ''){
        setState(true)
        return true
    }
    setState(false)
    return false
}
const isEmail = (value) => regexEmail.test(value) 
const isNumber = (value) => regexOnlyNumber.test(value) 

export { isEmpty, isEmail, isNumber }