package com.edu.assignment.model;

import java.util.Date;

public class UserInfoWeb {
	private Long userid;
	private String fullName;
	private String photo;
	private String email;
	private String accessToken;
	private Date expiry;

	public UserInfoWeb(String email) {
		this.email = email;
	}
	
	public UserInfoWeb(String email, String accessToken) {
		this.email = email;
		this.accessToken = accessToken;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Date getExpiry() {
		return expiry;
	}

	public void setExpiry(Date expiry) {
		this.expiry = expiry;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}
	
	
	
}
