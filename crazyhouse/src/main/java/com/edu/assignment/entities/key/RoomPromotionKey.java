package com.edu.assignment.entities.key;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RoomPromotionKey implements Serializable{
	@Column(name = "room_id")
	int roomId;
	@Column(name = "promotion_id")
	long promotionId;
}
