package com.edu.assignment.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Promotion implements Serializable{
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	
	@ColumnDefault("''")
	private String description;
	
	@CreationTimestamp
	private Date createdDate;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date beginDate;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date endDate;
	
	@ColumnDefault("0")
	private int isDeleted;
	
	@JsonIgnore
	@OneToMany(mappedBy = "promotion")
	private Set<RoomPromotion> promotion;
	
	@ManyToOne
	@JoinColumn(name = "createdBy", referencedColumnName = "id")
	private UserAccount createdBy;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}
	public UserAccount getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(UserAccount createdBy) {
		this.createdBy = createdBy;
	}
	public Set<RoomPromotion> getPromotion() {
		return promotion;
	}
	public void setPromotion(Set<RoomPromotion> promotion) {
		this.promotion = promotion;
	}
	

}
