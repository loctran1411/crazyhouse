package com.edu.assignment.entities;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

import com.edu.assignment.entities.key.RoomEventKey;

@Entity(name = "room_event")
public class RoomEvent implements Serializable{
	
	@EmbeddedId
	RoomEventKey id;
	
	@ManyToOne
	@MapsId("roomId")
	@JoinColumn(name = "room_id")
	Room room;
	
	@ManyToOne
	@MapsId("eventId")
	@JoinColumn(name = "event_id")
	Event event;
}
