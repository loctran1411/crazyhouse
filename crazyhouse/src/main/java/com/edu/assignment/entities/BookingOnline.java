package com.edu.assignment.entities;

import java.util.Date;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.transaction.Transactional;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class BookingOnline {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ColumnDefault("''")
	private String note;
	
	@JsonIgnore
	@OneToMany(mappedBy = "booking")
	private Set<RoomBookingOnline> booking;
//	
	
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private UserAccount customer;
	
	@ManyToOne
	@JoinColumn(name = "created_by")
	private UserAccount createdBy;
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_date", insertable = false, updatable = false, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	private Date createdDate;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public UserAccount getCustomer() {
		return customer;
	}

	public void setCustomer(UserAccount customer) {
		this.customer = customer;
	}
	public UserAccount getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(UserAccount createdBy) {
		this.createdBy = createdBy;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Set<RoomBookingOnline> getBooking() {
		return booking;
	}
	public void setBooking(Set<RoomBookingOnline> booking) {
		this.booking = booking;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
