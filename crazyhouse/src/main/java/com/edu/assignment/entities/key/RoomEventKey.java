package com.edu.assignment.entities.key;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RoomEventKey implements Serializable{
	@Column(name = "room_id")
	int roomId;
	@Column(name = "event_id")
	long eventId;

}
