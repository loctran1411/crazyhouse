package com.edu.assignment.entities.key;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class RoomBookingKey implements Serializable{
	@Column(name = "room_id")
	public int roomId;
	@Column(name = "booking_id")
	public long bookingId;
}
