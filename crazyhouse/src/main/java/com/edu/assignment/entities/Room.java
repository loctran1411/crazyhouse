package com.edu.assignment.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Room implements Serializable{
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private int room_number;
	private String name;
	@ColumnDefault(value = "'default.png'")
	private String image;
	
	@ColumnDefault(value = "'4x4'")
	private String size;
	
	private byte status;
	
	@JsonIgnore
	@OneToMany(mappedBy = "room")
	private Set<RoomPromotion> promotions;
	
	@JsonIgnore
	@OneToMany(mappedBy = "room")
	private Set<RoomEvent> event;
////	
	@JsonIgnore
	@OneToMany(mappedBy = "room")
	private Set<RoomBookingOnline> room;

	public void addRoomPromotion(RoomPromotion roomPromotion) {
		this.promotions.add(roomPromotion);
	}

	

	public String getImage() {
		return image;
	}



	public void setImage(String image) {
		this.image = image;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public int getRoom_number() {
		return room_number;
	}

	public void setRoom_number(int room_number) {
		this.room_number = room_number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public Set<RoomBookingOnline> getRoom() {
		return room;
	}

	public void setRooms(Set<RoomBookingOnline> room) {
		this.room = room;
	}

	public Set<RoomPromotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(Set<RoomPromotion> promotions) {
		this.promotions = promotions;
	}

	public void setRoom(Set<RoomBookingOnline> room) {
		this.room = room;
	}

	public Set<RoomEvent> getEvent() {
		return event;
	}

	public void setEvent(Set<RoomEvent> event) {
		this.event = event;
	}
	
	
}
