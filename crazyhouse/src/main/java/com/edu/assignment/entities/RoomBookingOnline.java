package com.edu.assignment.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.ColumnDefault;

@Entity(name = "room_booking")
public class RoomBookingOnline implements Serializable{
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(cascade = CascadeType.ALL) 
	@JoinColumn(name = "booking_id")
	private BookingOnline booking;
	
	@ManyToOne 
	@JoinColumn(name = "room_id")
	private Room room;
	
	@ManyToOne
	@JoinColumn(name = "combo_id")
	private Combo combo;
	
	@ColumnDefault(value = "0")
	private byte status;
	private Date bookingDate;
	@ColumnDefault("1")
	private int hours;
	@ColumnDefault("''")
	private String note;
	
	public BookingOnline getBooking() {
		return booking;
	}

	public void setBooking(BookingOnline booking) {
		this.booking = booking;
	}

	public Room getRoom() {
		return room;
	}

	public void setRooms(Room rooms) {
		this.room = rooms;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Combo getCombo() {
		return combo;
	}

	public void setCombo(Combo combo) {
		this.combo = combo;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}
	
}
