package com.edu.assignment.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.edu.assignment.entities.RoomPromotion;
import com.edu.assignment.entities.key.RoomPromotionKey;

public interface RoomPromotionRepository extends JpaRepository<RoomPromotion, RoomPromotionKey>{
	@Query("select rp from RoomPromotion rp join Promotion p on rp.promotion=p.id where p.description like %?1% or p.id like %?1% or p.name like %?1%")
	public List<RoomPromotion> searchPromotions(String keyword);
}
