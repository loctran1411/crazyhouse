package com.edu.assignment.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edu.assignment.entities.UserRole;


public interface RoleRepository extends JpaRepository<UserRole, Integer>{
	public UserRole findByName(String name);
	public UserRole findById(int id);
}
