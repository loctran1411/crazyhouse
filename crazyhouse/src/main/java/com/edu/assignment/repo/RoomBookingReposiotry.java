package com.edu.assignment.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.edu.assignment.entities.RoomBookingOnline;
import com.edu.assignment.model.RevenueReporter;

public interface RoomBookingReposiotry extends JpaRepository<RoomBookingOnline, Long>{
	@Query("select rp from room_booking rp "
			+ "join BookingOnline p on rp.booking=p.id "
			+ "join Room r on rp.room = r.id "
			+ "join UserAccount u on p.customer = u.id "
			+ "where p.id = ?1 or u.id = ?1")
	public List<RoomBookingOnline> searchBookings(Long keyword);
	
	@Query(value = "SELECT new com.edu.assignment.model.RevenueReporter('room ' + rb.room.id,(sum(rb.hours) * sum(c.price))) FROM room_booking rb "
			+ "JOIN Combo c ON c.comboId = rb.combo "
			+ "GROUP BY Year(rb.bookingDate), rb.room")
	public List<RevenueReporter> reportRevenueByYear();
	
	@Query(value = "SELECT new com.edu.assignment.model.RevenueReporter('room ' + rb.room.id,(sum(rb.hours) * sum(c.price))) FROM room_booking rb "
			+ "JOIN Combo c ON c.comboId = rb.combo "
			+ "WHERE YEAR(now()) = YEAR(rb.bookingDate) and MONTH(now()) = MONTH(rb.bookingDate) "
			+ "GROUP BY YEAR(rb.bookingDate), MONTH(rb.bookingDate), rb.room")
	public List<RevenueReporter> reportRevenueByMonth();
}
