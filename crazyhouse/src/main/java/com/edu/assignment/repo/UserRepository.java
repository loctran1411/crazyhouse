package com.edu.assignment.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.edu.assignment.entities.UserAccount;


public interface UserRepository extends JpaRepository<UserAccount, Long>{
	public UserAccount findByEmail(String email);
	public List<UserAccount> findByStatus(byte status);
	
	@Query("select rp from UserAccount rp join UserInfo p on rp.info=p.id where rp.email like %?1% or p.firstName like %?1% or p.lastName like %?1%")
	public List<UserAccount> searchAccount(String keyword);
}
