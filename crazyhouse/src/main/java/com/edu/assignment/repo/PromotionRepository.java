package com.edu.assignment.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.edu.assignment.entities.Promotion;

public interface PromotionRepository extends JpaRepository<Promotion, Long>{
	
	@Query("select p from Promotion p where p.isDeleted=0 and p.beginDate <= now() And p.endDate >= now()")
	public List<Promotion> findInActive();
	
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update Promotion p set p.isDeleted=1 where p.id=?1")
	public void setIsDeleted(long id);
	
	@Query("select p from Promotion p where p.description like %?1% or p.id like %?1% or p.name like %?1%")
	public List<Promotion> searchPromotions(String keyword);
}
