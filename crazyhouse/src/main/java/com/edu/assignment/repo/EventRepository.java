package com.edu.assignment.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.edu.assignment.entities.Event;

public interface EventRepository extends CrudRepository<Event, Long>{
	@Query("select e from Event e where e.beginDate <= now() And e.endDate >= now()")
	public List<Event> findInActive();
}
