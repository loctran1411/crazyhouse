package com.edu.assignment.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.edu.assignment.entities.Combo;

public interface ComboRepository extends CrudRepository<Combo, Integer>{
	@Query("select c from Combo c join Room r on c.room = r.id where r.id = ?1")
	public List<Combo> searchByRoom(Long id);
}
