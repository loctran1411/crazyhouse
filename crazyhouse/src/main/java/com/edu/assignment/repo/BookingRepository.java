package com.edu.assignment.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edu.assignment.entities.BookingOnline;

public interface BookingRepository extends JpaRepository<BookingOnline, Long>{
	
}
