package com.edu.assignment.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edu.assignment.entities.UserInfo;

public interface UserInfoRepository extends JpaRepository<UserInfo, Long>{

}
