package com.edu.assignment.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.edu.assignment.entities.Room;

public interface RoomRepository extends JpaRepository<Room, Long>{
	public List<Room> findByStatus(byte status);
}
