package com.edu.assignment;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.edu.assignment.filter.JWTAuthorizationFilter;

@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors()
			.and()
			.csrf().disable()
			.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
			.authorizeRequests()
//			.antMatchers(HttpMethod.OPTIONS,"/api/promotion/promotions/active").permitAll()
			.antMatchers(HttpMethod.POST, "/login").permitAll()
			.antMatchers(HttpMethod.POST, "/admin/login").permitAll()
			.antMatchers(HttpMethod.POST, "/web/login").permitAll()
			.anyRequest().authenticated();
//		http.formLogin()
//			.loginPage("http://localhost:3000/login")
//			.loginProcessingUrl("/user/login")
//			.defaultSuccessUrl("/", true)
//			.failureUrl("http://localhost:3000/error")
//			.and()
//			.logout()
//			.logoutUrl("/logout");
	}
}
