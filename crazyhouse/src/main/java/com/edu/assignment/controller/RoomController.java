package com.edu.assignment.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.edu.assignment.entities.Room;
import com.edu.assignment.service.RoomService;

@RestController
@RequestMapping("/room")
public class RoomController {
	@Autowired private RoomService service;
	
	@GetMapping("/rooms")
	public ResponseEntity<List<Room>> getAllRooms(){
		List<Room> room = null;
		try {
			room = service.getAllRoom();
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<List<Room>>(room, HttpStatus.OK);
	}
	
	@PostMapping("/new")
	public ResponseEntity<Long> createNew(@RequestBody Room room){
		try {
			
			Long id = service.addNew(room);
			return new ResponseEntity<Long>(id, HttpStatus.OK);
		}catch(Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/rooms/{id}")
	public ResponseEntity<Room> getRoomById(@PathVariable Long id){
		Room room = null;
		try {
			room = service.getRoomById(id);
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<Room>(room, HttpStatus.OK);
	}
	
	@PostMapping("rooms/update")
	public ResponseEntity<String> updateRoomStatus(@RequestBody Long roomId,@RequestBody byte status){
		Room checkExistance = service.getRoomById(roomId);
		if(checkExistance != null) {
			if(checkExistance.getStatus() == status) 
			return new ResponseEntity<String>("The new status is newest", HttpStatus.OK);
			else
			{
				checkExistance.setStatus(status);
				service.updateStatus(checkExistance);
				return new ResponseEntity<String>("Updating successfully", HttpStatus.OK);
			}
		}
		else
		return new ResponseEntity<String>("Room does not exists", HttpStatus.BAD_REQUEST);
	}
}
