package com.edu.assignment.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.edu.assignment.entities.RoomPromotion;
import com.edu.assignment.entities.UserAccount;
import com.edu.assignment.entities.UserInfo;
import com.edu.assignment.model.PasswordChanger;
import com.edu.assignment.model.UserInfoWeb;
import com.edu.assignment.service.UserAccountService;
import com.edu.assignment.service.UserInfoService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/user")
public class UserAccountController {
	@Autowired private UserAccountService service;
	@Autowired private UserInfoService infoService;
	@Autowired private Map<Long, String> sessionManager;
	
	@GetMapping("/users")
	public ResponseEntity<List<UserAccount>> getAllUserAccount(){
		List<UserAccount> users = null;
		try {
			users = service.getAllUserAccount();
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<List<UserAccount>>(users, HttpStatus.OK);
	}
	
	@GetMapping("/user/{id}")
	public ResponseEntity<UserAccount> getUserById(@PathVariable long id){
		UserAccount user = null;
		try {
			user = service.getUserById(id);
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<UserAccount>(user, HttpStatus.OK);
	}
	
	@GetMapping("/user/{status}")
	public ResponseEntity<List<UserAccount>> getUsersByStatus(@PathVariable byte status){
		List<UserAccount> users = null;
		try {
			users = service.getUserByStatus(status);
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<List<UserAccount>>(users, HttpStatus.OK);
	}
	
	@PostMapping("/new")
	public ResponseEntity<Long> createNew(@RequestBody UserAccount acc){
		try {
			Long id = service.createNew(acc);
			return new ResponseEntity<Long>(id, HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/update")
	public ResponseEntity<Long> updateInfo(@RequestBody UserInfo info){
		try {
			Long id = infoService.updateInfo(info);

			return new ResponseEntity<Long>(id, HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/changepwd")
	public ResponseEntity<Long> changePwd(@RequestBody PasswordChanger acc){
		try {
			UserAccount user = service.getUserById(acc.id);
			if(user == null) {
				return new ResponseEntity<Long>(HttpStatus.BAD_REQUEST);
			}
			if(!user.getPassword().equals(acc.oldPassword)) {
				return new ResponseEntity<Long>(0L,HttpStatus.OK);
			}
			Long id = service.changePwd(acc);
			return new ResponseEntity<Long>(id, HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<UserAccount>> searchByDescription(@RequestParam String keyword){
		List<UserAccount> lstAccounts = null;
		try {
			lstAccounts = service.search(keyword);
			return new ResponseEntity<List<UserAccount>>(lstAccounts, HttpStatus.OK);

		}catch (Exception ex) {
			return new ResponseEntity<List<UserAccount>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
//	@PostMapping("/login")
//	public ResponseEntity<UserInfoWeb> login(@RequestParam("email") String email, @RequestParam("password") String pwd){
//		UserAccount user = service.getUserByEmail(email);
//		if(user !=null && user.getPassword().equals(pwd)) {
//			String token = getJWTToken(email);
//			sessionManager.put(user.getId(), token);
//			UserInfoWeb info = new UserInfoWeb(email,token);
//			System.out.println("login susscessful");
//			return new ResponseEntity<UserInfoWeb>(info, HttpStatus.OK);
//		}
//		return new ResponseEntity<UserInfoWeb>(HttpStatus.OK);
//	}
//	
//	private String getJWTToken(String email) {
//		String secretKey = "crazyhouse";
//		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
//				.commaSeparatedStringToAuthorityList("ROLE_USER");
//		
//		String token = Jwts
//				.builder()
//				.setId("softtekJWT")
//				.setSubject(email)
//				.claim("authorities",
//						grantedAuthorities.stream()
//								.map(GrantedAuthority::getAuthority)
//								.collect(Collectors.toList()))
//				.setIssuedAt(new Date(System.currentTimeMillis()))
//				.setExpiration(new Date(System.currentTimeMillis() + 600000))
//				.signWith(SignatureAlgorithm.HS512,
//						secretKey.getBytes()).compact();
//
//		return "Bearer " + token;
//	}
}
