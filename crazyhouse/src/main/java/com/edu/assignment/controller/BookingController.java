package com.edu.assignment.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.edu.assignment.entities.RoomBookingOnline;
import com.edu.assignment.entities.RoomPromotion;
import com.edu.assignment.model.RevenueReporter;
import com.edu.assignment.service.BookingService;

@RestController
@RequestMapping("/booking")
public class BookingController {
	@Autowired private BookingService bookingService;
	
	
	@GetMapping("/all")
	public ResponseEntity<List<RoomBookingOnline>> getAllBookingDetails(){
		List<RoomBookingOnline> books = null;
		try {
			books = bookingService.getAllBookingDetails();
			return new ResponseEntity<List<RoomBookingOnline>>(books, HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<RoomBookingOnline>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/report/year")
	public ResponseEntity<List<RevenueReporter>> reportRevenueByYear(){
		List<RevenueReporter> books = null;
		try {
			books = bookingService.reportRevenueByYear();
			return new ResponseEntity<List<RevenueReporter>>(books, HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<RevenueReporter>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/report/month")
	public ResponseEntity<List<RevenueReporter>> reportRevenueByMonth(){
		List<RevenueReporter> books = null;
		try {
			books = bookingService.reportRevenueByMonth();
			return new ResponseEntity<List<RevenueReporter>>(books, HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<RevenueReporter>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/new")
	public ResponseEntity<Long> bookRoom(@RequestBody RoomBookingOnline book){
		try{
			if(book.getBookingDate().compareTo(new Date()) < 0) {
				return new ResponseEntity<Long>(0L, HttpStatus.OK);
			}
			Long bookId = bookingService.bookRoom(book);
			return new ResponseEntity<Long>(bookId, HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<RoomBookingOnline>> searchByDescription(@RequestParam Long keyword){
		List<RoomBookingOnline> promotions = null;
		try {
			promotions = bookingService.search(keyword);
			return new ResponseEntity<List<RoomBookingOnline>>(promotions, HttpStatus.OK);

		}catch (Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<RoomBookingOnline>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
