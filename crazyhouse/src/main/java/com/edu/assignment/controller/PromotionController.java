package com.edu.assignment.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.edu.assignment.entities.Promotion;
import com.edu.assignment.entities.RoomPromotion;
import com.edu.assignment.service.PromotionService;

@RestController
@RequestMapping("/promotion")
public class PromotionController {
	@Autowired private PromotionService service;
	
	@GetMapping("/promotions")
	public ResponseEntity<List<RoomPromotion>> getAllPromotions(){
		List<RoomPromotion> promotions = null;
		try {
			promotions = service.getAllPromotion();
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<List<RoomPromotion>>(promotions, HttpStatus.OK);

	}
	
	@GetMapping("/promotions/active")
	public ResponseEntity<List<Promotion>> getPromotionsInActive(){
		List<Promotion> promotions = null;
		try {
			promotions = service.getPromotionInActive();
			return new ResponseEntity<List<Promotion>>(promotions, HttpStatus.OK);

		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<RoomPromotion>> searchByDescription(@RequestParam String keyword){
		List<RoomPromotion> promotions = null;
		try {
			promotions = service.searchPromotions(keyword);
			return new ResponseEntity<List<RoomPromotion>>(promotions, HttpStatus.OK);

		}catch (Exception ex) {
			return new ResponseEntity<List<RoomPromotion>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/new")
	public ResponseEntity<Long> savePromotion(@RequestBody RoomPromotion promotion){
		try {
			Date beginDate = promotion.getPromotion().getBeginDate();
			Date endDate = promotion.getPromotion().getEndDate();
			if(beginDate.compareTo(endDate) >= 0) {
				return new ResponseEntity<Long>(0L, HttpStatus.OK);
			}
			Long id = service.saveNew(promotion);
			return new ResponseEntity<Long>(id, HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Long>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@DeleteMapping("/deactive/{id}")
	public ResponseEntity<Long> savePromotion(@PathVariable Long id){
		try {
			service.deactive(id);
			return new ResponseEntity<Long>(HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
