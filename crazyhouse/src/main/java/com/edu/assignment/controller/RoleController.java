package com.edu.assignment.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.edu.assignment.entities.UserRole;
import com.edu.assignment.service.RoleService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
@RequestMapping("/role")
public class RoleController {
	@Autowired private RoleService service;
	@Autowired private Map<Long, String> sessionManager;
	
	@GetMapping("/roles/")
	public ResponseEntity<List<UserRole>> getAllRoles(){
		List<UserRole> roles = null;
		try {
		
			roles = service.getAllRoles();
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<List<UserRole>>(roles, HttpStatus.OK);
	}
	
	@GetMapping("/roles/{id}")
	public ResponseEntity<UserRole> getRoleById(@PathVariable int id){
		UserRole role = null;
		try {
			role = service.getRoleById(id);
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<UserRole>(role, HttpStatus.OK);
	}
	@GetMapping("/roles/getByName")
	public ResponseEntity<UserRole> getRoleByName(String name){
		UserRole role = null;
		try {
			role = service.getRoleByName(name);
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<UserRole>(role, HttpStatus.OK);
	}
	
	
	@PostMapping("/roles/add")
	public ResponseEntity<String> addRole (UserRole role){
		UserRole checkExistance = service.getRoleByName(role.getName());
		if(checkExistance == null) {
			service.addRole(role);
			return new ResponseEntity<String>("Adding successfully", HttpStatus.OK);
		}
		return new ResponseEntity<String>("Role already exists", HttpStatus.OK);
	}
	
}
