package com.edu.assignment.controller;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.edu.assignment.entities.UserAccount;
import com.edu.assignment.entities.UserInfo;
import com.edu.assignment.model.UserInfoWeb;
import com.edu.assignment.model.UserLogin;
import com.edu.assignment.service.UserAccountService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


@RestController
public class HelperController {
	
	@Autowired private UserAccountService service;
	
	@PostMapping("/login")
	public HttpEntity<UserInfoWeb> login(@RequestBody UserLogin user){
		String email = user.getEmail();
		String password = user.getPassword();
		UserAccount account = service.getUserByEmail(email);
		if(account !=null && account.getPassword().equals(password)) {
			UserInfo userInfo = account.getInfo();
			UserInfoWeb info = new UserInfoWeb(email);
			info.setUserid(userInfo.getId());
			info.setFullName(userInfo.getFirstName());
			info.setPhoto(userInfo.getPhoto());
			processJWTToken(info);
			return new ResponseEntity<UserInfoWeb>(info, HttpStatus.OK);
		}
		return new ResponseEntity<UserInfoWeb>(HttpStatus.OK);
	}
	
	@PostMapping("/admin/login")
	public HttpEntity<UserInfoWeb> adminLogin(@RequestBody UserLogin user){
		try {
		String email = user.getEmail();
		String password = user.getPassword();
		UserAccount account = service.getUserByEmail(email);
		if(account !=null && account.getPassword().equals(password) && account.getRole().getId() == 1) {
			UserInfo userInfo = account.getInfo();
			UserInfoWeb info = new UserInfoWeb(email);
			info.setUserid(userInfo.getId());
			info.setFullName(userInfo.getFirstName());
			info.setPhoto(userInfo.getPhoto());
			processJWTToken(info);
			return new ResponseEntity<UserInfoWeb>(info, HttpStatus.OK);
		}
		}catch(Exception ex) {
			return new ResponseEntity<UserInfoWeb>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<UserInfoWeb>(HttpStatus.FORBIDDEN);
	}
	
	private void processJWTToken(UserInfoWeb user) {
		String secretKey = "crazyhouse";
		Date expiry = new Date(System.currentTimeMillis() + 604800000);
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils
				.commaSeparatedStringToAuthorityList("ROLE_USER");
		
		String token = Jwts
				.builder()
				.setId("softtekJWT")
				.setSubject(user.getEmail())
				.claim("authorities",
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(expiry)
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();
		user.setAccessToken("Bearer " + token);
		user.setExpiry(expiry);
	}
}
