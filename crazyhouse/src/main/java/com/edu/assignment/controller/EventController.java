package com.edu.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.edu.assignment.entities.Event;
import com.edu.assignment.service.EventService;


@RestController
@RequestMapping("/event")
public class EventController {
	
	@Autowired
	private EventService eventService ;
	
	@GetMapping("/events/getAll")
	public ResponseEntity<List<Event>> getAllEvents(){
		List<Event> eventsInActive = null;
		try {
			eventsInActive = eventService.getAllEvent();
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<List<Event>>(eventsInActive, HttpStatus.OK);

	}
	
	@GetMapping("/events/active")
	public ResponseEntity<List<Event>> getEventsInActive(){
		List<Event> eventsInActive = null;
		try {
			eventsInActive = eventService.getEventInActive();
		}catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
		}
		return new ResponseEntity<List<Event>>(eventsInActive, HttpStatus.OK);

	}
	
	@PostMapping("/new")
	public ResponseEntity<Long> saveEvent(@RequestBody Event event){
		try {
			if(event.getEndDate().compareTo(event.getBeginDate()) <= 0) {
				return new ResponseEntity<Long>(0L, HttpStatus.OK);
			}
			Long id = eventService.save(event);
			return new ResponseEntity<Long>(id, HttpStatus.OK);
		}catch(Exception ex) {
			return new ResponseEntity<Long>(0L, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
}
