package com.edu.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.edu.assignment.entities.Combo;
import com.edu.assignment.model.ComboDTO;
import com.edu.assignment.service.ComboService;

@RestController
@RequestMapping("/combo")
public class ComboController {
	@Autowired private ComboService comboService;
	
	@GetMapping("/combos")
	public ResponseEntity<List<Combo>> getAllCombos(){
		List<Combo> combos = null;
		try {
			combos = comboService.getAllCombos();
			return new ResponseEntity<List<Combo>>(combos,HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Combo>>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/combos/{roomid}")
	public ResponseEntity<List<Combo>> getAllCombos(@PathVariable Long roomid){
		List<Combo> combos = null;
		try {
			combos = comboService.findByRoom(roomid);
			return new ResponseEntity<List<Combo>>(combos,HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<List<Combo>>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/new")
	public ResponseEntity<Integer> saveCombo(@RequestBody Combo combo){
		try {
			Integer id = comboService.addNew(combo);
			return new ResponseEntity<Integer>(id, HttpStatus.OK);
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ResponseEntity<Integer>(0, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
