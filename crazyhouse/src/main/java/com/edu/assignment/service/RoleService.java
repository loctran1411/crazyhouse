package com.edu.assignment.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edu.assignment.entities.UserRole;
import com.edu.assignment.repo.RoleRepository;

@Service
public class RoleService {
	
	@Autowired private RoleRepository roleRepository;
	
	public List<UserRole> getAllRoles(){
		Iterable<UserRole> roles = roleRepository.findAll();
		return StreamSupport
				  .stream(roles.spliterator(), false)
				  .collect(Collectors.toList());
	}
	
	public UserRole getRoleByName(String name){
		UserRole role = roleRepository.findByName(name);
		return role;
	}
	
	public UserRole getRoleById(int id) {
		UserRole user = roleRepository.findById(id);
		return user;
	}
	
	public void addRole(UserRole role) {
		roleRepository.save(role);
	}
}
