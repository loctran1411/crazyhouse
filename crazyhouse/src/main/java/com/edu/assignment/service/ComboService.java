package com.edu.assignment.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edu.assignment.entities.Combo;
import com.edu.assignment.entities.Room;
import com.edu.assignment.model.ComboDTO;
import com.edu.assignment.repo.ComboRepository;
import com.edu.assignment.repo.RoomRepository;

@Service
public class ComboService {
	@Autowired private ComboRepository comboRepository;
	@Autowired private RoomRepository roomRepository;
	
	public List<Combo> getAllCombos(){
		Iterable<Combo> combos = comboRepository.findAll();
		return StreamSupport
				  .stream(combos.spliterator(), false)
				  .collect(Collectors.toList());
	}
	
	public Combo getEventById(Integer id) {
		Combo combo = comboRepository.findById(id)
									 .orElse(null);
		return combo;
	}
	
	public Integer addNew(ComboDTO c) {
		Combo combo = new Combo();
		combo.setComboName(c.name);
		combo.setPrice(c.price);
		Room room = roomRepository.findById(c.getRoomId()).orElse(null);
		combo.setRoom(room);
		comboRepository.save(combo);
		return combo.getComboId();
	}
	
	public Integer addNew(Combo combo) {
		comboRepository.save(combo);
		return combo.getComboId();
	}
	
	public List<Combo> findByRoom(Long id){
		return comboRepository.searchByRoom(id);
	}
}
