package com.edu.assignment.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edu.assignment.entities.Promotion;
import com.edu.assignment.entities.RoomPromotion;
import com.edu.assignment.repo.PromotionRepository;
import com.edu.assignment.repo.RoomPromotionRepository;

@Service
public class PromotionService {
	@Autowired private PromotionRepository promotionRepository;
	@Autowired private RoomPromotionRepository rpRepository;

	
	public List<RoomPromotion> getAllPromotion(){
		Iterable<RoomPromotion> promotions = rpRepository.findAll();
		return StreamSupport
				  .stream(promotions.spliterator(), false)
				  .collect(Collectors.toList());
	}
	
	public Promotion getPromotionById(Long id) {
		Promotion prmotion = promotionRepository.findById(id).orElse(null);
		return prmotion;
	}
	
	public List<Promotion> getPromotionInActive(){
		List<Promotion> promotions = promotionRepository.findInActive();
		return promotions;
	}
	
	public List<RoomPromotion> searchPromotions(String keyword){
		List<RoomPromotion> promotions = rpRepository.searchPromotions(keyword);
		return promotions;
	}
	
	public Long saveNew(RoomPromotion rp) {

		RoomPromotion rr = rpRepository.save(rp);
		return rr.getId();
	}
	
	public void deactive(Long id) {
		promotionRepository.setIsDeleted(id);
	}
}
