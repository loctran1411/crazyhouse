package com.edu.assignment.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.edu.assignment.entities.BookingOnline;
import com.edu.assignment.entities.RoomBookingOnline;
import com.edu.assignment.model.RevenueReporter;
import com.edu.assignment.repo.BookingRepository;
import com.edu.assignment.repo.RoomBookingReposiotry;

@Service
public class BookingService {
	@Autowired private BookingRepository bookingRepository;
	@Autowired private RoomBookingReposiotry rbRepository;
	
	public List<BookingOnline> getAllBooking(){
		Iterable<BookingOnline> booking = bookingRepository.findAll();
		return StreamSupport
				  .stream(booking.spliterator(), false)
				  .collect(Collectors.toList());
	}
	
	public List<RoomBookingOnline> getAllBookingDetails(){
		Iterable<RoomBookingOnline> booking = rbRepository.findAll();
		return StreamSupport
				  .stream(booking.spliterator(), false)
				  .collect(Collectors.toList());
	}
	
	public List<BookingOnline> getBookingByPage(Pageable page){
		Iterable<BookingOnline> booking = bookingRepository.findAll(page);
		return StreamSupport
				  .stream(booking.spliterator(), false)
				  .collect(Collectors.toList());
	}
	
	public BookingOnline getBookingById(Long id) {
		BookingOnline booking = bookingRepository.findById(id)
												 .orElse(null);
		return booking;
	}
	
	public Long bookRoom(RoomBookingOnline book) {
		RoomBookingOnline newBook  = rbRepository.save(book);
		return newBook.getId();
	}
	
	public List<RoomBookingOnline> search(Long keyword){
		List<RoomBookingOnline> result = rbRepository.searchBookings(keyword);
		return result;
	}
	
	public List<RevenueReporter> reportRevenueByYear(){
		return rbRepository.reportRevenueByYear();
	}
	
	public List<RevenueReporter> reportRevenueByMonth(){
		return rbRepository.reportRevenueByMonth();
	}
}
