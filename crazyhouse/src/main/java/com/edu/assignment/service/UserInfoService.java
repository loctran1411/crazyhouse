package com.edu.assignment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edu.assignment.entities.UserInfo;
import com.edu.assignment.repo.UserInfoRepository;

@Service
public class UserInfoService {
	@Autowired private UserInfoRepository repo;
	
	public Long updateInfo(UserInfo info) {
		UserInfo t = repo.getOne(info.getId());
		t.setAge(info.getAge());
		t.setPhone(info.getPhone());
		t.setPhoto(info.getPhoto());
		UserInfo user = repo.save(t);
		return user.getId();
	}
}
