package com.edu.assignment.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edu.assignment.entities.UserAccount;
import com.edu.assignment.model.PasswordChanger;
import com.edu.assignment.repo.UserRepository;

@Service
public class UserAccountService {
	
	@Autowired private UserRepository userRepository;
	
	public List<UserAccount> getAllUserAccount(){
		Iterable<UserAccount> users = userRepository.findAll();
		return StreamSupport
				  .stream(users.spliterator(), false)
				  .collect(Collectors.toList());
	}
	
	public UserAccount getUserByEmail(String email){
		UserAccount user = userRepository.findByEmail(email);
		return user;
	}
	
	public UserAccount getUserById(Long id) {
		UserAccount user = userRepository.findById(id).orElse(null);
		return user;
	}
	
	public List<UserAccount> getUserByStatus(byte status){
		List<UserAccount> users = userRepository.findByStatus(status);
		return users;
	}
	
	public Long createNew(UserAccount acc) {
		UserAccount r = userRepository.save(acc);
		return r.getId();
	}
	
	public List<UserAccount> search(String keyword){
		List<UserAccount> lstAccounts = userRepository.searchAccount(keyword);
		return lstAccounts;
	}
	
	public Long changePwd(PasswordChanger pwdChanger) {
		UserAccount acc = userRepository.getOne(pwdChanger.id);
		acc.setPassword(pwdChanger.newPassword);
		userRepository.save(acc);
		return acc.getId();
	}
}
