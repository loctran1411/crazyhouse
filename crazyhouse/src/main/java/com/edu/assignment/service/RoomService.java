package com.edu.assignment.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edu.assignment.entities.Room;
import com.edu.assignment.repo.RoomRepository;

@Service
public class RoomService {
	@Autowired private RoomRepository roomRepository;
	
	public List<Room> getAllRoom(){
		Iterable<Room> room = roomRepository.findAll();
		return StreamSupport
				  .stream(room.spliterator(), false)
				  .collect(Collectors.toList());
	}
	
	
	public Room getRoomById(Long id) {
		Room room = roomRepository.findById(id)
									 .orElse(null);
		return room;
	}
	
	public List<Room> getRoomByStatus(byte status){
		List<Room> rooms = roomRepository.findByStatus(status);
		return rooms;
	}
	
	public Long addNew(Room room) {
		Room r =  roomRepository.save(room);
		return r.getId();
	}
	
	public void updateStatus(Room room) {
		roomRepository.save(room);
	}
}
