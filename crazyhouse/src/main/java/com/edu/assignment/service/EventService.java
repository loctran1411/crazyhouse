package com.edu.assignment.service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edu.assignment.entities.Event;
import com.edu.assignment.entities.Promotion;
import com.edu.assignment.repo.EventRepository;

@Service
public class EventService {
	@Autowired private EventRepository eventRepository;
	
	public List<Event> getAllEvent(){
		Iterable<Event> events = eventRepository.findAll();
		return StreamSupport
				  .stream(events.spliterator(), false)
				  .collect(Collectors.toList());
	}
	
	public Event getEventById(Long id) {
		Event event = eventRepository.findById(id)
									 .orElse(null);
		return event;
	}
	
	public List<Event> getEventInActive(){
		List<Event> promotions = eventRepository.findInActive();
		return promotions;
	}
	
	public Long save(Event event) {
		Event e = eventRepository.save(event);
		return e.getId();
	}
}
