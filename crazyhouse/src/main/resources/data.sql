-- Import user_role
INSERT INTO user_role(`created_date`,`description`,`name`)
VALUE(now(), 'super_user', 'admin');

INSERT INTO user_role(`created_date`,`description`,`name`)
VALUE(now(), 'customer', 'customer');

-- Import room
INSERT INTO room(`name`,`room_number`,`size`,`status`, `image`) 
VALUES('Phòng đập phá lớn', 201,'5x6', 0, 'dappha.jpg');

INSERT INTO room(`name`,`room_number`,`size`,`status`, `image`) 
VALUES('Phòng đấm bóc', 101,'5x4', 0, 'boxing.jpg');

INSERT INTO room(`name`,`room_number`,`size`,`status`, `image`)  
VALUES('Phòng phi tiêu', 102,'5x3', 0, 'phitieu.jpg');

INSERT INTO room(`name`,`room_number`,`size`,`status`, `image`) 
VALUES('Phòng đập phá nhỏ', 103,'5x4', 0, 'dapphanho.jpg');

INSERT INTO room(`name`,`room_number`,`size`,`status`, `image`) 
VALUES('Phòng tư vấn', 202,'5x4', 0, 'tuvan.jpg');

INSERT INTO room(`name`,`room_number`,`size`,`status`, `image`)  
VALUES('Phòng game thực tế ảo', 203,'5x4', 0, 'game.jpg');

-- Import combo
INSERT INTO combo(`combo_name`, `price`, `description`, `room_id`)
VALUES('normal', 50000, 'bình thường', 1);

INSERT INTO combo(`combo_name`, `price`, `description`, `room_id`)
VALUES('x3', 650000, 'thêm 15 chai coca, 1 bình sứ', 1);

INSERT INTO combo(`combo_name`, `price`, `description`, `room_id`)
VALUES('normal', 50000, 'bình thường', 2);

INSERT INTO combo(`combo_name`, `price`, `description`, `room_id`)
VALUES('normal', 50000, 'bình thường', 3);

INSERT INTO combo(`combo_name`, `price`, `description`, `room_id`)
VALUES('normal', 50000, 'bình thường', 4);

INSERT INTO combo(`combo_name`, `price`, `description`, `room_id`)
VALUES('normal', 50000, 'bình thường', 5);

INSERT INTO combo(`combo_name`, `price`, `description`, `room_id`)
VALUES('normal', 50000, 'bình thường', 6);

-- import user_info
insert into user_info(age,first_name,last_name,phone,photo)
value
(25,"Thế Tài","Trần","09400218113","1.jpg"),
(21,"Phi Nhật","Cao","09878719001","2.jpg"),
(21,"Thanh Toàn","Nguyễn","09382818990","3.jpg"),
(27,"Châu Tân","Võ","09382818990","6.jpg"),
(26,"Xuân Chi","Lê Thị","092181917181","7.jpg"),
(26,"Sandy","Nguyễn","092181677011","8.jpg"),
(34,"Văn Trung","Nguyễn","092181677011","9.jpg");


-- Import user_account
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'tai_fuckboy@gmail.com','123456789',1,now(),1,1);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`)  
VALUES(now(), 'nhatcao20@gmail.com','123456789',1,now(),1,2);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'toannguyen@gmail.com','toandepzai',1,now(),1,3);

INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'tuanbd@gmail.com','123456789',2,now(),2,1);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'tumonhon@gmail.com','123456789',2,now(),2,2);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'tan_chauvo@gmail.com','123456789',1,now(),2,4);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`)  
VALUES(now(), 'xuanchi@gmail.com','123456789',1,now(),2,5);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'sandynguyen@gmail.com','123456789',1,now(),2,6);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`)  
VALUES(now(), 'trungnguyen@gmail.com','123456789',1,now(),2,7);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`)  
VALUES(now(), 'robin@gmail.com','123456789',1,now(),2,4);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'hanhnguyen@gmail.com','123456789',1,now(),2,6);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'hanhtocdo@gmail.com','123456789',1,now(),2,7);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'longtran@gmail.com','123456789',0,now(),2,1);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'ngocpham@gmail.com','123456789',1,now(),2,2);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'trulz@gmail.com','123456789',0,now(),2,4);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`)  
VALUES(now(), 'huongxx@gmail.com','123456789',2,now(),2,6);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'tamdoc@gmail.com','123456789',0,now(),2,5);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'meolangthang@gmail.com','123456789',1,now(),2,2);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`)  
VALUES(now(), 'phachoi@gmail.com','123456789',1,now(),2,4);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'fpt@gmail.com','123456789',0,now(),2,3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'ancu@gmail.com','123456789',2,now(),2,1);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'thienpho@gmail.com','123456789',0,now(),2,5);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`, `info`) 
VALUES(now(), 'haileader@gmail.com','123456789',1,now(),2,7);

-- import promotion
INSERT INTO promotion(`begin_date`,`end_date`,`created_date`,`description`,`name`,`created_by`)
VALUES(now(), '2021-3-27 11:51:04', now(), 'Giảm 25% tất cả các phòng','discount 25%', 1);

INSERT INTO promotion(`begin_date`,`end_date`,`created_date`,`description`,`name`,`created_by`)
VALUES(now(), '2021-3-27 11:51:04', now(), 'Giảm 50% tất cả các phòng','discount 50%', 1);

INSERT INTO promotion(`begin_date`,`end_date`,`created_date`,`description`,`name`,`created_by`)
VALUES(now(), '2021-3-27 11:51:04', now(), 'Giảm 10% tất cả các phòng','discount 10%', 1);

INSERT INTO promotion(`begin_date`,`end_date`,`created_date`,`description`,`name`,`created_by`)
VALUES(now(), '2021-3-27 11:51:04', now(), 'Tặng voucher 20.000đ','voucher', 1);

-- import event
INSERT INTO event(`begin_date`,`end_date`,`created_date`,`content`,`event_name`,`created_by`,`image`)
VALUES(now(), '2021-04-05 23:59:00', now(), 'Tưng bùng khai trương','Khai trương', 1, 'open.jpg');


-- import promotion room
insert into room_promotion(room_id,promotion_id,discount)
values
(1,1,25),
(2,1,25),
(3,1,25),
(4,1,25),
(5,1,25),
(6,1,25);

insert into room_promotion(room_id,promotion_id,discount)
values
(1,2,50),
(2,2,50),
(3,2,50),
(4,2,50),
(5,2,50),
(6,2,50);

insert into room_promotion(room_id,promotion_id,discount)
values
(1,3,10),
(2,3,10),
(5,3,10),
(6,3,10);

insert into room_promotion(room_id,promotion_id,discount)
values
(6,4,20000);

-- import booking online
insert into booking_online(note,customer_id, created_by)
values
('',5, 1),
('',6, 1),
('',7, 1),
('',8, 1),
('',9, 1),
('',10, 1),
('',9, 1),
('',10, 1),
('',7, 1),
('',8, 1),
('',10, 1),
('',10, 1),
('',8, 2);

-- import booking room
insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (1, 1, '2021-01-01 09:00:00', 1, 1);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (2, 2, '2021-01-08 09:00:00', 1, 3);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (3, 3, '2021-01-17 09:00:00', 1, 4);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (4, 4, '2021-01-20 22:00:00', 1, 5);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (5, 5, '2021-01-21 09:00:00', 1, 6);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (6, 6, '2021-01-07 09:00:00', 1, 7);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (7, 1, '2021-01-03 18:00:00', 1, 1);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (8, 2, '2021-02-22 14:00:00', 1, 3);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (9, 6, '2021-02-01 09:00:00', 1, 7);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (10, 3, '2021-01-05 16:00:00', 1, 4);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (11, 1, '2021-01-02 15:00:00', 1, 1);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (12, 2, '2021-04-01 12:00:00', 1, 3);

insert into room_booking(`booking_id`,`room_id`,`booking_date`,`status`,`combo_id`)
values (13, 3, '2021-04-23 20:00:00', 1, 4);

