-- Import user_role
INSERT INTO user_role(`created_date`,`description`,`name`)
VALUE(now(), 'super_user', 'admin');

INSERT INTO user_role(`created_date`,`description`,`name`)
VALUE(now(), 'normal_user', 'user');

INSERT INTO user_role(`created_date`,`description`,`name`)
VALUE(now(), 'customer', 'customer');

-- Import room
INSERT INTO room(`name`,`price`,`room_number`,`size`,`status`) 
VALUES('Phòng đập phá lớn', 100000, 201,'5x6', 0);

INSERT INTO room(`name`,`price`,`room_number`,`size`,`status`) 
VALUES('Phòng đấm bóc', 50000, 101,'5x4', 0);

INSERT INTO room(`name`,`price`,`room_number`,`size`,`status`) 
VALUES('Phòng phi tiêu', 40000, 102,'5x3', 0);

INSERT INTO room(`name`,`price`,`room_number`,`size`,`status`) 
VALUES('Phòng đập phá nhỏ', 80000, 103,'5x4', 0);

INSERT INTO room(`name`,`price`,`room_number`,`size`,`status`) 
VALUES('Phòng tư vấn', 120000, 202,'5x4', 0);

INSERT INTO room(`name`,`price`,`room_number`,`size`,`status`) 
VALUES('Phòng game thực tế ảo', 130000, 203,'5x4', 0);

-- Import combo
INSERT INTO combo(`name`, `price`, `description`)
VALUES('normal', '10000', 'thêm 5 chai coca');

INSERT INTO combo(`name`, `price`, `description`)
VALUES('x3', '30000', 'thêm 15 chai coca,bonus 1 bình sứ');


-- Import user_account
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'tai_fuckboy@gmail.com','123456789',1,now(),1);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'nhatcao20@gmail.com','123456789',1,now(),1);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'toannguyen@gmail.com','toandepzai',1,now(),1);

INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'tuanbd@gmail.com','123456789',2,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'tumonhon@gmail.com','123456789',2,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'tan_chauvo@gmail.com','123456789',1,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'xuanchi@gmail.com','123456789',1,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'sandynguyen@gmail.com','123456789',1,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'trungnguyen@gmail.com','123456789',1,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'robin@gmail.com','123456789',1,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'hanhnguyen@gmail.com','123456789',1,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'hanhtocdo@gmail.com','123456789',1,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'longtran@gmail.com','123456789',0,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'ngocpham@gmail.com','123456789',1,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'trulz@gmail.com','123456789',0,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'huongxx@gmail.com','123456789',2,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'tamdoc@gmail.com','123456789',0,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'meolangthang@gmail.com','123456789',1,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'phachoi@gmail.com','123456789',1,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'fpt@gmail.com','123456789',0,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'ancu@gmail.com','123456789',2,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'thienpho@gmail.com','123456789',0,now(),3);
INSERT INTO user_account(`created_date`,`email`,`password`,`status`,`updated_date`,`role_id`) 
VALUES(now(), 'haileader@gmail.com','123456789',1,now(),3);

-- import promotion
INSERT INTO promotion(`begin_date`,`end_date`,`created_date`,`description`,`name`,`created_by`)
VALUES(now(), '2021-3-27 11:51:04', now(), 'Giảm 25% tất cả các phòng','discount 25%', 1);

INSERT INTO promotion(`begin_date`,`end_date`,`created_date`,`description`,`name`,`created_by`)
VALUES(now(), '2021-3-27 11:51:04', now(), 'Giảm 50% tất cả các phòng','discount 50%', 1);

INSERT INTO promotion(`begin_date`,`end_date`,`created_date`,`description`,`name`,`created_by`)
VALUES(now(), '2021-3-27 11:51:04', now(), 'Giảm 10% tất cả các phòng','discount 10%', 1);

INSERT INTO promotion(`begin_date`,`end_date`,`created_date`,`description`,`name`,`created_by`)
VALUES(now(), '2021-3-27 11:51:04', now(), 'Tặng voucher 20.000đ','voucher', 1);

-- import event
INSERT INTO event(`begin_date`,`end_date`,`created_date`,`content`,`event_name`,`created_by`,`image`,`is_home`)
VALUES(now(), '2021-3-27 11:51:04', now(), 'Tưng bùng khai trương','Khai trương', 1, 'open.jpg',true);


-- import promotion room
insert into room_promotion(room_id,promotion_id,discount)
values
(1,1,25),
(2,1,25),
(3,1,25),
(4,1,25),
(5,1,25),
(6,1,25);

-- import user_info
insert into user_info(age,first_name,last_name,phone,photo,user_id)
value
(25,"Thế Tài","Trần","09400218113","1.jpg",1),
(21,"Phi Nhật","Cao","09878719001","2.jpg",2),
(21,"Thanh Toàn","Nguyễn","09382818990","3.jpg",3),
(27,"Châu Tân","Võ","09382818990","6.jpg",6),
(26,"Xuân Chi","Lê Thị","092181917181","7.jpg",7),
(26,"Sandy","Nguyễn","092181677011","8.jpg",8),
(34,"Văn Trung","Nguyễn","092181677011","9.jpg",9);
