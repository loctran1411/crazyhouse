module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/auth/signin.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/master/breadcrumbs.js":
/*!******************************************!*\
  !*** ./components/master/breadcrumbs.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_Breadcrumbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Breadcrumbs */ "@material-ui/core/Breadcrumbs");
/* harmony import */ var _material_ui_core_Breadcrumbs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Breadcrumbs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Typography */ "@material-ui/core/Typography");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Link */ "@material-ui/core/Link");
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/icons/NavigateNext */ "@material-ui/icons/NavigateNext");
/* harmony import */ var _material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_4__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\components\\master\\breadcrumbs.js";





function BreadCrumb() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
    className: "py-2 ml-4",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Breadcrumbs__WEBPACK_IMPORTED_MODULE_1___default.a, {
      separator: "\u203A",
      "aria-label": "breadcrumb",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_3___default.a, {
        color: "inherit",
        href: "/",
        children: "Home"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 17
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2___default.a, {
        color: "textPrimary",
        children: "Account"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 13
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 8,
    columnNumber: 9
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (BreadCrumb);

/***/ }),

/***/ "./components/master/mainmenu.js":
/*!***************************************!*\
  !*** ./components/master/mainmenu.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Paper */ "@material-ui/core/Paper");
/* harmony import */ var _material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Tabs */ "@material-ui/core/Tabs");
/* harmony import */ var _material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/Tab */ "@material-ui/core/Tab");
/* harmony import */ var _material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_icons_PeopleOutlineRounded__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/icons/PeopleOutlineRounded */ "@material-ui/icons/PeopleOutlineRounded");
/* harmony import */ var _material_ui_icons_PeopleOutlineRounded__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_PeopleOutlineRounded__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_icons_RoomServiceRounded__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/icons/RoomServiceRounded */ "@material-ui/icons/RoomServiceRounded");
/* harmony import */ var _material_ui_icons_RoomServiceRounded__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_RoomServiceRounded__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _material_ui_icons_MeetingRoomRounded__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/icons/MeetingRoomRounded */ "@material-ui/icons/MeetingRoomRounded");
/* harmony import */ var _material_ui_icons_MeetingRoomRounded__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_MeetingRoomRounded__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _material_ui_icons_StarRounded__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/icons/StarRounded */ "@material-ui/icons/StarRounded");
/* harmony import */ var _material_ui_icons_StarRounded__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_StarRounded__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _material_ui_icons_RedeemRounded__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/icons/RedeemRounded */ "@material-ui/icons/RedeemRounded");
/* harmony import */ var _material_ui_icons_RedeemRounded__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_RedeemRounded__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _material_ui_icons_LocalOfferRounded__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/icons/LocalOfferRounded */ "@material-ui/icons/LocalOfferRounded");
/* harmony import */ var _material_ui_icons_LocalOfferRounded__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_LocalOfferRounded__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _material_ui_icons_Assessment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/icons/Assessment */ "@material-ui/icons/Assessment");
/* harmony import */ var _material_ui_icons_Assessment__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Assessment__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _material_ui_core_Card__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/core/Card */ "@material-ui/core/Card");
/* harmony import */ var _material_ui_core_Card__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Card__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _material_ui_icons_Person__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/icons/Person */ "@material-ui/icons/Person");
/* harmony import */ var _material_ui_icons_Person__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Person__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! next-auth/client */ "next-auth/client");
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(next_auth_client__WEBPACK_IMPORTED_MODULE_15__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\components\\master\\mainmenu.js";
















function MainMenu() {
  var _session$user;

  const [session] = Object(next_auth_client__WEBPACK_IMPORTED_MODULE_15__["useSession"])();
  const [value, setValue] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState();
  const router = Object(next_router__WEBPACK_IMPORTED_MODULE_14__["useRouter"])();
  const {
    tab
  } = router.query;
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Card__WEBPACK_IMPORTED_MODULE_12___default.a, {
    variant: "outlined",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_2___default.a, {
      square: true,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_3___default.a, {
        value: parseInt(tab ? tab : '0'),
        textColor: "primary",
        indicatorColor: "primary",
        "aria-label": "disabled tabs example",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/profile",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_Person__WEBPACK_IMPORTED_MODULE_13___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 32,
              columnNumber: 77
            }, this),
            label: "welcome, " + (session === null || session === void 0 ? void 0 : (_session$user = session.user) === null || _session$user === void 0 ? void 0 : _session$user.fullName),
            className: "text-indigo-600"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 32,
            columnNumber: 66
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/account?tab=1",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Account",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_PeopleOutlineRounded__WEBPACK_IMPORTED_MODULE_5___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 33,
              columnNumber: 99
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 72
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/booking?tab=2",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Booking",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_RoomServiceRounded__WEBPACK_IMPORTED_MODULE_6___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 34,
              columnNumber: 99
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 34,
            columnNumber: 72
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/room?tab=3",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Room",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_MeetingRoomRounded__WEBPACK_IMPORTED_MODULE_7___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 35,
              columnNumber: 93
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 35,
            columnNumber: 69
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 35,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/combo?tab=4",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Combo",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_LocalOfferRounded__WEBPACK_IMPORTED_MODULE_10___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 36,
              columnNumber: 95
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 36,
            columnNumber: 70
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 36,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/promotion?tab=5",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Promotion",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_RedeemRounded__WEBPACK_IMPORTED_MODULE_9___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 37,
              columnNumber: 103
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 37,
            columnNumber: 74
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 37,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/event?tab=6",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Event",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_StarRounded__WEBPACK_IMPORTED_MODULE_8___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 38,
              columnNumber: 95
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 38,
            columnNumber: 70
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 38,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/report?tab=7",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Report",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_Assessment__WEBPACK_IMPORTED_MODULE_11___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 39,
              columnNumber: 97
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 39,
            columnNumber: 71
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 39,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 11
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 9
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 24,
    columnNumber: 9
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (MainMenu);

/***/ }),

/***/ "./components/master/navbar.js":
/*!*************************************!*\
  !*** ./components/master/navbar.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_icons_PetsRounded__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/icons/PetsRounded */ "@material-ui/icons/PetsRounded");
/* harmony import */ var _material_ui_icons_PetsRounded__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_PetsRounded__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next-auth/client */ "next-auth/client");
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_auth_client__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/Link */ "@material-ui/core/Link");
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\components\\master\\navbar.js";






function Navbar() {
  // let [isShowMenu, setIsShowMenu] = useState(false);
  const logoutHandle = e => {
    e.preventDefault();
    Object(next_auth_client__WEBPACK_IMPORTED_MODULE_3__["signOut"])();
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("nav", {
    class: "flex items-center justify-between flex-wrap bg-green-500 px-6 py-1",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_4___default.a, {
      href: "/",
      color: "inherit",
      underline: "none",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        class: "flex items-center flex-shrink-0 text-white mr-6",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_PetsRounded__WEBPACK_IMPORTED_MODULE_2___default.a, {
          className: "mr-2"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 21,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
          class: "font-semibold text-xl tracking-tight",
          children: "Crazy House"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 22,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 20,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      class: "block lg:hidden",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
        class: "flex items-center px-3 py-2 border rounded text-green-200 border-teal-400 hover:text-white hover:border-white",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("svg", {
          class: "fill-current h-3 w-3",
          viewBox: "0 0 20 20",
          xmlns: "http://www.w3.org/2000/svg",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
            children: "Menu"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 32,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("path", {
            d: "M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 27,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 18,
    columnNumber: 5
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (Navbar);

/***/ }),

/***/ "./pages/auth/signin.js":
/*!******************************!*\
  !*** ./pages/auth/signin.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return SignIn; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Button */ "@material-ui/core/Button");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/TextField */ "@material-ui/core/TextField");
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/FormControlLabel */ "@material-ui/core/FormControlLabel");
/* harmony import */ var _material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_FormControlLabel__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core/Checkbox */ "@material-ui/core/Checkbox");
/* harmony import */ var _material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Checkbox__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/Link */ "@material-ui/core/Link");
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core/Grid */ "@material-ui/core/Grid");
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core/Box */ "@material-ui/core/Box");
/* harmony import */ var _material_ui_core_Box__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Box__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/core/Typography */ "@material-ui/core/Typography");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! next-auth/client */ "next-auth/client");
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(next_auth_client__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/lab/Alert */ "@material-ui/lab/Alert");
/* harmony import */ var _material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @material-ui/core/InputAdornment */ "@material-ui/core/InputAdornment");
/* harmony import */ var _material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var _material_ui_icons_Email__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @material-ui/icons/Email */ "@material-ui/icons/Email");
/* harmony import */ var _material_ui_icons_Email__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Email__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var _material_ui_icons_Https__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @material-ui/icons/Https */ "@material-ui/icons/Https");
/* harmony import */ var _material_ui_icons_Https__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Https__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var _material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @material-ui/icons/AccountCircle */ "@material-ui/icons/AccountCircle");
/* harmony import */ var _material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_17___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_17__);
/* harmony import */ var _changepass__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../changepass */ "./pages/changepass.js");

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\pages\\auth\\signin.js";





















function Copyright() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_9___default.a, {
    variant: "body2",
    color: "textSecondary",
    align: "center",
    children: ["Copyright © ", /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_6___default.a, {
      href: "#",
      className: "text-red-500",
      children: "Crazyhouse"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 7
    }, this), " ", new Date().getFullYear(), "."]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 23,
    columnNumber: 5
  }, this);
}

const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_10__["makeStyles"])(theme => ({
  paper: {
    marginTop: theme.spacing(2),
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%",
    // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));
function SignIn({
  csrfToken
}) {
  const classes = useStyles();
  const {
    0: email,
    1: setEmail
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: pwd,
    1: setPwd
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const {
    0: isRemember,
    1: setIsRemember
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(false);
  const {
    0: loginError,
    1: setLoginError
  } = Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])("");
  const router = Object(next_router__WEBPACK_IMPORTED_MODULE_12__["useRouter"])();
  Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(() => {
    if (router.query.error) {
      setLoginError(router.query.error); // Shown below the input field in my example
      // setEmail(router.query.email)
    }
  }, [router]);

  const handleLogin = e => {
    e.preventDefault();

    if (email && pwd) {
      Object(next_auth_client__WEBPACK_IMPORTED_MODULE_11__["signIn"])('credentials', {
        email: email,
        password: pwd
      });
    }

    return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      className: "border rounded-md p-2 mt-4 w-96 mx-auto border-gray-300 ring-4",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: classes.paper,
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
          className: "flex justify-center content-center",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_AccountCircle__WEBPACK_IMPORTED_MODULE_17___default.a, {
            className: "text-blue-600 mr-2",
            fontSize: "large"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 81,
            columnNumber: 11
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
            className: "text-2xl text-blue-400",
            children: "Sign in"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 82,
            columnNumber: 11
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 80,
          columnNumber: 9
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("form", {
          className: classes.form,
          method: "POST",
          action: "/api/auth/callback/credentials",
          noValidate: true,
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
            name: "csrfToken",
            type: "hidden",
            defaultValue: csrfToken
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 90,
            columnNumber: 11
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3___default.a // variant="outlined"
          , {
            margin: "normal",
            required: true,
            fullWidth: true,
            id: "email",
            label: "Email Address",
            name: "email",
            type: "email",
            autoComplete: "email",
            value: email,
            autoFocus: true,
            required: true,
            InputProps: {
              startAdornment: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_14___default.a, {
                position: "start",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_Email__WEBPACK_IMPORTED_MODULE_15___default.a, {
                  className: "text-blue-500"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 107,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 106,
                columnNumber: 17
              }, this)
            },
            onChange: e => setEmail(e.target.value)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 91,
            columnNumber: 11
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_3___default.a // variant="outlined"
          , {
            margin: "normal",
            required: true,
            fullWidth: true,
            name: "password",
            label: "Password",
            type: "password",
            id: "password",
            autoComplete: "current-password",
            InputProps: {
              startAdornment: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_InputAdornment__WEBPACK_IMPORTED_MODULE_14___default.a, {
                position: "start",
                children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_Https__WEBPACK_IMPORTED_MODULE_16___default.a, {
                  className: "text-blue-500"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 126,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 125,
                columnNumber: 17
              }, this)
            },
            onChange: e => setPwd(e.target.value),
            required: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 113,
            columnNumber: 11
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("label", {
            children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("input", {
              type: "checkbox",
              className: "mr-2",
              checked: isRemember,
              onChange: e => setIsRemember(e.target.value)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 134,
              columnNumber: 13
            }, this), "Remember me"]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 133,
            columnNumber: 11
          }, this), loginError && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_13___default.a, {
            severity: "error",
            variant: "outlined",
            className: "my-2",
            children: loginError
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 144,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_2___default.a, {
            type: "submit",
            fullWidth: true,
            variant: "contained",
            color: "primary",
            className: classes.submit,
            onSubmit: e => handleLogin(e),
            children: "Sign In"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 148,
            columnNumber: 11
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 84,
          columnNumber: 9
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 79,
        columnNumber: 7
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "my-2",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(Copyright, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 161,
          columnNumber: 9
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 160,
        columnNumber: 7
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 78,
      columnNumber: 5
    }, this);
  };
} // SignIn.getInitialProps = async (context) => {
//   return {
//     csrfToken: await csrfToken(context),
//   };
// };

/***/ }),

/***/ "./pages/changepass.js":
/*!*****************************!*\
  !*** ./pages/changepass.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ChangePass; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_master_navbar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/master/navbar */ "./components/master/navbar.js");
/* harmony import */ var _components_master_breadcrumbs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/master/breadcrumbs */ "./components/master/breadcrumbs.js");
/* harmony import */ var _components_master_mainmenu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/master/mainmenu */ "./components/master/mainmenu.js");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/core/Button */ "@material-ui/core/Button");
/* harmony import */ var _material_ui_core_Button__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/core/TextField */ "@material-ui/core/TextField");
/* harmony import */ var _material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/core/Grid */ "@material-ui/core/Grid");
/* harmony import */ var _material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! next-auth/client */ "next-auth/client");
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(next_auth_client__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/lab/Alert */ "@material-ui/lab/Alert");
/* harmony import */ var _material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _common_validateutils__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./common/validateutils */ "./pages/common/validateutils.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_12__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\pages\\changepass.js";












function ChangePass() {
  const [session] = Object(next_auth_client__WEBPACK_IMPORTED_MODULE_9__["useSession"])();
  const [message, setMessage] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState('');
  const [errorText, setErrorText] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState('');
  const [error, setError] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState(false);
  const [oldpwd, setOldPwd] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState("");
  const [newpwd, setNewPwd] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState("");
  const [confirmpwd, setConfirmPwd] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState("");
  const [pwdErr, SetPwdErr] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState(false);
  const [newpwdErr, SetNewPwdErr] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState(false);
  const [pwdconfirmErr, SetConfirmPwdErr] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState(false);
  const [matchErr, SetMatchErr] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState(false);

  const validFields = () => {
    if (Object(_common_validateutils__WEBPACK_IMPORTED_MODULE_11__["isEmpty"])(oldpwd, SetPwdErr)) {
      return true;
    }

    if (Object(_common_validateutils__WEBPACK_IMPORTED_MODULE_11__["isEmpty"])(newpwd, SetNewPwdErr)) {
      return true;
    }

    if (Object(_common_validateutils__WEBPACK_IMPORTED_MODULE_11__["isEmpty"])(confirmpwd, SetConfirmPwdErr)) {
      return true;
    }

    if (newpwd != confirmpwd) {
      SetMatchErr(true);
      return true;
    } else {
      SetMatchErr(false);
    }

    return false;
  };

  const handleChangePwd = e => {
    e.preventDefault();

    if (!validFields()) {
      var _session$user;

      const options = {
        method: 'POST',
        headers: {
          'content-type': 'application/json',
          'Authorization': session.accessToken
        },
        data: {
          id: session === null || session === void 0 ? void 0 : (_session$user = session.user) === null || _session$user === void 0 ? void 0 : _session$user.userid,
          oldPassword: oldpwd,
          newPassword: newpwd
        },
        baseURL: 'http://localhost:8080/api/',
        url: 'user/changepwd'
      };
      axios__WEBPACK_IMPORTED_MODULE_12___default()(options).then(function (response) {
        if (response.data == 0) {
          setError(false);
          SetPwdErr(true);
          setErrorText("Old password isn't match");
        } else {
          setError(false);
          setMessage("Updated successfully!");
          setErrorText("");
        }
      }).catch(function (error) {
        setError(true);
        setMessage("Error");
      });
    }
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
    className: "",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Crazy Web"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 82,
        columnNumber: 17
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("link", {
        rel: "icon",
        href: "/favicon.ico"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 83,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 81,
      columnNumber: 13
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("main", {
      className: "h-screen",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_master_navbar__WEBPACK_IMPORTED_MODULE_3__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 86,
        columnNumber: 17
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_master_breadcrumbs__WEBPACK_IMPORTED_MODULE_4__["default"], {
        className: "bg-gray-400"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 87,
        columnNumber: 17
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_master_mainmenu__WEBPACK_IMPORTED_MODULE_5__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 88,
        columnNumber: 17
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("form", {
        className: "w-96 mx-auto border p-4 mt-4",
        children: [message && /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_lab_Alert__WEBPACK_IMPORTED_MODULE_10___default.a, {
          color: error ? "error" : "success",
          severity: error ? "error" : "success",
          className: "pb-2",
          children: message
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 90,
          columnNumber: 33
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_8___default.a, {
          item: true,
          xs: 12,
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7___default.a, {
            id: "standard-basic",
            label: "Old Password",
            type: "password",
            onChange: e => setOldPwd(e.target.value),
            error: pwdErr,
            helperText: pwdErr ? errorText : "",
            autoFocus: true,
            required: true,
            fullWidth: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 92,
            columnNumber: 21
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 91,
          columnNumber: 21
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 103,
          columnNumber: 21
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_8___default.a, {
          item: true,
          xs: 12,
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7___default.a, {
            id: "standard-basic",
            label: "New Password",
            type: "password",
            error: newpwdErr,
            helperText: newpwdErr ? "New password invalid" : "",
            onChange: e => setNewPwd(e.target.value),
            required: true,
            fullWidth: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 105,
            columnNumber: 21
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 104,
          columnNumber: 21
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 116,
          columnNumber: 21
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_8___default.a, {
          item: true,
          xs: 12,
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_TextField__WEBPACK_IMPORTED_MODULE_7___default.a, {
            id: "standard-basic",
            label: "Confirm Password",
            type: "password",
            error: pwdconfirmErr || matchErr,
            helperText: pwdconfirmErr ? "Confirm password invalid" : matchErr ? "Confirm password not match" : "",
            onChange: e => setConfirmPwd(e.target.value),
            required: true,
            fullWidth: true
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 118,
            columnNumber: 21
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 117,
          columnNumber: 21
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("br", {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 128,
          columnNumber: 21
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Grid__WEBPACK_IMPORTED_MODULE_8___default.a, {
          item: true,
          xs: 12,
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Button__WEBPACK_IMPORTED_MODULE_6___default.a, {
            onClick: e => handleChangePwd(e),
            type: "submit",
            fullWidth: true,
            variant: "contained",
            color: "primary" // className={classes.submit}
            ,
            children: "Confirm"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 130,
            columnNumber: 21
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 129,
          columnNumber: 21
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 89,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 85,
      columnNumber: 13
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 80,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./pages/common/validateutils.js":
/*!***************************************!*\
  !*** ./pages/common/validateutils.js ***!
  \***************************************/
/*! exports provided: isEmpty, isEmail, isNumber */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isEmpty", function() { return isEmpty; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isEmail", function() { return isEmail; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNumber", function() { return isNumber; });
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-ui/icons */ "@material-ui/icons");
/* harmony import */ var _material_ui_icons__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons__WEBPACK_IMPORTED_MODULE_0__);

const regexEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
const regexOnlyNumber = /^[0-9]*$/g; // const isEmpty = (value) => value.trim() == '' ? true : false

const isEmpty = (value, setState) => {
  if (value.trim() == '') {
    setState(true);
    return true;
  }

  setState(false);
  return false;
};

const isEmail = value => regexEmail.test(value);

const isNumber = value => regexOnlyNumber.test(value);



/***/ }),

/***/ "@material-ui/core":
/*!************************************!*\
  !*** external "@material-ui/core" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core");

/***/ }),

/***/ "@material-ui/core/Box":
/*!****************************************!*\
  !*** external "@material-ui/core/Box" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Box");

/***/ }),

/***/ "@material-ui/core/Breadcrumbs":
/*!************************************************!*\
  !*** external "@material-ui/core/Breadcrumbs" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Breadcrumbs");

/***/ }),

/***/ "@material-ui/core/Button":
/*!*******************************************!*\
  !*** external "@material-ui/core/Button" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Button");

/***/ }),

/***/ "@material-ui/core/Card":
/*!*****************************************!*\
  !*** external "@material-ui/core/Card" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Card");

/***/ }),

/***/ "@material-ui/core/Checkbox":
/*!*********************************************!*\
  !*** external "@material-ui/core/Checkbox" ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Checkbox");

/***/ }),

/***/ "@material-ui/core/FormControlLabel":
/*!*****************************************************!*\
  !*** external "@material-ui/core/FormControlLabel" ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/FormControlLabel");

/***/ }),

/***/ "@material-ui/core/Grid":
/*!*****************************************!*\
  !*** external "@material-ui/core/Grid" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Grid");

/***/ }),

/***/ "@material-ui/core/InputAdornment":
/*!***************************************************!*\
  !*** external "@material-ui/core/InputAdornment" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/InputAdornment");

/***/ }),

/***/ "@material-ui/core/Link":
/*!*****************************************!*\
  !*** external "@material-ui/core/Link" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Link");

/***/ }),

/***/ "@material-ui/core/Paper":
/*!******************************************!*\
  !*** external "@material-ui/core/Paper" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Paper");

/***/ }),

/***/ "@material-ui/core/Tab":
/*!****************************************!*\
  !*** external "@material-ui/core/Tab" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Tab");

/***/ }),

/***/ "@material-ui/core/Tabs":
/*!*****************************************!*\
  !*** external "@material-ui/core/Tabs" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Tabs");

/***/ }),

/***/ "@material-ui/core/TextField":
/*!**********************************************!*\
  !*** external "@material-ui/core/TextField" ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/TextField");

/***/ }),

/***/ "@material-ui/core/Typography":
/*!***********************************************!*\
  !*** external "@material-ui/core/Typography" ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Typography");

/***/ }),

/***/ "@material-ui/core/styles":
/*!*******************************************!*\
  !*** external "@material-ui/core/styles" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/styles");

/***/ }),

/***/ "@material-ui/icons":
/*!*************************************!*\
  !*** external "@material-ui/icons" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons");

/***/ }),

/***/ "@material-ui/icons/AccountCircle":
/*!***************************************************!*\
  !*** external "@material-ui/icons/AccountCircle" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/AccountCircle");

/***/ }),

/***/ "@material-ui/icons/Assessment":
/*!************************************************!*\
  !*** external "@material-ui/icons/Assessment" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Assessment");

/***/ }),

/***/ "@material-ui/icons/Email":
/*!*******************************************!*\
  !*** external "@material-ui/icons/Email" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Email");

/***/ }),

/***/ "@material-ui/icons/Https":
/*!*******************************************!*\
  !*** external "@material-ui/icons/Https" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Https");

/***/ }),

/***/ "@material-ui/icons/LocalOfferRounded":
/*!*******************************************************!*\
  !*** external "@material-ui/icons/LocalOfferRounded" ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/LocalOfferRounded");

/***/ }),

/***/ "@material-ui/icons/MeetingRoomRounded":
/*!********************************************************!*\
  !*** external "@material-ui/icons/MeetingRoomRounded" ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/MeetingRoomRounded");

/***/ }),

/***/ "@material-ui/icons/NavigateNext":
/*!**************************************************!*\
  !*** external "@material-ui/icons/NavigateNext" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/NavigateNext");

/***/ }),

/***/ "@material-ui/icons/PeopleOutlineRounded":
/*!**********************************************************!*\
  !*** external "@material-ui/icons/PeopleOutlineRounded" ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/PeopleOutlineRounded");

/***/ }),

/***/ "@material-ui/icons/Person":
/*!********************************************!*\
  !*** external "@material-ui/icons/Person" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Person");

/***/ }),

/***/ "@material-ui/icons/PetsRounded":
/*!*************************************************!*\
  !*** external "@material-ui/icons/PetsRounded" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/PetsRounded");

/***/ }),

/***/ "@material-ui/icons/RedeemRounded":
/*!***************************************************!*\
  !*** external "@material-ui/icons/RedeemRounded" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/RedeemRounded");

/***/ }),

/***/ "@material-ui/icons/RoomServiceRounded":
/*!********************************************************!*\
  !*** external "@material-ui/icons/RoomServiceRounded" ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/RoomServiceRounded");

/***/ }),

/***/ "@material-ui/icons/StarRounded":
/*!*************************************************!*\
  !*** external "@material-ui/icons/StarRounded" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/StarRounded");

/***/ }),

/***/ "@material-ui/lab/Alert":
/*!*****************************************!*\
  !*** external "@material-ui/lab/Alert" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/lab/Alert");

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "next-auth/client":
/*!***********************************!*\
  !*** external "next-auth/client" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-auth/client");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9tYXN0ZXIvYnJlYWRjcnVtYnMuanMiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9tYXN0ZXIvbWFpbm1lbnUuanMiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9tYXN0ZXIvbmF2YmFyLmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2F1dGgvc2lnbmluLmpzIiwid2VicGFjazovLy8uL3BhZ2VzL2NoYW5nZXBhc3MuanMiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvY29tbW9uL3ZhbGlkYXRldXRpbHMuanMiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2NvcmVcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZS9Cb3hcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZS9CcmVhZGNydW1ic1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9jb3JlL0J1dHRvblwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9jb3JlL0NhcmRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZS9DaGVja2JveFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9jb3JlL0Zvcm1Db250cm9sTGFiZWxcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZS9HcmlkXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2NvcmUvSW5wdXRBZG9ybm1lbnRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZS9MaW5rXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2NvcmUvUGFwZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZS9UYWJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZS9UYWJzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2NvcmUvVGV4dEZpZWxkXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2NvcmUvVHlwb2dyYXBoeVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9jb3JlL3N0eWxlc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9pY29uc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9pY29ucy9BY2NvdW50Q2lyY2xlXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2ljb25zL0Fzc2Vzc21lbnRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvRW1haWxcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvSHR0cHNcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvTG9jYWxPZmZlclJvdW5kZWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvTWVldGluZ1Jvb21Sb3VuZGVkXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2ljb25zL05hdmlnYXRlTmV4dFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9pY29ucy9QZW9wbGVPdXRsaW5lUm91bmRlZFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9pY29ucy9QZXJzb25cIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvUGV0c1JvdW5kZWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvUmVkZWVtUm91bmRlZFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9pY29ucy9Sb29tU2VydmljZVJvdW5kZWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvU3RhclJvdW5kZWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvbGFiL0FsZXJ0XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiYXhpb3NcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuZXh0LWF1dGgvY2xpZW50XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9oZWFkXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9yb3V0ZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIl0sIm5hbWVzIjpbIkJyZWFkQ3J1bWIiLCJNYWluTWVudSIsInNlc3Npb24iLCJ1c2VTZXNzaW9uIiwidmFsdWUiLCJzZXRWYWx1ZSIsIlJlYWN0IiwidXNlU3RhdGUiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJ0YWIiLCJxdWVyeSIsInBhcnNlSW50IiwidXNlciIsImZ1bGxOYW1lIiwiTmF2YmFyIiwibG9nb3V0SGFuZGxlIiwiZSIsInByZXZlbnREZWZhdWx0Iiwic2lnbk91dCIsIkNvcHlyaWdodCIsIkRhdGUiLCJnZXRGdWxsWWVhciIsInVzZVN0eWxlcyIsIm1ha2VTdHlsZXMiLCJ0aGVtZSIsInBhcGVyIiwibWFyZ2luVG9wIiwic3BhY2luZyIsImRpc3BsYXkiLCJmbGV4RGlyZWN0aW9uIiwiYWxpZ25JdGVtcyIsImF2YXRhciIsIm1hcmdpbiIsImJhY2tncm91bmRDb2xvciIsInBhbGV0dGUiLCJzZWNvbmRhcnkiLCJtYWluIiwiZm9ybSIsIndpZHRoIiwic3VibWl0IiwiU2lnbkluIiwiY3NyZlRva2VuIiwiY2xhc3NlcyIsImVtYWlsIiwic2V0RW1haWwiLCJwd2QiLCJzZXRQd2QiLCJpc1JlbWVtYmVyIiwic2V0SXNSZW1lbWJlciIsImxvZ2luRXJyb3IiLCJzZXRMb2dpbkVycm9yIiwidXNlRWZmZWN0IiwiZXJyb3IiLCJoYW5kbGVMb2dpbiIsInNpZ25JbiIsInBhc3N3b3JkIiwic3RhcnRBZG9ybm1lbnQiLCJ0YXJnZXQiLCJDaGFuZ2VQYXNzIiwibWVzc2FnZSIsInNldE1lc3NhZ2UiLCJlcnJvclRleHQiLCJzZXRFcnJvclRleHQiLCJzZXRFcnJvciIsIm9sZHB3ZCIsInNldE9sZFB3ZCIsIm5ld3B3ZCIsInNldE5ld1B3ZCIsImNvbmZpcm1wd2QiLCJzZXRDb25maXJtUHdkIiwicHdkRXJyIiwiU2V0UHdkRXJyIiwibmV3cHdkRXJyIiwiU2V0TmV3UHdkRXJyIiwicHdkY29uZmlybUVyciIsIlNldENvbmZpcm1Qd2RFcnIiLCJtYXRjaEVyciIsIlNldE1hdGNoRXJyIiwidmFsaWRGaWVsZHMiLCJpc0VtcHR5IiwiaGFuZGxlQ2hhbmdlUHdkIiwib3B0aW9ucyIsIm1ldGhvZCIsImhlYWRlcnMiLCJhY2Nlc3NUb2tlbiIsImRhdGEiLCJpZCIsInVzZXJpZCIsIm9sZFBhc3N3b3JkIiwibmV3UGFzc3dvcmQiLCJiYXNlVVJMIiwidXJsIiwiYXhpb3MiLCJ0aGVuIiwicmVzcG9uc2UiLCJjYXRjaCIsInJlZ2V4RW1haWwiLCJyZWdleE9ubHlOdW1iZXIiLCJzZXRTdGF0ZSIsInRyaW0iLCJpc0VtYWlsIiwidGVzdCIsImlzTnVtYmVyIl0sIm1hcHBpbmdzIjoiOztRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsSUFBSTtRQUNKO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3hGQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxTQUFTQSxVQUFULEdBQXFCO0FBQ2pCLHNCQUNJO0FBQUssYUFBUyxFQUFDLFdBQWY7QUFBQSwyQkFDSSxxRUFBQyxvRUFBRDtBQUFhLGVBQVMsRUFBQyxRQUF2QjtBQUEyQixvQkFBVyxZQUF0QztBQUFBLDhCQUNJLHFFQUFDLDZEQUFEO0FBQU0sYUFBSyxFQUFDLFNBQVo7QUFBc0IsWUFBSSxFQUFDLEdBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREosZUFJSSxxRUFBQyxtRUFBRDtBQUFZLGFBQUssRUFBQyxhQUFsQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUpKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQVVIOztBQUVjQSx5RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2xCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU0MsUUFBVCxHQUFvQjtBQUFBOztBQUNoQixRQUFNLENBQUVDLE9BQUYsSUFBYUMsb0VBQVUsRUFBN0I7QUFDQSxRQUFNLENBQUNDLEtBQUQsRUFBUUMsUUFBUixJQUFvQkMsNENBQUssQ0FBQ0MsUUFBTixFQUExQjtBQUNBLFFBQU1DLE1BQU0sR0FBR0MsOERBQVMsRUFBeEI7QUFDQSxRQUFNO0FBQUNDO0FBQUQsTUFBUUYsTUFBTSxDQUFDRyxLQUFyQjtBQUVBLHNCQUNJLHFFQUFDLDhEQUFEO0FBQU0sV0FBTyxFQUFDLFVBQWQ7QUFBQSwyQkFDQSxxRUFBQyw4REFBRDtBQUFPLFlBQU0sTUFBYjtBQUFBLDZCQUNFLHFFQUFDLDZEQUFEO0FBQ0UsYUFBSyxFQUFFQyxRQUFRLENBQUNGLEdBQUcsR0FBR0EsR0FBSCxHQUFTLEdBQWIsQ0FEakI7QUFFRSxpQkFBUyxFQUFDLFNBRlo7QUFHRSxzQkFBYyxFQUFDLFNBSGpCO0FBSUUsc0JBQVcsdUJBSmI7QUFBQSxnQ0FNRTtBQUFHLGNBQUksRUFBQyxVQUFSO0FBQW1CLG1CQUFTLEVBQUMsdUJBQTdCO0FBQUEsaUNBQXFELHFFQUFDLDREQUFEO0FBQUssZ0JBQUksZUFBRSxxRUFBQyxpRUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQUFYO0FBQTJCLGlCQUFLLEVBQUUsZUFBY1IsT0FBZCxhQUFjQSxPQUFkLHdDQUFjQSxPQUFPLENBQUVXLElBQXZCLGtEQUFjLGNBQWVDLFFBQTdCLENBQWxDO0FBQXlFLHFCQUFTLEVBQUM7QUFBbkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFyRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQU5GLGVBT0U7QUFBRyxjQUFJLEVBQUMsZ0JBQVI7QUFBeUIsbUJBQVMsRUFBQyx1QkFBbkM7QUFBQSxpQ0FBMkQscUVBQUMsNERBQUQ7QUFBSyxpQkFBSyxFQUFDLFNBQVg7QUFBcUIsZ0JBQUksZUFBRSxxRUFBQyw4RUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBM0Q7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFQRixlQVFFO0FBQUcsY0FBSSxFQUFDLGdCQUFSO0FBQXlCLG1CQUFTLEVBQUMsdUJBQW5DO0FBQUEsaUNBQTJELHFFQUFDLDREQUFEO0FBQUssaUJBQUssRUFBQyxTQUFYO0FBQXFCLGdCQUFJLGVBQUUscUVBQUMsNEVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTNEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBUkYsZUFTRTtBQUFHLGNBQUksRUFBQyxhQUFSO0FBQXNCLG1CQUFTLEVBQUMsdUJBQWhDO0FBQUEsaUNBQXdELHFFQUFDLDREQUFEO0FBQUssaUJBQUssRUFBQyxNQUFYO0FBQWtCLGdCQUFJLGVBQUUscUVBQUMsNEVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF4QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXhEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBVEYsZUFVRTtBQUFHLGNBQUksRUFBQyxjQUFSO0FBQXVCLG1CQUFTLEVBQUMsdUJBQWpDO0FBQUEsaUNBQXlELHFFQUFDLDREQUFEO0FBQUssaUJBQUssRUFBQyxPQUFYO0FBQW1CLGdCQUFJLGVBQUUscUVBQUMsNEVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF6QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXpEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBVkYsZUFXRTtBQUFHLGNBQUksRUFBQyxrQkFBUjtBQUEyQixtQkFBUyxFQUFDLHVCQUFyQztBQUFBLGlDQUE2RCxxRUFBQyw0REFBRDtBQUFLLGlCQUFLLEVBQUMsV0FBWDtBQUF1QixnQkFBSSxlQUFFLHFFQUFDLHVFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBN0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUE3RDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVhGLGVBWUU7QUFBRyxjQUFJLEVBQUMsY0FBUjtBQUF1QixtQkFBUyxFQUFDLHVCQUFqQztBQUFBLGlDQUF5RCxxRUFBQyw0REFBRDtBQUFLLGlCQUFLLEVBQUMsT0FBWDtBQUFtQixnQkFBSSxlQUFFLHFFQUFDLHFFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF6RDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVpGLGVBYUU7QUFBRyxjQUFJLEVBQUMsZUFBUjtBQUF3QixtQkFBUyxFQUFDLHVCQUFsQztBQUFBLGlDQUEwRCxxRUFBQyw0REFBRDtBQUFLLGlCQUFLLEVBQUMsUUFBWDtBQUFvQixnQkFBSSxlQUFFLHFFQUFDLHFFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUExRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQWJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFEQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREo7QUFzQkg7O0FBRWNiLHVFQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUlBLFNBQVNjLE1BQVQsR0FBa0I7QUFDaEI7QUFFQSxRQUFNQyxZQUFZLEdBQUlDLENBQUQsSUFBTztBQUMxQkEsS0FBQyxDQUFDQyxjQUFGO0FBQ0FDLG9FQUFPO0FBQ1IsR0FIRDs7QUFLQSxzQkFDRTtBQUFLLFNBQUssRUFBQyxvRUFBWDtBQUFBLDRCQUNFLHFFQUFDLDZEQUFEO0FBQU0sVUFBSSxFQUFDLEdBQVg7QUFBZSxXQUFLLEVBQUMsU0FBckI7QUFBK0IsZUFBUyxFQUFDLE1BQXpDO0FBQUEsNkJBQ0U7QUFBSyxhQUFLLEVBQUMsaURBQVg7QUFBQSxnQ0FDRSxxRUFBQyxxRUFBRDtBQUFpQixtQkFBUyxFQUFDO0FBQTNCO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREYsZUFFRTtBQUFNLGVBQUssRUFBQyxzQ0FBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFPRTtBQUFLLFdBQUssRUFBQyxpQkFBWDtBQUFBLDZCQUNFO0FBQVEsYUFBSyxFQUFDLCtHQUFkO0FBQUEsK0JBQ0U7QUFDRSxlQUFLLEVBQUMsc0JBRFI7QUFFRSxpQkFBTyxFQUFDLFdBRlY7QUFHRSxlQUFLLEVBQUMsNEJBSFI7QUFBQSxrQ0FLRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFMRixlQU1FO0FBQU0sYUFBQyxFQUFDO0FBQVI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFORjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQVBGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBNEdEOztBQUVjSixxRUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlIQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLFNBQVNLLFNBQVQsR0FBcUI7QUFDbkIsc0JBQ0UscUVBQUMsbUVBQUQ7QUFBWSxXQUFPLEVBQUMsT0FBcEI7QUFBNEIsU0FBSyxFQUFDLGVBQWxDO0FBQWtELFNBQUssRUFBQyxRQUF4RDtBQUFBLGVBQ0csY0FESCxlQUVFLHFFQUFDLDZEQUFEO0FBQU0sVUFBSSxFQUFDLEdBQVg7QUFBZSxlQUFTLEVBQUMsY0FBekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFGRixFQUlVLEdBSlYsRUFLRyxJQUFJQyxJQUFKLEdBQVdDLFdBQVgsRUFMSCxFQU1HLEdBTkg7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREY7QUFVRDs7QUFFRCxNQUFNQyxTQUFTLEdBQUdDLDRFQUFVLENBQUVDLEtBQUQsS0FBWTtBQUN2Q0MsT0FBSyxFQUFFO0FBQ0xDLGFBQVMsRUFBRUYsS0FBSyxDQUFDRyxPQUFOLENBQWMsQ0FBZCxDQUROO0FBRUxDLFdBQU8sRUFBRSxNQUZKO0FBR0xDLGlCQUFhLEVBQUUsUUFIVjtBQUlMQyxjQUFVLEVBQUU7QUFKUCxHQURnQztBQU92Q0MsUUFBTSxFQUFFO0FBQ05DLFVBQU0sRUFBRVIsS0FBSyxDQUFDRyxPQUFOLENBQWMsQ0FBZCxDQURGO0FBRU5NLG1CQUFlLEVBQUVULEtBQUssQ0FBQ1UsT0FBTixDQUFjQyxTQUFkLENBQXdCQztBQUZuQyxHQVArQjtBQVd2Q0MsTUFBSSxFQUFFO0FBQ0pDLFNBQUssRUFBRSxNQURIO0FBQ1c7QUFDZlosYUFBUyxFQUFFRixLQUFLLENBQUNHLE9BQU4sQ0FBYyxDQUFkO0FBRlAsR0FYaUM7QUFldkNZLFFBQU0sRUFBRTtBQUNOUCxVQUFNLEVBQUVSLEtBQUssQ0FBQ0csT0FBTixDQUFjLENBQWQsRUFBaUIsQ0FBakIsRUFBb0IsQ0FBcEI7QUFERjtBQWYrQixDQUFaLENBQUQsQ0FBNUI7QUFvQmUsU0FBU2EsTUFBVCxDQUFnQjtBQUFFQztBQUFGLENBQWhCLEVBQStCO0FBQzVDLFFBQU1DLE9BQU8sR0FBR3BCLFNBQVMsRUFBekI7QUFDQSxRQUFNO0FBQUEsT0FBQ3FCLEtBQUQ7QUFBQSxPQUFRQztBQUFSLE1BQW9CdEMsc0RBQVEsQ0FBQyxFQUFELENBQWxDO0FBQ0EsUUFBTTtBQUFBLE9BQUN1QyxHQUFEO0FBQUEsT0FBTUM7QUFBTixNQUFnQnhDLHNEQUFRLENBQUMsRUFBRCxDQUE5QjtBQUNBLFFBQU07QUFBQSxPQUFDeUMsVUFBRDtBQUFBLE9BQWFDO0FBQWIsTUFBOEIxQyxzREFBUSxDQUFDLEtBQUQsQ0FBNUM7QUFDQSxRQUFNO0FBQUEsT0FBQzJDLFVBQUQ7QUFBQSxPQUFhQztBQUFiLE1BQThCNUMsc0RBQVEsQ0FBQyxFQUFELENBQTVDO0FBQ0EsUUFBTUMsTUFBTSxHQUFHQyw4REFBUyxFQUF4QjtBQUVBMkMseURBQVMsQ0FBQyxNQUFNO0FBQ2QsUUFBSTVDLE1BQU0sQ0FBQ0csS0FBUCxDQUFhMEMsS0FBakIsRUFBd0I7QUFDdEJGLG1CQUFhLENBQUMzQyxNQUFNLENBQUNHLEtBQVAsQ0FBYTBDLEtBQWQsQ0FBYixDQURzQixDQUNhO0FBQ25DO0FBQ0Q7QUFDRixHQUxRLEVBS04sQ0FBQzdDLE1BQUQsQ0FMTSxDQUFUOztBQU9BLFFBQU04QyxXQUFXLEdBQUlyQyxDQUFELElBQU87QUFDekJBLEtBQUMsQ0FBQ0MsY0FBRjs7QUFDQSxRQUFHMEIsS0FBSyxJQUFJRSxHQUFaLEVBQWdCO0FBQ2RTLHNFQUFNLENBQUMsYUFBRCxFQUFnQjtBQUNwQlgsYUFBSyxFQUFFQSxLQURhO0FBRXBCWSxnQkFBUSxFQUFFVjtBQUZVLE9BQWhCLENBQU47QUFJRDs7QUFDSCx3QkFDRTtBQUFLLGVBQVMsRUFBQyxnRUFBZjtBQUFBLDhCQUNFO0FBQUssaUJBQVMsRUFBRUgsT0FBTyxDQUFDakIsS0FBeEI7QUFBQSxnQ0FDRTtBQUFLLG1CQUFTLEVBQUMsb0NBQWY7QUFBQSxrQ0FDRSxxRUFBQyx3RUFBRDtBQUFtQixxQkFBUyxFQUFDLG9CQUE3QjtBQUFrRCxvQkFBUSxFQUFDO0FBQTNEO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREYsZUFFRTtBQUFNLHFCQUFTLEVBQUMsd0JBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFERixlQUtFO0FBQ0UsbUJBQVMsRUFBRWlCLE9BQU8sQ0FBQ0wsSUFEckI7QUFFRSxnQkFBTSxFQUFDLE1BRlQ7QUFHRSxnQkFBTSxFQUFDLGdDQUhUO0FBSUUsb0JBQVUsTUFKWjtBQUFBLGtDQU1FO0FBQU8sZ0JBQUksRUFBQyxXQUFaO0FBQXdCLGdCQUFJLEVBQUMsUUFBN0I7QUFBc0Msd0JBQVksRUFBRUk7QUFBcEQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFORixlQU9FLHFFQUFDLGtFQUFELENBQ0U7QUFERjtBQUVFLGtCQUFNLEVBQUMsUUFGVDtBQUdFLG9CQUFRLE1BSFY7QUFJRSxxQkFBUyxNQUpYO0FBS0UsY0FBRSxFQUFDLE9BTEw7QUFNRSxpQkFBSyxFQUFDLGVBTlI7QUFPRSxnQkFBSSxFQUFDLE9BUFA7QUFRRSxnQkFBSSxFQUFDLE9BUlA7QUFTRSx3QkFBWSxFQUFDLE9BVGY7QUFVRSxpQkFBSyxFQUFFRSxLQVZUO0FBV0UscUJBQVMsTUFYWDtBQVlFLG9CQUFRLE1BWlY7QUFhRSxzQkFBVSxFQUFFO0FBQ1ZhLDRCQUFjLGVBQ1oscUVBQUMsd0VBQUQ7QUFBZ0Isd0JBQVEsRUFBQyxPQUF6QjtBQUFBLHVDQUNFLHFFQUFDLGdFQUFEO0FBQVcsMkJBQVMsRUFBQztBQUFyQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZRLGFBYmQ7QUFvQkUsb0JBQVEsRUFBR3hDLENBQUQsSUFBTzRCLFFBQVEsQ0FBQzVCLENBQUMsQ0FBQ3lDLE1BQUYsQ0FBU3RELEtBQVY7QUFwQjNCO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBUEYsZUE2QkUscUVBQUMsa0VBQUQsQ0FDRTtBQURGO0FBRUUsa0JBQU0sRUFBQyxRQUZUO0FBR0Usb0JBQVEsTUFIVjtBQUlFLHFCQUFTLE1BSlg7QUFLRSxnQkFBSSxFQUFDLFVBTFA7QUFNRSxpQkFBSyxFQUFDLFVBTlI7QUFPRSxnQkFBSSxFQUFDLFVBUFA7QUFRRSxjQUFFLEVBQUMsVUFSTDtBQVNFLHdCQUFZLEVBQUMsa0JBVGY7QUFVRSxzQkFBVSxFQUFFO0FBQ1ZxRCw0QkFBYyxlQUNaLHFFQUFDLHdFQUFEO0FBQWdCLHdCQUFRLEVBQUMsT0FBekI7QUFBQSx1Q0FDRSxxRUFBQyxnRUFBRDtBQUFXLDJCQUFTLEVBQUM7QUFBckI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGUSxhQVZkO0FBaUJFLG9CQUFRLEVBQUd4QyxDQUFELElBQU84QixNQUFNLENBQUM5QixDQUFDLENBQUN5QyxNQUFGLENBQVN0RCxLQUFWLENBakJ6QjtBQWtCRSxvQkFBUTtBQWxCVjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQTdCRixlQWlERTtBQUFBLG9DQUNFO0FBQ0Usa0JBQUksRUFBQyxVQURQO0FBRUUsdUJBQVMsRUFBQyxNQUZaO0FBR0UscUJBQU8sRUFBRTRDLFVBSFg7QUFJRSxzQkFBUSxFQUFHL0IsQ0FBRCxJQUFPZ0MsYUFBYSxDQUFDaEMsQ0FBQyxDQUFDeUMsTUFBRixDQUFTdEQsS0FBVjtBQUpoQztBQUFBO0FBQUE7QUFBQTtBQUFBLG9CQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFqREYsRUEyREc4QyxVQUFVLGlCQUNULHFFQUFDLDhEQUFEO0FBQU8sb0JBQVEsRUFBQyxPQUFoQjtBQUF3QixtQkFBTyxFQUFDLFVBQWhDO0FBQTJDLHFCQUFTLEVBQUMsTUFBckQ7QUFBQSxzQkFDR0E7QUFESDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQTVESixlQWdFRSxxRUFBQywrREFBRDtBQUNFLGdCQUFJLEVBQUMsUUFEUDtBQUVFLHFCQUFTLE1BRlg7QUFHRSxtQkFBTyxFQUFDLFdBSFY7QUFJRSxpQkFBSyxFQUFDLFNBSlI7QUFLRSxxQkFBUyxFQUFFUCxPQUFPLENBQUNILE1BTHJCO0FBTUUsb0JBQVEsRUFBR3ZCLENBQUQsSUFBT3FDLFdBQVcsQ0FBQ3JDLENBQUQsQ0FOOUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBaEVGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFMRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQWtGRTtBQUFLLGlCQUFTLEVBQUMsTUFBZjtBQUFBLCtCQUNFLHFFQUFDLFNBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FsRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREY7QUF3RkQsR0FoR0M7QUFnR0EsQyxDQUVGO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUMxS0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRWUsU0FBUzBDLFVBQVQsR0FBcUI7QUFDaEMsUUFBTSxDQUFFekQsT0FBRixJQUFhQyxtRUFBVSxFQUE3QjtBQUNBLFFBQU0sQ0FBQ3lELE9BQUQsRUFBVUMsVUFBVixJQUF3QnZELDRDQUFLLENBQUNDLFFBQU4sQ0FBZSxFQUFmLENBQTlCO0FBQ0EsUUFBTSxDQUFDdUQsU0FBRCxFQUFZQyxZQUFaLElBQTRCekQsNENBQUssQ0FBQ0MsUUFBTixDQUFlLEVBQWYsQ0FBbEM7QUFDQSxRQUFNLENBQUM4QyxLQUFELEVBQVFXLFFBQVIsSUFBb0IxRCw0Q0FBSyxDQUFDQyxRQUFOLENBQWUsS0FBZixDQUExQjtBQUNBLFFBQU0sQ0FBQzBELE1BQUQsRUFBUUMsU0FBUixJQUFxQjVELDRDQUFLLENBQUNDLFFBQU4sQ0FBZSxFQUFmLENBQTNCO0FBQ0EsUUFBTSxDQUFDNEQsTUFBRCxFQUFRQyxTQUFSLElBQXFCOUQsNENBQUssQ0FBQ0MsUUFBTixDQUFlLEVBQWYsQ0FBM0I7QUFDQSxRQUFNLENBQUM4RCxVQUFELEVBQVlDLGFBQVosSUFBNkJoRSw0Q0FBSyxDQUFDQyxRQUFOLENBQWUsRUFBZixDQUFuQztBQUNBLFFBQU0sQ0FBQ2dFLE1BQUQsRUFBUUMsU0FBUixJQUFxQmxFLDRDQUFLLENBQUNDLFFBQU4sQ0FBZSxLQUFmLENBQTNCO0FBQ0EsUUFBTSxDQUFDa0UsU0FBRCxFQUFXQyxZQUFYLElBQTJCcEUsNENBQUssQ0FBQ0MsUUFBTixDQUFlLEtBQWYsQ0FBakM7QUFDQSxRQUFNLENBQUNvRSxhQUFELEVBQWVDLGdCQUFmLElBQW1DdEUsNENBQUssQ0FBQ0MsUUFBTixDQUFlLEtBQWYsQ0FBekM7QUFDQSxRQUFNLENBQUNzRSxRQUFELEVBQVVDLFdBQVYsSUFBeUJ4RSw0Q0FBSyxDQUFDQyxRQUFOLENBQWUsS0FBZixDQUEvQjs7QUFFQSxRQUFNd0UsV0FBVyxHQUFHLE1BQU07QUFDdEIsUUFBR0Msc0VBQU8sQ0FBQ2YsTUFBRCxFQUFTTyxTQUFULENBQVYsRUFBOEI7QUFDMUIsYUFBTyxJQUFQO0FBQ0g7O0FBQ0QsUUFBR1Esc0VBQU8sQ0FBQ2IsTUFBRCxFQUFTTyxZQUFULENBQVYsRUFBaUM7QUFDN0IsYUFBTyxJQUFQO0FBQ0g7O0FBQ0QsUUFBR00sc0VBQU8sQ0FBQ1gsVUFBRCxFQUFhTyxnQkFBYixDQUFWLEVBQXlDO0FBQ3JDLGFBQU8sSUFBUDtBQUNIOztBQUNELFFBQUdULE1BQU0sSUFBSUUsVUFBYixFQUF3QjtBQUNwQlMsaUJBQVcsQ0FBQyxJQUFELENBQVg7QUFDQSxhQUFPLElBQVA7QUFDSCxLQUhELE1BR0s7QUFDREEsaUJBQVcsQ0FBQyxLQUFELENBQVg7QUFDSDs7QUFDRCxXQUFPLEtBQVA7QUFDSCxHQWpCRDs7QUFrQkEsUUFBTUcsZUFBZSxHQUFJaEUsQ0FBRCxJQUFPO0FBQzNCQSxLQUFDLENBQUNDLGNBQUY7O0FBRUEsUUFBRyxDQUFDNkQsV0FBVyxFQUFmLEVBQW1CO0FBQUE7O0FBQ2YsWUFBTUcsT0FBTyxHQUFHO0FBQ1pDLGNBQU0sRUFBRSxNQURJO0FBRVpDLGVBQU8sRUFBRTtBQUFFLDBCQUFnQixrQkFBbEI7QUFBc0MsMkJBQWlCbEYsT0FBTyxDQUFDbUY7QUFBL0QsU0FGRztBQUdaQyxZQUFJLEVBQUM7QUFDREMsWUFBRSxFQUFDckYsT0FBRCxhQUFDQSxPQUFELHdDQUFDQSxPQUFPLENBQUVXLElBQVYsa0RBQUMsY0FBZTJFLE1BRGpCO0FBRURDLHFCQUFXLEVBQUN4QixNQUZYO0FBR0R5QixxQkFBVyxFQUFDdkI7QUFIWCxTQUhPO0FBUVp3QixlQUFPLEVBQUUsNEJBUkc7QUFTWkMsV0FBRyxFQUFFO0FBVE8sT0FBaEI7QUFZQUMsbURBQUssQ0FBQ1gsT0FBRCxDQUFMLENBQ0NZLElBREQsQ0FDTSxVQUFVQyxRQUFWLEVBQW9CO0FBQ3RCLFlBQUdBLFFBQVEsQ0FBQ1QsSUFBVCxJQUFpQixDQUFwQixFQUFzQjtBQUNsQnRCLGtCQUFRLENBQUMsS0FBRCxDQUFSO0FBQ0FRLG1CQUFTLENBQUMsSUFBRCxDQUFUO0FBQ0FULHNCQUFZLENBQUMsMEJBQUQsQ0FBWjtBQUNILFNBSkQsTUFJSztBQUNEQyxrQkFBUSxDQUFDLEtBQUQsQ0FBUjtBQUNBSCxvQkFBVSxDQUFDLHVCQUFELENBQVY7QUFDQUUsc0JBQVksQ0FBQyxFQUFELENBQVo7QUFDSDtBQUNKLE9BWEQsRUFZQ2lDLEtBWkQsQ0FZTyxVQUFVM0MsS0FBVixFQUFpQjtBQUNwQlcsZ0JBQVEsQ0FBQyxJQUFELENBQVI7QUFDQUgsa0JBQVUsQ0FBQyxPQUFELENBQVY7QUFDSCxPQWZEO0FBZ0JIO0FBQ0osR0FqQ0Q7O0FBa0NBLHNCQUNJO0FBQUssYUFBUyxFQUFDLEVBQWY7QUFBQSw0QkFDSSxxRUFBQyxnREFBRDtBQUFBLDhCQUNJO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREosZUFFSTtBQUFNLFdBQUcsRUFBQyxNQUFWO0FBQWlCLFlBQUksRUFBQztBQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREosZUFLSTtBQUFNLGVBQVMsRUFBQyxVQUFoQjtBQUFBLDhCQUNJLHFFQUFDLGlFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FESixlQUVJLHFFQUFDLHNFQUFEO0FBQVksaUJBQVMsRUFBQztBQUF0QjtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBRkosZUFHSSxxRUFBQyxtRUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBSEosZUFJSTtBQUFNLGlCQUFTLEVBQUMsOEJBQWhCO0FBQUEsbUJBQ0tELE9BQU8saUJBQUkscUVBQUMsOERBQUQ7QUFBTyxlQUFLLEVBQUVQLEtBQUssR0FBRyxPQUFILEdBQWEsU0FBaEM7QUFBMkMsa0JBQVEsRUFBRUEsS0FBSyxHQUFHLE9BQUgsR0FBYSxTQUF2RTtBQUFrRixtQkFBUyxFQUFDLE1BQTVGO0FBQUEsb0JBQW9HTztBQUFwRztBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURoQixlQUVJLHFFQUFDLDZEQUFEO0FBQU0sY0FBSSxNQUFWO0FBQVcsWUFBRSxFQUFFLEVBQWY7QUFBQSxpQ0FDQSxxRUFBQyxrRUFBRDtBQUNJLGNBQUUsRUFBQyxnQkFEUDtBQUVJLGlCQUFLLEVBQUMsY0FGVjtBQUdJLGdCQUFJLEVBQUMsVUFIVDtBQUlJLG9CQUFRLEVBQUczQyxDQUFELElBQU9pRCxTQUFTLENBQUNqRCxDQUFDLENBQUN5QyxNQUFGLENBQVN0RCxLQUFWLENBSjlCO0FBS0ksaUJBQUssRUFBRW1FLE1BTFg7QUFNSSxzQkFBVSxFQUFFQSxNQUFNLEdBQUdULFNBQUgsR0FBZSxFQU5yQztBQU9JLHFCQUFTLE1BUGI7QUFRSSxvQkFBUSxNQVJaO0FBU0kscUJBQVM7QUFUYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGSixlQWNJO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBZEosZUFlSSxxRUFBQyw2REFBRDtBQUFPLGNBQUksTUFBWDtBQUFZLFlBQUUsRUFBRSxFQUFoQjtBQUFBLGlDQUNBLHFFQUFDLGtFQUFEO0FBQ0ksY0FBRSxFQUFDLGdCQURQO0FBRUksaUJBQUssRUFBQyxjQUZWO0FBR0ksZ0JBQUksRUFBQyxVQUhUO0FBSUksaUJBQUssRUFBRVcsU0FKWDtBQUtJLHNCQUFVLEVBQUVBLFNBQVMsR0FBRyxzQkFBSCxHQUE0QixFQUxyRDtBQU1JLG9CQUFRLEVBQUd4RCxDQUFELElBQU9tRCxTQUFTLENBQUNuRCxDQUFDLENBQUN5QyxNQUFGLENBQVN0RCxLQUFWLENBTjlCO0FBT0ksb0JBQVEsTUFQWjtBQVFJLHFCQUFTO0FBUmI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBZkosZUEyQkk7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkEzQkosZUE0QkkscUVBQUMsNkRBQUQ7QUFBTyxjQUFJLE1BQVg7QUFBWSxZQUFFLEVBQUUsRUFBaEI7QUFBQSxpQ0FDQSxxRUFBQyxrRUFBRDtBQUNJLGNBQUUsRUFBQyxnQkFEUDtBQUVJLGlCQUFLLEVBQUMsa0JBRlY7QUFHSSxnQkFBSSxFQUFDLFVBSFQ7QUFJSSxpQkFBSyxFQUFFdUUsYUFBYSxJQUFJRSxRQUo1QjtBQUtJLHNCQUFVLEVBQUVGLGFBQWEsR0FBRywwQkFBSCxHQUFnQ0UsUUFBUSxHQUFHLDRCQUFILEdBQWtDLEVBTHZHO0FBTUksb0JBQVEsRUFBRzVELENBQUQsSUFBT3FELGFBQWEsQ0FBQ3JELENBQUMsQ0FBQ3lDLE1BQUYsQ0FBU3RELEtBQVYsQ0FObEM7QUFPSSxvQkFBUSxNQVBaO0FBUUkscUJBQVM7QUFSYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkE1QkosZUF1Q0k7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkF2Q0osZUF3Q0kscUVBQUMsNkRBQUQ7QUFBTSxjQUFJLE1BQVY7QUFBVyxZQUFFLEVBQUUsRUFBZjtBQUFBLGlDQUNBLHFFQUFDLCtEQUFEO0FBQVEsbUJBQU8sRUFBSWEsQ0FBRCxJQUFPZ0UsZUFBZSxDQUFDaEUsQ0FBRCxDQUF4QztBQUNJLGdCQUFJLEVBQUMsUUFEVDtBQUVJLHFCQUFTLE1BRmI7QUFHSSxtQkFBTyxFQUFDLFdBSFo7QUFJSSxpQkFBSyxFQUFDLFNBSlYsQ0FLSTtBQUxKO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkF4Q0o7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBSko7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBTEo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREo7QUFxRUgsQzs7Ozs7Ozs7Ozs7O0FDbkpEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUEsTUFBTWdGLFVBQVUsR0FBRyxtQ0FBbkI7QUFDQSxNQUFNQyxlQUFlLEdBQUcsV0FBeEIsQyxDQUVBOztBQUNBLE1BQU1sQixPQUFPLEdBQUcsQ0FBQzVFLEtBQUQsRUFBUStGLFFBQVIsS0FBcUI7QUFDakMsTUFBRy9GLEtBQUssQ0FBQ2dHLElBQU4sTUFBZ0IsRUFBbkIsRUFBc0I7QUFDbEJELFlBQVEsQ0FBQyxJQUFELENBQVI7QUFDQSxXQUFPLElBQVA7QUFDSDs7QUFDREEsVUFBUSxDQUFDLEtBQUQsQ0FBUjtBQUNBLFNBQU8sS0FBUDtBQUNILENBUEQ7O0FBUUEsTUFBTUUsT0FBTyxHQUFJakcsS0FBRCxJQUFXNkYsVUFBVSxDQUFDSyxJQUFYLENBQWdCbEcsS0FBaEIsQ0FBM0I7O0FBQ0EsTUFBTW1HLFFBQVEsR0FBSW5HLEtBQUQsSUFBVzhGLGVBQWUsQ0FBQ0ksSUFBaEIsQ0FBcUJsRyxLQUFyQixDQUE1Qjs7Ozs7Ozs7Ozs7OztBQ2ZBLDhDOzs7Ozs7Ozs7OztBQ0FBLGtEOzs7Ozs7Ozs7OztBQ0FBLDBEOzs7Ozs7Ozs7OztBQ0FBLHFEOzs7Ozs7Ozs7OztBQ0FBLG1EOzs7Ozs7Ozs7OztBQ0FBLHVEOzs7Ozs7Ozs7OztBQ0FBLCtEOzs7Ozs7Ozs7OztBQ0FBLG1EOzs7Ozs7Ozs7OztBQ0FBLDZEOzs7Ozs7Ozs7OztBQ0FBLG1EOzs7Ozs7Ozs7OztBQ0FBLG9EOzs7Ozs7Ozs7OztBQ0FBLGtEOzs7Ozs7Ozs7OztBQ0FBLG1EOzs7Ozs7Ozs7OztBQ0FBLHdEOzs7Ozs7Ozs7OztBQ0FBLHlEOzs7Ozs7Ozs7OztBQ0FBLHFEOzs7Ozs7Ozs7OztBQ0FBLCtDOzs7Ozs7Ozs7OztBQ0FBLDZEOzs7Ozs7Ozs7OztBQ0FBLDBEOzs7Ozs7Ozs7OztBQ0FBLHFEOzs7Ozs7Ozs7OztBQ0FBLHFEOzs7Ozs7Ozs7OztBQ0FBLGlFOzs7Ozs7Ozs7OztBQ0FBLGtFOzs7Ozs7Ozs7OztBQ0FBLDREOzs7Ozs7Ozs7OztBQ0FBLG9FOzs7Ozs7Ozs7OztBQ0FBLHNEOzs7Ozs7Ozs7OztBQ0FBLDJEOzs7Ozs7Ozs7OztBQ0FBLDZEOzs7Ozs7Ozs7OztBQ0FBLGtFOzs7Ozs7Ozs7OztBQ0FBLDJEOzs7Ozs7Ozs7OztBQ0FBLG1EOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLDZDOzs7Ozs7Ozs7OztBQ0FBLHNDOzs7Ozs7Ozs7OztBQ0FBLHdDOzs7Ozs7Ozs7OztBQ0FBLGtDOzs7Ozs7Ozs7OztBQ0FBLGtEIiwiZmlsZSI6InBhZ2VzL2F1dGgvc2lnbmluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSByZXF1aXJlKCcuLi8uLi9zc3ItbW9kdWxlLWNhY2hlLmpzJyk7XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdHZhciB0aHJldyA9IHRydWU7XG4gXHRcdHRyeSB7XG4gXHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG4gXHRcdFx0dGhyZXcgPSBmYWxzZTtcbiBcdFx0fSBmaW5hbGx5IHtcbiBcdFx0XHRpZih0aHJldykgZGVsZXRlIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9wYWdlcy9hdXRoL3NpZ25pbi5qc1wiKTtcbiIsImltcG9ydCBCcmVhZGNydW1icyBmcm9tICdAbWF0ZXJpYWwtdWkvY29yZS9CcmVhZGNydW1icyc7XHJcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlL1R5cG9ncmFwaHknO1xyXG5pbXBvcnQgTGluayBmcm9tICdAbWF0ZXJpYWwtdWkvY29yZS9MaW5rJztcclxuaW1wb3J0IE5hdmlnYXRlTmV4dEljb24gZnJvbSAnQG1hdGVyaWFsLXVpL2ljb25zL05hdmlnYXRlTmV4dCdcclxuXHJcbmZ1bmN0aW9uIEJyZWFkQ3J1bWIoKXtcclxuICAgIHJldHVybiAoXHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJweS0yIG1sLTRcIj5cclxuICAgICAgICAgICAgPEJyZWFkY3J1bWJzIHNlcGFyYXRvcj1cIuKAulwiIGFyaWEtbGFiZWw9XCJicmVhZGNydW1iXCI+XHJcbiAgICAgICAgICAgICAgICA8TGluayBjb2xvcj1cImluaGVyaXRcIiBocmVmPVwiL1wiPlxyXG4gICAgICAgICAgICAgICAgSG9tZVxyXG4gICAgICAgICAgICAgICAgPC9MaW5rPlxyXG4gICAgICAgICAgICAgICAgPFR5cG9ncmFwaHkgY29sb3I9XCJ0ZXh0UHJpbWFyeVwiPkFjY291bnQ8L1R5cG9ncmFwaHk+XHJcbiAgICAgICAgICAgIDwvQnJlYWRjcnVtYnM+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEJyZWFkQ3J1bWIiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUGFwZXIgZnJvbSAnQG1hdGVyaWFsLXVpL2NvcmUvUGFwZXInO1xyXG5pbXBvcnQgVGFicyBmcm9tICdAbWF0ZXJpYWwtdWkvY29yZS9UYWJzJztcclxuaW1wb3J0IFRhYiBmcm9tICdAbWF0ZXJpYWwtdWkvY29yZS9UYWInO1xyXG5pbXBvcnQgUGVvcGxlT3V0bGluZVJvdW5kZWRJY29uIGZyb20gJ0BtYXRlcmlhbC11aS9pY29ucy9QZW9wbGVPdXRsaW5lUm91bmRlZCc7XHJcbmltcG9ydCBSb29tU2VydmljZVJvdW5kZWRJY29uIGZyb20gJ0BtYXRlcmlhbC11aS9pY29ucy9Sb29tU2VydmljZVJvdW5kZWQnO1xyXG5pbXBvcnQgTWVldGluZ1Jvb21Sb3VuZGVkSWNvbiBmcm9tICdAbWF0ZXJpYWwtdWkvaWNvbnMvTWVldGluZ1Jvb21Sb3VuZGVkJztcclxuaW1wb3J0IFN0YXJSb3VuZGVkSWNvbiBmcm9tICdAbWF0ZXJpYWwtdWkvaWNvbnMvU3RhclJvdW5kZWQnO1xyXG5pbXBvcnQgUmVkZWVtUm91bmRlZEljb24gZnJvbSAnQG1hdGVyaWFsLXVpL2ljb25zL1JlZGVlbVJvdW5kZWQnO1xyXG5pbXBvcnQgTG9jYWxPZmZlclJvdW5kZWRJY29uIGZyb20gJ0BtYXRlcmlhbC11aS9pY29ucy9Mb2NhbE9mZmVyUm91bmRlZCc7XHJcbmltcG9ydCBBc3Nlc3NtZW50SWNvbiBmcm9tICdAbWF0ZXJpYWwtdWkvaWNvbnMvQXNzZXNzbWVudCc7XHJcbmltcG9ydCBDYXJkIGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlL0NhcmQnO1xyXG5pbXBvcnQgUGVyc29uSWNvbiBmcm9tICdAbWF0ZXJpYWwtdWkvaWNvbnMvUGVyc29uJztcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInXHJcbmltcG9ydCB7IHVzZVNlc3Npb24gfSBmcm9tICduZXh0LWF1dGgvY2xpZW50J1xyXG5cclxuZnVuY3Rpb24gTWFpbk1lbnUoKSB7XHJcbiAgICBjb25zdCBbIHNlc3Npb25dID0gdXNlU2Vzc2lvbigpXHJcbiAgICBjb25zdCBbdmFsdWUsIHNldFZhbHVlXSA9IFJlYWN0LnVzZVN0YXRlKCk7XHJcbiAgICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKVxyXG4gICAgY29uc3Qge3RhYn0gPSByb3V0ZXIucXVlcnlcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxDYXJkIHZhcmlhbnQ9XCJvdXRsaW5lZFwiPlxyXG4gICAgICAgIDxQYXBlciBzcXVhcmU+XHJcbiAgICAgICAgICA8VGFic1xyXG4gICAgICAgICAgICB2YWx1ZT17cGFyc2VJbnQodGFiID8gdGFiIDogJzAnKX1cclxuICAgICAgICAgICAgdGV4dENvbG9yPVwicHJpbWFyeVwiXHJcbiAgICAgICAgICAgIGluZGljYXRvckNvbG9yPVwicHJpbWFyeVwiXHJcbiAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJkaXNhYmxlZCB0YWJzIGV4YW1wbGVcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL3Byb2ZpbGVcIiBjbGFzc05hbWU9XCJob3Zlcjp0ZXh0LWluZGlnby02MDBcIj48VGFiIGljb249ezxQZXJzb25JY29uIC8+fSBsYWJlbD17XCJ3ZWxjb21lLCBcIiArIHNlc3Npb24/LnVzZXI/LmZ1bGxOYW1lfSBjbGFzc05hbWU9XCJ0ZXh0LWluZGlnby02MDBcIi8+PC9hPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL2FjY291bnQ/dGFiPTFcIiBjbGFzc05hbWU9XCJob3Zlcjp0ZXh0LWluZGlnby02MDBcIj48VGFiIGxhYmVsPVwiQWNjb3VudFwiIGljb249ezxQZW9wbGVPdXRsaW5lUm91bmRlZEljb24gLz59Lz48L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIvYm9va2luZz90YWI9MlwiIGNsYXNzTmFtZT1cImhvdmVyOnRleHQtaW5kaWdvLTYwMFwiPjxUYWIgbGFiZWw9XCJCb29raW5nXCIgaWNvbj17PFJvb21TZXJ2aWNlUm91bmRlZEljb24gLz59Lz48L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIvcm9vbT90YWI9M1wiIGNsYXNzTmFtZT1cImhvdmVyOnRleHQtaW5kaWdvLTYwMFwiPjxUYWIgbGFiZWw9XCJSb29tXCIgaWNvbj17PE1lZXRpbmdSb29tUm91bmRlZEljb24gLz59IC8+PC9hPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL2NvbWJvP3RhYj00XCIgY2xhc3NOYW1lPVwiaG92ZXI6dGV4dC1pbmRpZ28tNjAwXCI+PFRhYiBsYWJlbD1cIkNvbWJvXCIgaWNvbj17PExvY2FsT2ZmZXJSb3VuZGVkSWNvbiAvPn0vPjwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIi9wcm9tb3Rpb24/dGFiPTVcIiBjbGFzc05hbWU9XCJob3Zlcjp0ZXh0LWluZGlnby02MDBcIj48VGFiIGxhYmVsPVwiUHJvbW90aW9uXCIgaWNvbj17PFJlZGVlbVJvdW5kZWRJY29uIC8+fS8+PC9hPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL2V2ZW50P3RhYj02XCIgY2xhc3NOYW1lPVwiaG92ZXI6dGV4dC1pbmRpZ28tNjAwXCI+PFRhYiBsYWJlbD1cIkV2ZW50XCIgaWNvbj17PFN0YXJSb3VuZGVkSWNvbiAvPn0gLz48L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIvcmVwb3J0P3RhYj03XCIgY2xhc3NOYW1lPVwiaG92ZXI6dGV4dC1pbmRpZ28tNjAwXCI+PFRhYiBsYWJlbD1cIlJlcG9ydFwiIGljb249ezxBc3Nlc3NtZW50SWNvbiAvPn0gLz48L2E+XHJcbiAgICAgICAgICA8L1RhYnM+XHJcbiAgICAgICAgPC9QYXBlcj5cclxuICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICk7XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNYWluTWVudSIsImltcG9ydCB7IHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBQZXRzUm91bmRlZEljb24gZnJvbSAnQG1hdGVyaWFsLXVpL2ljb25zL1BldHNSb3VuZGVkJ1xyXG5pbXBvcnQgeyBzaWduT3V0IH0gZnJvbSAnbmV4dC1hdXRoL2NsaWVudCdcclxuaW1wb3J0IExpbmsgZnJvbSAnQG1hdGVyaWFsLXVpL2NvcmUvTGluayc7XHJcbmltcG9ydCB7IEJ1dHRvbiB9IGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlJztcclxuXHJcblxyXG5cclxuZnVuY3Rpb24gTmF2YmFyKCkge1xyXG4gIC8vIGxldCBbaXNTaG93TWVudSwgc2V0SXNTaG93TWVudV0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcblxyXG4gIGNvbnN0IGxvZ291dEhhbmRsZSA9IChlKSA9PiB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KClcclxuICAgIHNpZ25PdXQoKVxyXG4gIH1cclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxuYXYgY2xhc3M9XCJmbGV4IGl0ZW1zLWNlbnRlciBqdXN0aWZ5LWJldHdlZW4gZmxleC13cmFwIGJnLWdyZWVuLTUwMCBweC02IHB5LTFcIj5cclxuICAgICAgPExpbmsgaHJlZj1cIi9cIiBjb2xvcj1cImluaGVyaXRcIiB1bmRlcmxpbmU9XCJub25lXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImZsZXggaXRlbXMtY2VudGVyIGZsZXgtc2hyaW5rLTAgdGV4dC13aGl0ZSBtci02XCI+XHJcbiAgICAgICAgICA8UGV0c1JvdW5kZWRJY29uIGNsYXNzTmFtZT1cIm1yLTJcIi8+XHJcbiAgICAgICAgICA8c3BhbiBjbGFzcz1cImZvbnQtc2VtaWJvbGQgdGV4dC14bCB0cmFja2luZy10aWdodFwiPkNyYXp5IEhvdXNlPC9zcGFuPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L0xpbms+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJibG9jayBsZzpoaWRkZW5cIj5cclxuICAgICAgICA8YnV0dG9uIGNsYXNzPVwiZmxleCBpdGVtcy1jZW50ZXIgcHgtMyBweS0yIGJvcmRlciByb3VuZGVkIHRleHQtZ3JlZW4tMjAwIGJvcmRlci10ZWFsLTQwMCBob3Zlcjp0ZXh0LXdoaXRlIGhvdmVyOmJvcmRlci13aGl0ZVwiPlxyXG4gICAgICAgICAgPHN2Z1xyXG4gICAgICAgICAgICBjbGFzcz1cImZpbGwtY3VycmVudCBoLTMgdy0zXCJcclxuICAgICAgICAgICAgdmlld0JveD1cIjAgMCAyMCAyMFwiXHJcbiAgICAgICAgICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8dGl0bGU+TWVudTwvdGl0bGU+XHJcbiAgICAgICAgICAgIDxwYXRoIGQ9XCJNMCAzaDIwdjJIMFYzem0wIDZoMjB2MkgwVjl6bTAgNmgyMHYySDB2LTJ6XCIgLz5cclxuICAgICAgICAgIDwvc3ZnPlxyXG4gICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgey8qIDxkaXYgY2xhc3M9XCJ3LWZ1bGwgYmxvY2sgZmxleC1ncm93IGxnOmZsZXggbGc6aXRlbXMtY2VudGVyIGxnOnctYXV0b1wiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJmbGV4LWdyb3dcIj48L2Rpdj4gKi99XHJcbiAgICAgICAgey8qIDxkaXYgY2xhc3M9XCJhYnNvbHV0ZSBpbnNldC15LTAgcmlnaHQtMCBmbGV4IGl0ZW1zLWNlbnRlciBwci0yIHNtOnN0YXRpYyBzbTppbnNldC1hdXRvIHNtOm1sLTYgc206cHItMFwiPiAqL31cclxuICAgICAgICAgIHsvKiA8YnV0dG9uXHJcbiAgICAgICAgICAgIGNsYXNzPVwicC0xIGJvcmRlci0yIGJvcmRlci10cmFuc3BhcmVudCB0ZXh0LXdoaXRlIHJvdW5kZWQtZnVsbCBob3Zlcjp0ZXh0LXdoaXRlIGZvY3VzOm91dGxpbmUtbm9uZSBmb2N1czp0ZXh0LXdoaXRlIGZvY3VzOmJnLWdyYXktNzAwIHRyYW5zaXRpb24gZHVyYXRpb24tMTUwIGVhc2UtaW4tb3V0XCJcclxuICAgICAgICAgICAgYXJpYS1sYWJlbD1cIk5vdGlmaWNhdGlvbnNcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8c3ZnXHJcbiAgICAgICAgICAgICAgY2xhc3M9XCJoLTYgdy02XCJcclxuICAgICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcclxuICAgICAgICAgICAgICBmaWxsPVwibm9uZVwiXHJcbiAgICAgICAgICAgICAgdmlld0JveD1cIjAgMCAyNCAyNFwiXHJcbiAgICAgICAgICAgICAgc3Ryb2tlPVwiY3VycmVudENvbG9yXCJcclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDxwYXRoXHJcbiAgICAgICAgICAgICAgICBzdHJva2UtbGluZWNhcD1cInJvdW5kXCJcclxuICAgICAgICAgICAgICAgIHN0cm9rZS1saW5lam9pbj1cInJvdW5kXCJcclxuICAgICAgICAgICAgICAgIHN0cm9rZS13aWR0aD1cIjJcIlxyXG4gICAgICAgICAgICAgICAgZD1cIk0xNSAxN2g1bC0xLjQwNS0xLjQwNUEyLjAzMiAyLjAzMiAwIDAxMTggMTQuMTU4VjExYTYuMDAyIDYuMDAyIDAgMDAtNC01LjY1OVY1YTIgMiAwIDEwLTQgMHYuMzQxQzcuNjcgNi4xNjUgNiA4LjM4OCA2IDExdjMuMTU5YzAgLjUzOC0uMjE0IDEuMDU1LS41OTUgMS40MzZMNCAxN2g1bTYgMHYxYTMgMyAwIDExLTYgMHYtMW02IDBIOVwiXHJcbiAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9zdmc+XHJcbiAgICAgICAgICA8L2J1dHRvbj4gKi99XHJcblxyXG4gICAgICAgICAgey8qIDxkaXYgY2xhc3M9XCJtbC0zIHJlbGF0aXZlXCI+ICovfVxyXG4gICAgICAgICAgICB7LyogPGRpdj4gKi99XHJcbiAgICAgICAgICAgIHsvKiA8QnV0dG9uIGNvbG9yPVwid2hpdGVcIiAgb25DbGljaz17KGUpID0+IGxvZ291dEhhbmRsZShlKX0+U2lnbiBvdXQ8L0J1dHRvbj4gKi99XHJcbiAgICAgICAgICAgIHsvKiA8YVxyXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJibG9jayBweC00IHB5LTIgdGV4dC1tZCBsZWFkaW5nLTUgdGV4dC1ncmF5LTcwMCAgaG92ZXI6dGV4dC13aGl0ZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZT1cIm1lbnVpdGVtXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoZSkgPT4gbG9nb3V0SGFuZGxlKGUpfVxyXG4gICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgIFNpZ24gb3V0ICBcclxuICAgICAgICAgICAgPC9hPiAqL31cclxuICAgICAgICAgICAgICB7LyogPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJmbGV4IHRleHQtc20gYm9yZGVyLTIgYm9yZGVyLXRyYW5zcGFyZW50IHJvdW5kZWQtZnVsbCBob3Zlcjpib3JkZXItd2hpdGUgZm9jdXM6b3V0bGluZS1ub25lIGZvY3VzOmJvcmRlci13aGl0ZSB0cmFuc2l0aW9uIGR1cmF0aW9uLTE1MCBlYXNlLWluLW91dFwiXHJcbiAgICAgICAgICAgICAgICBpZD1cInVzZXItbWVudVwiXHJcbiAgICAgICAgICAgICAgICBvbkZvY3VzPXsoKSA9PiBzZXRJc1Nob3dNZW51KHRydWUpfVxyXG4gICAgICAgICAgICAgICAgLy8gb25CbHVyPXsoKSA9PiBzZXRJc1Nob3dNZW51KGZhbHNlKX1cclxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJVc2VyIG1lbnVcIlxyXG4gICAgICAgICAgICAgICAgYXJpYS1oYXNwb3B1cD1cInRydWVcIlxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxpbWdcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJoLTggdy04IHJvdW5kZWQtZnVsbFwiXHJcbiAgICAgICAgICAgICAgICAgIHNyYz1cImh0dHBzOi8vaW1hZ2VzLnVuc3BsYXNoLmNvbS9waG90by0xNDcyMDk5NjQ1Nzg1LTU2NThhYmY0ZmY0ZT9peGxpYj1yYi0xLjIuMSZpeGlkPWV5SmhjSEJmYVdRaU9qRXlNRGQ5JmF1dG89Zm9ybWF0JmZpdD1mYWNlYXJlYSZmYWNlcGFkPTImdz0yNTYmaD0yNTYmcT04MFwiXHJcbiAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgIDwvYnV0dG9uPiAqL31cclxuICAgICAgICAgICAgey8qIDwvZGl2PiAqL31cclxuXHJcbiAgICAgICAgICAgIHsvKiB7aXNTaG93TWVudSAmJiAoXHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm9yaWdpbi10b3AtcmlnaHQgYWJzb2x1dGUgcmlnaHQtMCAgdy00OCByb3VuZGVkLW1kIHNoYWRvdy1sZyB6LTEwXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwicHktMSByb3VuZGVkLW1kIGJnLXdoaXRlIHNoYWRvdy14c1wiXHJcbiAgICAgICAgICAgICAgICAgIHJvbGU9XCJtZW51XCJcclxuICAgICAgICAgICAgICAgICAgYXJpYS1vcmllbnRhdGlvbj1cInZlcnRpY2FsXCJcclxuICAgICAgICAgICAgICAgICAgYXJpYS1sYWJlbGxlZGJ5PVwidXNlci1tZW51XCJcclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgPGFcclxuICAgICAgICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJibG9jayBweC00IHB5LTIgdGV4dC1zbSBsZWFkaW5nLTUgdGV4dC1ncmF5LTcwMCBob3Zlcjp0ZXh0LXdoaXRlIGhvdmVyOmJnLWdyZWVuLTMwMCBmb2N1czpvdXRsaW5lLW5vbmUgZm9jdXM6YmctZ3JheS0xMDAgdHJhbnNpdGlvbiBkdXJhdGlvbi0xNTAgZWFzZS1pbi1vdXRcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU9XCJtZW51aXRlbVwiXHJcbiAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICBZb3VyIFByb2ZpbGVcclxuICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImJsb2NrIHB4LTQgcHktMiB0ZXh0LXNtIGxlYWRpbmctNSB0ZXh0LWdyYXktNzAwICBob3Zlcjp0ZXh0LXdoaXRlIGhvdmVyOmJnLWdyZWVuLTMwMCBmb2N1czpvdXRsaW5lLW5vbmUgZm9jdXM6YmctZ3JheS0xMDAgdHJhbnNpdGlvbiBkdXJhdGlvbi0xNTAgZWFzZS1pbi1vdXRcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU9XCJtZW51aXRlbVwiXHJcbiAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICBTZXR0aW5nc1xyXG4gICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJsb2NrIHB4LTQgcHktMiB0ZXh0LXNtIGxlYWRpbmctNSB0ZXh0LWdyYXktNzAwICBob3Zlcjp0ZXh0LXdoaXRlIGhvdmVyOmJnLWdyZWVuLTMwMCBmb2N1czpvdXRsaW5lLW5vbmUgZm9jdXM6YmctZ3JheS0xMDAgdHJhbnNpdGlvbiBkdXJhdGlvbi0xNTAgZWFzZS1pbi1vdXRcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU9XCJtZW51aXRlbVwiXHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KGUpID0+IGxvZ291dEhhbmRsZShlKX1cclxuICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIFNpZ24gb3V0XHJcbiAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApfSAqL31cclxuICAgICAgICAgIHsvKiA8L2Rpdj4gKi99XHJcbiAgICAgICAgey8qIDwvZGl2PlxyXG4gICAgICA8L2Rpdj4gKi99XHJcbiAgICA8L25hdj5cclxuICApO1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBOYXZiYXI7IiwiaW1wb3J0IFJlYWN0IGZyb20gXCJyZWFjdFwiO1xyXG5pbXBvcnQgQnV0dG9uIGZyb20gXCJAbWF0ZXJpYWwtdWkvY29yZS9CdXR0b25cIjtcclxuaW1wb3J0IFRleHRGaWVsZCBmcm9tIFwiQG1hdGVyaWFsLXVpL2NvcmUvVGV4dEZpZWxkXCI7XHJcbmltcG9ydCBGb3JtQ29udHJvbExhYmVsIGZyb20gXCJAbWF0ZXJpYWwtdWkvY29yZS9Gb3JtQ29udHJvbExhYmVsXCI7XHJcbmltcG9ydCBDaGVja2JveCBmcm9tIFwiQG1hdGVyaWFsLXVpL2NvcmUvQ2hlY2tib3hcIjtcclxuaW1wb3J0IExpbmsgZnJvbSBcIkBtYXRlcmlhbC11aS9jb3JlL0xpbmtcIjtcclxuaW1wb3J0IEdyaWQgZnJvbSBcIkBtYXRlcmlhbC11aS9jb3JlL0dyaWRcIjtcclxuaW1wb3J0IEJveCBmcm9tIFwiQG1hdGVyaWFsLXVpL2NvcmUvQm94XCI7XHJcbmltcG9ydCBUeXBvZ3JhcGh5IGZyb20gXCJAbWF0ZXJpYWwtdWkvY29yZS9UeXBvZ3JhcGh5XCI7XHJcbmltcG9ydCB7IG1ha2VTdHlsZXMgfSBmcm9tIFwiQG1hdGVyaWFsLXVpL2NvcmUvc3R5bGVzXCI7XHJcbmltcG9ydCB7IGNzcmZUb2tlbiwgc2lnbkluIH0gZnJvbSBcIm5leHQtYXV0aC9jbGllbnRcIjtcclxuaW1wb3J0IHsgdXNlU3RhdGUgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSBcIm5leHQvcm91dGVyXCI7XHJcbmltcG9ydCBBbGVydCBmcm9tIFwiQG1hdGVyaWFsLXVpL2xhYi9BbGVydFwiO1xyXG5pbXBvcnQgeyB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IElucHV0QWRvcm5tZW50IGZyb20gXCJAbWF0ZXJpYWwtdWkvY29yZS9JbnB1dEFkb3JubWVudFwiO1xyXG5pbXBvcnQgRW1haWxJY29uIGZyb20gXCJAbWF0ZXJpYWwtdWkvaWNvbnMvRW1haWxcIjtcclxuaW1wb3J0IEh0dHBzSWNvbiBmcm9tIFwiQG1hdGVyaWFsLXVpL2ljb25zL0h0dHBzXCI7XHJcbmltcG9ydCBBY2NvdW50Q2lyY2xlSWNvbiBmcm9tIFwiQG1hdGVyaWFsLXVpL2ljb25zL0FjY291bnRDaXJjbGVcIjtcclxuaW1wb3J0IFBhc3MgZnJvbSBcIi4uL2NoYW5nZXBhc3NcIjtcclxuZnVuY3Rpb24gQ29weXJpZ2h0KCkge1xyXG4gIHJldHVybiAoXHJcbiAgICA8VHlwb2dyYXBoeSB2YXJpYW50PVwiYm9keTJcIiBjb2xvcj1cInRleHRTZWNvbmRhcnlcIiBhbGlnbj1cImNlbnRlclwiPlxyXG4gICAgICB7XCJDb3B5cmlnaHQgwqkgXCJ9XHJcbiAgICAgIDxMaW5rIGhyZWY9XCIjXCIgY2xhc3NOYW1lPVwidGV4dC1yZWQtNTAwXCI+XHJcbiAgICAgICAgQ3Jhenlob3VzZVxyXG4gICAgICA8L0xpbms+e1wiIFwifVxyXG4gICAgICB7bmV3IERhdGUoKS5nZXRGdWxsWWVhcigpfVxyXG4gICAgICB7XCIuXCJ9XHJcbiAgICA8L1R5cG9ncmFwaHk+XHJcbiAgKTtcclxufVxyXG5cclxuY29uc3QgdXNlU3R5bGVzID0gbWFrZVN0eWxlcygodGhlbWUpID0+ICh7XHJcbiAgcGFwZXI6IHtcclxuICAgIG1hcmdpblRvcDogdGhlbWUuc3BhY2luZygyKSxcclxuICAgIGRpc3BsYXk6IFwiZmxleFwiLFxyXG4gICAgZmxleERpcmVjdGlvbjogXCJjb2x1bW5cIixcclxuICAgIGFsaWduSXRlbXM6IFwiY2VudGVyXCIsXHJcbiAgfSxcclxuICBhdmF0YXI6IHtcclxuICAgIG1hcmdpbjogdGhlbWUuc3BhY2luZygxKSxcclxuICAgIGJhY2tncm91bmRDb2xvcjogdGhlbWUucGFsZXR0ZS5zZWNvbmRhcnkubWFpbixcclxuICB9LFxyXG4gIGZvcm06IHtcclxuICAgIHdpZHRoOiBcIjEwMCVcIiwgLy8gRml4IElFIDExIGlzc3VlLlxyXG4gICAgbWFyZ2luVG9wOiB0aGVtZS5zcGFjaW5nKDEpLFxyXG4gIH0sXHJcbiAgc3VibWl0OiB7XHJcbiAgICBtYXJnaW46IHRoZW1lLnNwYWNpbmcoMywgMCwgMiksXHJcbiAgfSxcclxufSkpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gU2lnbkluKHsgY3NyZlRva2VuIH0pIHtcclxuICBjb25zdCBjbGFzc2VzID0gdXNlU3R5bGVzKCk7XHJcbiAgY29uc3QgW2VtYWlsLCBzZXRFbWFpbF0gPSB1c2VTdGF0ZShcIlwiKTtcclxuICBjb25zdCBbcHdkLCBzZXRQd2RdID0gdXNlU3RhdGUoXCJcIik7XHJcbiAgY29uc3QgW2lzUmVtZW1iZXIsIHNldElzUmVtZW1iZXJdID0gdXNlU3RhdGUoZmFsc2UpO1xyXG4gIGNvbnN0IFtsb2dpbkVycm9yLCBzZXRMb2dpbkVycm9yXSA9IHVzZVN0YXRlKFwiXCIpO1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKHJvdXRlci5xdWVyeS5lcnJvcikge1xyXG4gICAgICBzZXRMb2dpbkVycm9yKHJvdXRlci5xdWVyeS5lcnJvcik7IC8vIFNob3duIGJlbG93IHRoZSBpbnB1dCBmaWVsZCBpbiBteSBleGFtcGxlXHJcbiAgICAgIC8vIHNldEVtYWlsKHJvdXRlci5xdWVyeS5lbWFpbClcclxuICAgIH1cclxuICB9LCBbcm91dGVyXSk7XHJcblxyXG4gIGNvbnN0IGhhbmRsZUxvZ2luID0gKGUpID0+IHtcclxuICAgIGUucHJldmVudERlZmF1bHQoKVxyXG4gICAgaWYoZW1haWwgJiYgcHdkKXtcclxuICAgICAgc2lnbkluKCdjcmVkZW50aWFscycsIHtcclxuICAgICAgICBlbWFpbDogZW1haWwsXHJcbiAgICAgICAgcGFzc3dvcmQ6IHB3ZFxyXG4gICAgICB9KVxyXG4gICAgfVxyXG4gIHJldHVybiAoXHJcbiAgICA8ZGl2IGNsYXNzTmFtZT1cImJvcmRlciByb3VuZGVkLW1kIHAtMiBtdC00IHctOTYgbXgtYXV0byBib3JkZXItZ3JheS0zMDAgcmluZy00XCI+XHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtjbGFzc2VzLnBhcGVyfT5cclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgganVzdGlmeS1jZW50ZXIgY29udGVudC1jZW50ZXJcIj5cclxuICAgICAgICAgIDxBY2NvdW50Q2lyY2xlSWNvbiBjbGFzc05hbWU9XCJ0ZXh0LWJsdWUtNjAwIG1yLTJcIiBmb250U2l6ZT1cImxhcmdlXCIgLz5cclxuICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInRleHQtMnhsIHRleHQtYmx1ZS00MDBcIj5TaWduIGluPC9zcGFuPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxmb3JtXHJcbiAgICAgICAgICBjbGFzc05hbWU9e2NsYXNzZXMuZm9ybX1cclxuICAgICAgICAgIG1ldGhvZD1cIlBPU1RcIlxyXG4gICAgICAgICAgYWN0aW9uPVwiL2FwaS9hdXRoL2NhbGxiYWNrL2NyZWRlbnRpYWxzXCJcclxuICAgICAgICAgIG5vVmFsaWRhdGVcclxuICAgICAgICA+XHJcbiAgICAgICAgICA8aW5wdXQgbmFtZT1cImNzcmZUb2tlblwiIHR5cGU9XCJoaWRkZW5cIiBkZWZhdWx0VmFsdWU9e2NzcmZUb2tlbn0gLz5cclxuICAgICAgICAgIDxUZXh0RmllbGRcclxuICAgICAgICAgICAgLy8gdmFyaWFudD1cIm91dGxpbmVkXCJcclxuICAgICAgICAgICAgbWFyZ2luPVwibm9ybWFsXCJcclxuICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgICAgZnVsbFdpZHRoXHJcbiAgICAgICAgICAgIGlkPVwiZW1haWxcIlxyXG4gICAgICAgICAgICBsYWJlbD1cIkVtYWlsIEFkZHJlc3NcIlxyXG4gICAgICAgICAgICBuYW1lPVwiZW1haWxcIlxyXG4gICAgICAgICAgICB0eXBlPVwiZW1haWxcIlxyXG4gICAgICAgICAgICBhdXRvQ29tcGxldGU9XCJlbWFpbFwiXHJcbiAgICAgICAgICAgIHZhbHVlPXtlbWFpbH1cclxuICAgICAgICAgICAgYXV0b0ZvY3VzXHJcbiAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAgIElucHV0UHJvcHM9e3tcclxuICAgICAgICAgICAgICBzdGFydEFkb3JubWVudDogKFxyXG4gICAgICAgICAgICAgICAgPElucHV0QWRvcm5tZW50IHBvc2l0aW9uPVwic3RhcnRcIj5cclxuICAgICAgICAgICAgICAgICAgPEVtYWlsSWNvbiBjbGFzc05hbWU9XCJ0ZXh0LWJsdWUtNTAwXCIgLz5cclxuICAgICAgICAgICAgICAgIDwvSW5wdXRBZG9ybm1lbnQ+XHJcbiAgICAgICAgICAgICAgKSxcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRFbWFpbChlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAvPlxyXG4gICAgICAgICAgPFRleHRGaWVsZFxyXG4gICAgICAgICAgICAvLyB2YXJpYW50PVwib3V0bGluZWRcIlxyXG4gICAgICAgICAgICBtYXJnaW49XCJub3JtYWxcIlxyXG4gICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgICBmdWxsV2lkdGhcclxuICAgICAgICAgICAgbmFtZT1cInBhc3N3b3JkXCJcclxuICAgICAgICAgICAgbGFiZWw9XCJQYXNzd29yZFwiXHJcbiAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgIGlkPVwicGFzc3dvcmRcIlxyXG4gICAgICAgICAgICBhdXRvQ29tcGxldGU9XCJjdXJyZW50LXBhc3N3b3JkXCJcclxuICAgICAgICAgICAgSW5wdXRQcm9wcz17e1xyXG4gICAgICAgICAgICAgIHN0YXJ0QWRvcm5tZW50OiAoXHJcbiAgICAgICAgICAgICAgICA8SW5wdXRBZG9ybm1lbnQgcG9zaXRpb249XCJzdGFydFwiPlxyXG4gICAgICAgICAgICAgICAgICA8SHR0cHNJY29uIGNsYXNzTmFtZT1cInRleHQtYmx1ZS01MDBcIiAvPlxyXG4gICAgICAgICAgICAgICAgPC9JbnB1dEFkb3JubWVudD5cclxuICAgICAgICAgICAgICApLFxyXG4gICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldFB3ZChlLnRhcmdldC52YWx1ZSl9XHJcbiAgICAgICAgICAgIHJlcXVpcmVkXHJcbiAgICAgICAgICAvPlxyXG4gICAgICAgICAgPGxhYmVsPlxyXG4gICAgICAgICAgICA8aW5wdXRcclxuICAgICAgICAgICAgICB0eXBlPVwiY2hlY2tib3hcIlxyXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cIm1yLTJcIlxyXG4gICAgICAgICAgICAgIGNoZWNrZWQ9e2lzUmVtZW1iZXJ9XHJcbiAgICAgICAgICAgICAgb25DaGFuZ2U9eyhlKSA9PiBzZXRJc1JlbWVtYmVyKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgUmVtZW1iZXIgbWVcclxuICAgICAgICAgIDwvbGFiZWw+XHJcblxyXG4gICAgICAgICAge2xvZ2luRXJyb3IgJiYgKFxyXG4gICAgICAgICAgICA8QWxlcnQgc2V2ZXJpdHk9XCJlcnJvclwiIHZhcmlhbnQ9XCJvdXRsaW5lZFwiIGNsYXNzTmFtZT1cIm15LTJcIj5cclxuICAgICAgICAgICAgICB7bG9naW5FcnJvcn1cclxuICAgICAgICAgICAgPC9BbGVydD5cclxuICAgICAgICAgICl9XHJcbiAgICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICAgIHR5cGU9XCJzdWJtaXRcIlxyXG4gICAgICAgICAgICBmdWxsV2lkdGhcclxuICAgICAgICAgICAgdmFyaWFudD1cImNvbnRhaW5lZFwiXHJcbiAgICAgICAgICAgIGNvbG9yPVwicHJpbWFyeVwiXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT17Y2xhc3Nlcy5zdWJtaXR9XHJcbiAgICAgICAgICAgIG9uU3VibWl0PXsoZSkgPT4gaGFuZGxlTG9naW4oZSkgfVxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICBTaWduIEluXHJcbiAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICA8L2Zvcm0+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgICA8ZGl2IGNsYXNzTmFtZT1cIm15LTJcIj5cclxuICAgICAgICA8Q29weXJpZ2h0IC8+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgPC9kaXY+XHJcbiAgKTtcclxufX1cclxuXHJcbi8vIFNpZ25Jbi5nZXRJbml0aWFsUHJvcHMgPSBhc3luYyAoY29udGV4dCkgPT4ge1xyXG4vLyAgIHJldHVybiB7XHJcbi8vICAgICBjc3JmVG9rZW46IGF3YWl0IGNzcmZUb2tlbihjb250ZXh0KSxcclxuLy8gICB9O1xyXG4vLyB9O1xyXG5cclxuIiwiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0IEhlYWQgZnJvbSAnbmV4dC9oZWFkJ1xyXG5pbXBvcnQgTmF2QmFyIGZyb20gJy4uL2NvbXBvbmVudHMvbWFzdGVyL25hdmJhcidcclxuaW1wb3J0IEJyZWFkQ3J1bWIgZnJvbSAnLi4vY29tcG9uZW50cy9tYXN0ZXIvYnJlYWRjcnVtYnMnXHJcbmltcG9ydCBNYWluTWVudSBmcm9tICcuLi9jb21wb25lbnRzL21hc3Rlci9tYWlubWVudSdcclxuaW1wb3J0IEJ1dHRvbiBmcm9tICdAbWF0ZXJpYWwtdWkvY29yZS9CdXR0b24nO1xyXG5pbXBvcnQgVGV4dEZpZWxkIGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlL1RleHRGaWVsZCc7XHJcbmltcG9ydCBHcmlkIGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlL0dyaWQnO1xyXG5pbXBvcnQgeyB1c2VTZXNzaW9uIH0gZnJvbSAnbmV4dC1hdXRoL2NsaWVudCdcclxuaW1wb3J0IEFsZXJ0IGZyb20gJ0BtYXRlcmlhbC11aS9sYWIvQWxlcnQnXHJcbmltcG9ydCB7IGlzRW1wdHkgfSBmcm9tICcuL2NvbW1vbi92YWxpZGF0ZXV0aWxzJ1xyXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBDaGFuZ2VQYXNzKCl7XHJcbiAgICBjb25zdCBbIHNlc3Npb25dID0gdXNlU2Vzc2lvbigpXHJcbiAgICBjb25zdCBbbWVzc2FnZSwgc2V0TWVzc2FnZV0gPSBSZWFjdC51c2VTdGF0ZSgnJyk7XHJcbiAgICBjb25zdCBbZXJyb3JUZXh0LCBzZXRFcnJvclRleHRdID0gUmVhY3QudXNlU3RhdGUoJycpO1xyXG4gICAgY29uc3QgW2Vycm9yLCBzZXRFcnJvcl0gPSBSZWFjdC51c2VTdGF0ZShmYWxzZSk7XHJcbiAgICBjb25zdCBbb2xkcHdkLHNldE9sZFB3ZF09ICBSZWFjdC51c2VTdGF0ZShcIlwiKVxyXG4gICAgY29uc3QgW25ld3B3ZCxzZXROZXdQd2RdPSAgUmVhY3QudXNlU3RhdGUoXCJcIilcclxuICAgIGNvbnN0IFtjb25maXJtcHdkLHNldENvbmZpcm1Qd2RdPSAgUmVhY3QudXNlU3RhdGUoXCJcIilcclxuICAgIGNvbnN0IFtwd2RFcnIsU2V0UHdkRXJyXSA9IFJlYWN0LnVzZVN0YXRlKGZhbHNlKVxyXG4gICAgY29uc3QgW25ld3B3ZEVycixTZXROZXdQd2RFcnJdID0gUmVhY3QudXNlU3RhdGUoZmFsc2UpXHJcbiAgICBjb25zdCBbcHdkY29uZmlybUVycixTZXRDb25maXJtUHdkRXJyXSA9IFJlYWN0LnVzZVN0YXRlKGZhbHNlKVxyXG4gICAgY29uc3QgW21hdGNoRXJyLFNldE1hdGNoRXJyXSA9IFJlYWN0LnVzZVN0YXRlKGZhbHNlKVxyXG5cclxuICAgIGNvbnN0IHZhbGlkRmllbGRzID0gKCkgPT4ge1xyXG4gICAgICAgIGlmKGlzRW1wdHkob2xkcHdkLCBTZXRQd2RFcnIpKXtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWVcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYoaXNFbXB0eShuZXdwd2QsIFNldE5ld1B3ZEVycikpe1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZihpc0VtcHR5KGNvbmZpcm1wd2QsIFNldENvbmZpcm1Qd2RFcnIpKXtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWVcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYobmV3cHdkICE9IGNvbmZpcm1wd2Qpe1xyXG4gICAgICAgICAgICBTZXRNYXRjaEVycih0cnVlKVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZVxyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICBTZXRNYXRjaEVycihmYWxzZSlcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlXHJcbiAgICB9XHJcbiAgICBjb25zdCBoYW5kbGVDaGFuZ2VQd2QgPSAoZSkgPT4ge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKVxyXG4gICAgICAgIFxyXG4gICAgICAgIGlmKCF2YWxpZEZpZWxkcygpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgICAgICBtZXRob2Q6ICdQT1NUJyxcclxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHsgJ2NvbnRlbnQtdHlwZSc6ICdhcHBsaWNhdGlvbi9qc29uJywgJ0F1dGhvcml6YXRpb24nOiBzZXNzaW9uLmFjY2Vzc1Rva2VufSxcclxuICAgICAgICAgICAgICAgIGRhdGE6e1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOnNlc3Npb24/LnVzZXI/LnVzZXJpZCxcclxuICAgICAgICAgICAgICAgICAgICBvbGRQYXNzd29yZDpvbGRwd2QsXHJcbiAgICAgICAgICAgICAgICAgICAgbmV3UGFzc3dvcmQ6bmV3cHdkXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgYmFzZVVSTDogJ2h0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9hcGkvJyxcclxuICAgICAgICAgICAgICAgIHVybDogJ3VzZXIvY2hhbmdlcHdkJ1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgXHJcbiAgICAgICAgICAgIGF4aW9zKG9wdGlvbnMpXHJcbiAgICAgICAgICAgIC50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgaWYocmVzcG9uc2UuZGF0YSA9PSAwKXtcclxuICAgICAgICAgICAgICAgICAgICBzZXRFcnJvcihmYWxzZSlcclxuICAgICAgICAgICAgICAgICAgICBTZXRQd2RFcnIodHJ1ZSlcclxuICAgICAgICAgICAgICAgICAgICBzZXRFcnJvclRleHQoXCJPbGQgcGFzc3dvcmQgaXNuJ3QgbWF0Y2hcIik7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICBzZXRFcnJvcihmYWxzZSlcclxuICAgICAgICAgICAgICAgICAgICBzZXRNZXNzYWdlKFwiVXBkYXRlZCBzdWNjZXNzZnVsbHkhXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNldEVycm9yVGV4dChcIlwiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmNhdGNoKGZ1bmN0aW9uIChlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgc2V0RXJyb3IodHJ1ZSlcclxuICAgICAgICAgICAgICAgIHNldE1lc3NhZ2UoXCJFcnJvclwiKVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4oXHJcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJcIj5cclxuICAgICAgICAgICAgPEhlYWQ+XHJcbiAgICAgICAgICAgICAgICA8dGl0bGU+Q3JhenkgV2ViPC90aXRsZT5cclxuICAgICAgICAgICAgICAgIDxsaW5rIHJlbD1cImljb25cIiBocmVmPVwiL2Zhdmljb24uaWNvXCIgLz5cclxuICAgICAgICAgICAgPC9IZWFkPlxyXG4gICAgICAgICAgICA8bWFpbiBjbGFzc05hbWU9XCJoLXNjcmVlblwiPlxyXG4gICAgICAgICAgICAgICAgPE5hdkJhciAvPlxyXG4gICAgICAgICAgICAgICAgPEJyZWFkQ3J1bWIgY2xhc3NOYW1lPVwiYmctZ3JheS00MDBcIi8+XHJcbiAgICAgICAgICAgICAgICA8TWFpbk1lbnUgLz5cclxuICAgICAgICAgICAgICAgIDxmb3JtIGNsYXNzTmFtZT1cInctOTYgbXgtYXV0byBib3JkZXIgcC00IG10LTRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7bWVzc2FnZSAmJiA8QWxlcnQgY29sb3I9e2Vycm9yID8gXCJlcnJvclwiIDogXCJzdWNjZXNzXCJ9IHNldmVyaXR5PXtlcnJvciA/IFwiZXJyb3JcIiA6IFwic3VjY2Vzc1wifSBjbGFzc05hbWU9XCJwYi0yXCI+e21lc3NhZ2V9PC9BbGVydD59XHJcbiAgICAgICAgICAgICAgICAgICAgPEdyaWQgaXRlbSB4cz17MTJ9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxUZXh0RmllbGQgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwic3RhbmRhcmQtYmFzaWNcIiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9XCJPbGQgUGFzc3dvcmRcIiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0T2xkUHdkKGUudGFyZ2V0LnZhbHVlKX0gXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yPXtwd2RFcnJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlbHBlclRleHQ9e3B3ZEVyciA/IGVycm9yVGV4dCA6IFwiXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGF1dG9Gb2N1c1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmdWxsV2lkdGggLz5cclxuICAgICAgICAgICAgICAgICAgICA8L0dyaWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPGJyLz5cclxuICAgICAgICAgICAgICAgICAgICA8R3JpZCAgaXRlbSB4cz17MTJ9PlxyXG4gICAgICAgICAgICAgICAgICAgIDxUZXh0RmllbGQgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkPVwic3RhbmRhcmQtYmFzaWNcIiBcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw9XCJOZXcgUGFzc3dvcmRcIiAgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVycm9yPXtuZXdwd2RFcnJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlbHBlclRleHQ9e25ld3B3ZEVyciA/IFwiTmV3IHBhc3N3b3JkIGludmFsaWRcIiA6IFwiXCJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9uQ2hhbmdlPXsoZSkgPT4gc2V0TmV3UHdkKGUudGFyZ2V0LnZhbHVlKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWRcclxuICAgICAgICAgICAgICAgICAgICAgICAgZnVsbFdpZHRoIC8+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9HcmlkPlxyXG4gICAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICAgIDxici8+XHJcbiAgICAgICAgICAgICAgICAgICAgPEdyaWQgIGl0ZW0geHM9ezEyfT5cclxuICAgICAgICAgICAgICAgICAgICA8VGV4dEZpZWxkIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZD1cInN0YW5kYXJkLWJhc2ljXCIgXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsPVwiQ29uZmlybSBQYXNzd29yZFwiICBcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInBhc3N3b3JkXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgZXJyb3I9e3B3ZGNvbmZpcm1FcnIgfHwgbWF0Y2hFcnJ9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlbHBlclRleHQ9e3B3ZGNvbmZpcm1FcnIgPyBcIkNvbmZpcm0gcGFzc3dvcmQgaW52YWxpZFwiIDogbWF0Y2hFcnIgPyBcIkNvbmZpcm0gcGFzc3dvcmQgbm90IG1hdGNoXCIgOiBcIlwifVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17KGUpID0+IHNldENvbmZpcm1Qd2QoZS50YXJnZXQudmFsdWUpfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmdWxsV2lkdGggLz5cclxuICAgICAgICAgICAgICAgICAgICA8L0dyaWQ+XHJcbiAgICAgICAgICAgICAgICAgICAgPGJyLz5cclxuICAgICAgICAgICAgICAgICAgICA8R3JpZCBpdGVtIHhzPXsxMn0+XHJcbiAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBvbkNsaWNrID17KGUpID0+IGhhbmRsZUNoYW5nZVB3ZChlKX1cclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZT1cInN1Ym1pdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZ1bGxXaWR0aFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXJpYW50PVwiY29udGFpbmVkXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sb3I9XCJwcmltYXJ5XCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gY2xhc3NOYW1lPXtjbGFzc2VzLnN1Ym1pdH1cclxuICAgICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIENvbmZpcm1cclxuICAgICAgICAgICAgICAgICAgICA8L0J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvR3JpZD5cclxuICAgICAgICAgICAgICAgIDwvZm9ybT5cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgXHJcblxyXG4gICAgICAgICAgICA8L21haW4+XHJcbiAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgKVxyXG59IiwiaW1wb3J0IHsgRmFzdGZvb2RPdXRsaW5lZCB9IGZyb20gXCJAbWF0ZXJpYWwtdWkvaWNvbnNcIlxyXG5cclxuY29uc3QgcmVnZXhFbWFpbCA9IC9eW1xcdy1cXC5dK0AoW1xcdy1dK1xcLikrW1xcdy1dezIsNH0kL2dcclxuY29uc3QgcmVnZXhPbmx5TnVtYmVyID0gL15bMC05XSokL2dcclxuXHJcbi8vIGNvbnN0IGlzRW1wdHkgPSAodmFsdWUpID0+IHZhbHVlLnRyaW0oKSA9PSAnJyA/IHRydWUgOiBmYWxzZVxyXG5jb25zdCBpc0VtcHR5ID0gKHZhbHVlLCBzZXRTdGF0ZSkgPT4geyBcclxuICAgIGlmKHZhbHVlLnRyaW0oKSA9PSAnJyl7XHJcbiAgICAgICAgc2V0U3RhdGUodHJ1ZSlcclxuICAgICAgICByZXR1cm4gdHJ1ZVxyXG4gICAgfVxyXG4gICAgc2V0U3RhdGUoZmFsc2UpXHJcbiAgICByZXR1cm4gZmFsc2VcclxufVxyXG5jb25zdCBpc0VtYWlsID0gKHZhbHVlKSA9PiByZWdleEVtYWlsLnRlc3QodmFsdWUpIFxyXG5jb25zdCBpc051bWJlciA9ICh2YWx1ZSkgPT4gcmVnZXhPbmx5TnVtYmVyLnRlc3QodmFsdWUpIFxyXG5cclxuZXhwb3J0IHsgaXNFbXB0eSwgaXNFbWFpbCwgaXNOdW1iZXIgfSIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9jb3JlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9jb3JlL0JveFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9CcmVhZGNydW1ic1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9CdXR0b25cIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2NvcmUvQ2FyZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9DaGVja2JveFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9Gb3JtQ29udHJvbExhYmVsXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9jb3JlL0dyaWRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2NvcmUvSW5wdXRBZG9ybm1lbnRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2NvcmUvTGlua1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9QYXBlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9UYWJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2NvcmUvVGFic1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9UZXh0RmllbGRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2NvcmUvVHlwb2dyYXBoeVwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9zdHlsZXNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2ljb25zXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9pY29ucy9BY2NvdW50Q2lyY2xlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9pY29ucy9Bc3Nlc3NtZW50XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9pY29ucy9FbWFpbFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvaWNvbnMvSHR0cHNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2ljb25zL0xvY2FsT2ZmZXJSb3VuZGVkXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9pY29ucy9NZWV0aW5nUm9vbVJvdW5kZWRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2ljb25zL05hdmlnYXRlTmV4dFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvaWNvbnMvUGVvcGxlT3V0bGluZVJvdW5kZWRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2ljb25zL1BlcnNvblwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvaWNvbnMvUGV0c1JvdW5kZWRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2ljb25zL1JlZGVlbVJvdW5kZWRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2ljb25zL1Jvb21TZXJ2aWNlUm91bmRlZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvaWNvbnMvU3RhclJvdW5kZWRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2xhYi9BbGVydFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJheGlvc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0LWF1dGgvY2xpZW50XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvaGVhZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L3JvdXRlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7Il0sInNvdXJjZVJvb3QiOiIifQ==