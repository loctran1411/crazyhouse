module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/api/auth/[...nextauth].js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./pages/api/auth/[...nextauth].js":
/*!*****************************************!*\
  !*** ./pages/api/auth/[...nextauth].js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var next_auth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next-auth */ "next-auth");
/* harmony import */ var next_auth__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_auth__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_auth_providers__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next-auth/providers */ "next-auth/providers");
/* harmony import */ var next_auth_providers__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_auth_providers__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "axios");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);



/* harmony default export */ __webpack_exports__["default"] = (next_auth__WEBPACK_IMPORTED_MODULE_0___default()({
  providers: [next_auth_providers__WEBPACK_IMPORTED_MODULE_1___default.a.Credentials({
    name: 'Credentials',

    async authorize(credentials) {
      try {
        const user = await axios__WEBPACK_IMPORTED_MODULE_2___default.a.post("http://localhost:8080/api/login", {
          email: credentials.email,
          password: credentials.password
        }, {
          headers: {
            accept: '*/*',
            'content-type': 'application/json'
          }
        }).then(res => res.data);

        if (user) {
          return user;
        } else {
          throw new Error('email or password invalid' + '&email=' + credentials.email);
        }
      } catch (e) {
        const error = e.message;
        throw new Error(error + '&email=' + credentials.email);
      }
    }

  })],
  pages: {
    signIn: '/auth/signin',
    signOut: '/auth/signin',
    error: '/auth/signin'
  },
  callbacks: {
    async signIn(user, account, profile) {
      return true;
    },

    async redirect(url, baseUrl) {
      return baseUrl;
    },

    async session(session, token) {
      if (token !== null && token !== void 0 && token.accessToken) {
        session.accessToken = token.accessToken;
        session.user = token.user;
      }

      return session;
    },

    async jwt(token, user, account, profile, isNewUser) {
      if (user !== null && user !== void 0 && user.accessToken) {
        token.accessToken = user.accessToken;
        token.user = user;
      }

      return token;
    }

  },
  logger: {
    error(code, ...message) {
      console.error(code, message);
    },

    warn(code, ...message) {
      console.error(code, message);
    },

    debug(code, ...message) {
      console.error(code, message);
    }

  }
}));

/***/ }),

/***/ "axios":
/*!************************!*\
  !*** external "axios" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "next-auth":
/*!****************************!*\
  !*** external "next-auth" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-auth");

/***/ }),

/***/ "next-auth/providers":
/*!**************************************!*\
  !*** external "next-auth/providers" ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-auth/providers");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vcGFnZXMvYXBpL2F1dGgvWy4uLm5leHRhdXRoXS5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJheGlvc1wiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm5leHQtYXV0aFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIm5leHQtYXV0aC9wcm92aWRlcnNcIiJdLCJuYW1lcyI6WyJOZXh0QXV0aCIsInByb3ZpZGVycyIsIlByb3ZpZGVycyIsIkNyZWRlbnRpYWxzIiwibmFtZSIsImF1dGhvcml6ZSIsImNyZWRlbnRpYWxzIiwidXNlciIsImF4aW9zIiwicG9zdCIsImVtYWlsIiwicGFzc3dvcmQiLCJoZWFkZXJzIiwiYWNjZXB0IiwidGhlbiIsInJlcyIsImRhdGEiLCJFcnJvciIsImUiLCJlcnJvciIsIm1lc3NhZ2UiLCJwYWdlcyIsInNpZ25JbiIsInNpZ25PdXQiLCJjYWxsYmFja3MiLCJhY2NvdW50IiwicHJvZmlsZSIsInJlZGlyZWN0IiwidXJsIiwiYmFzZVVybCIsInNlc3Npb24iLCJ0b2tlbiIsImFjY2Vzc1Rva2VuIiwiand0IiwiaXNOZXdVc2VyIiwibG9nZ2VyIiwiY29kZSIsImNvbnNvbGUiLCJ3YXJuIiwiZGVidWciXSwibWFwcGluZ3MiOiI7O1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxJQUFJO1FBQ0o7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUN4RkE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQ0E7QUFFZUEsK0dBQVEsQ0FBQztBQUNwQkMsV0FBUyxFQUFFLENBQ1BDLDBEQUFTLENBQUNDLFdBQVYsQ0FBc0I7QUFDbEJDLFFBQUksRUFBRSxhQURZOztBQUVsQixVQUFNQyxTQUFOLENBQWdCQyxXQUFoQixFQUE2QjtBQUMzQixVQUFHO0FBQ0MsY0FBTUMsSUFBSSxHQUFHLE1BQU1DLDRDQUFLLENBQUNDLElBQU4sQ0FBVyxpQ0FBWCxFQUNuQjtBQUNJQyxlQUFLLEVBQUVKLFdBQVcsQ0FBQ0ksS0FEdkI7QUFFSUMsa0JBQVEsRUFBRUwsV0FBVyxDQUFDSztBQUYxQixTQURtQixFQUtuQjtBQUNJQyxpQkFBTyxFQUFDO0FBQ0pDLGtCQUFNLEVBQUUsS0FESjtBQUVKLDRCQUFnQjtBQUZaO0FBRFosU0FMbUIsRUFVaEJDLElBVmdCLENBVVhDLEdBQUcsSUFBSUEsR0FBRyxDQUFDQyxJQVZBLENBQW5COztBQVdBLFlBQUlULElBQUosRUFBVTtBQUNSLGlCQUFPQSxJQUFQO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsZ0JBQU0sSUFBSVUsS0FBSixDQUFVLDhCQUE4QixTQUE5QixHQUEwQ1gsV0FBVyxDQUFDSSxLQUFoRSxDQUFOO0FBQ0Q7QUFFRixPQWxCSCxDQWtCRyxPQUFNUSxDQUFOLEVBQVE7QUFDUCxjQUFNQyxLQUFLLEdBQUdELENBQUMsQ0FBQ0UsT0FBaEI7QUFDQSxjQUFNLElBQUlILEtBQUosQ0FBVUUsS0FBSyxHQUFHLFNBQVIsR0FBb0JiLFdBQVcsQ0FBQ0ksS0FBMUMsQ0FBTjtBQUNEO0FBRUY7O0FBMUJlLEdBQXRCLENBRE8sQ0FEUztBQStCcEJXLE9BQUssRUFBRTtBQUNIQyxVQUFNLEVBQUUsY0FETDtBQUVIQyxXQUFPLEVBQUUsY0FGTjtBQUdISixTQUFLLEVBQUU7QUFISixHQS9CYTtBQW9DcEJLLFdBQVMsRUFBRTtBQUNULFVBQU1GLE1BQU4sQ0FBYWYsSUFBYixFQUFtQmtCLE9BQW5CLEVBQTRCQyxPQUE1QixFQUFxQztBQUNuQyxhQUFPLElBQVA7QUFDRCxLQUhROztBQUlULFVBQU1DLFFBQU4sQ0FBZUMsR0FBZixFQUFvQkMsT0FBcEIsRUFBNkI7QUFDM0IsYUFBT0EsT0FBUDtBQUNELEtBTlE7O0FBT1QsVUFBTUMsT0FBTixDQUFjQSxPQUFkLEVBQXVCQyxLQUF2QixFQUE4QjtBQUM1QixVQUFHQSxLQUFILGFBQUdBLEtBQUgsZUFBR0EsS0FBSyxDQUFFQyxXQUFWLEVBQXNCO0FBQ3BCRixlQUFPLENBQUNFLFdBQVIsR0FBc0JELEtBQUssQ0FBQ0MsV0FBNUI7QUFDQUYsZUFBTyxDQUFDdkIsSUFBUixHQUFld0IsS0FBSyxDQUFDeEIsSUFBckI7QUFDRDs7QUFDRCxhQUFPdUIsT0FBUDtBQUNELEtBYlE7O0FBY1QsVUFBTUcsR0FBTixDQUFVRixLQUFWLEVBQWlCeEIsSUFBakIsRUFBdUJrQixPQUF2QixFQUFnQ0MsT0FBaEMsRUFBeUNRLFNBQXpDLEVBQW9EO0FBQ2xELFVBQUczQixJQUFILGFBQUdBLElBQUgsZUFBR0EsSUFBSSxDQUFFeUIsV0FBVCxFQUFxQjtBQUNuQkQsYUFBSyxDQUFDQyxXQUFOLEdBQW9CekIsSUFBSSxDQUFDeUIsV0FBekI7QUFDQUQsYUFBSyxDQUFDeEIsSUFBTixHQUFhQSxJQUFiO0FBQ0Q7O0FBQ0QsYUFBT3dCLEtBQVA7QUFDRDs7QUFwQlEsR0FwQ1M7QUEwRHBCSSxRQUFNLEVBQUU7QUFDSmhCLFNBQUssQ0FBQ2lCLElBQUQsRUFBTyxHQUFHaEIsT0FBVixFQUFtQjtBQUN0QmlCLGFBQU8sQ0FBQ2xCLEtBQVIsQ0FBY2lCLElBQWQsRUFBb0JoQixPQUFwQjtBQUNELEtBSEc7O0FBSUprQixRQUFJLENBQUNGLElBQUQsRUFBTyxHQUFHaEIsT0FBVixFQUFtQjtBQUNyQmlCLGFBQU8sQ0FBQ2xCLEtBQVIsQ0FBY2lCLElBQWQsRUFBb0JoQixPQUFwQjtBQUNELEtBTkc7O0FBT0ptQixTQUFLLENBQUNILElBQUQsRUFBTyxHQUFHaEIsT0FBVixFQUFtQjtBQUN0QmlCLGFBQU8sQ0FBQ2xCLEtBQVIsQ0FBY2lCLElBQWQsRUFBb0JoQixPQUFwQjtBQUNEOztBQVRHO0FBMURZLENBQUQsQ0FBdkIsRTs7Ozs7Ozs7Ozs7QUNKQSxrQzs7Ozs7Ozs7Ozs7QUNBQSxzQzs7Ozs7Ozs7Ozs7QUNBQSxnRCIsImZpbGUiOiJwYWdlcy9hcGkvYXV0aC9bLi4ubmV4dGF1dGhdLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSByZXF1aXJlKCcuLi8uLi8uLi9zc3ItbW9kdWxlLWNhY2hlLmpzJyk7XG5cbiBcdC8vIFRoZSByZXF1aXJlIGZ1bmN0aW9uXG4gXHRmdW5jdGlvbiBfX3dlYnBhY2tfcmVxdWlyZV9fKG1vZHVsZUlkKSB7XG5cbiBcdFx0Ly8gQ2hlY2sgaWYgbW9kdWxlIGlzIGluIGNhY2hlXG4gXHRcdGlmKGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdKSB7XG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG4gXHRcdH1cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdHZhciB0aHJldyA9IHRydWU7XG4gXHRcdHRyeSB7XG4gXHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG4gXHRcdFx0dGhyZXcgPSBmYWxzZTtcbiBcdFx0fSBmaW5hbGx5IHtcbiBcdFx0XHRpZih0aHJldykgZGVsZXRlIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdO1xuIFx0XHR9XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9wYWdlcy9hcGkvYXV0aC9bLi4ubmV4dGF1dGhdLmpzXCIpO1xuIiwiaW1wb3J0IE5leHRBdXRoIGZyb20gJ25leHQtYXV0aCdcclxuaW1wb3J0IFByb3ZpZGVycyBmcm9tICduZXh0LWF1dGgvcHJvdmlkZXJzJ1xyXG5pbXBvcnQgYXhpb3MgZnJvbSAnYXhpb3MnXHJcblxyXG5leHBvcnQgZGVmYXVsdCBOZXh0QXV0aCh7XHJcbiAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICBQcm92aWRlcnMuQ3JlZGVudGlhbHMoe1xyXG4gICAgICAgICAgICBuYW1lOiAnQ3JlZGVudGlhbHMnLFxyXG4gICAgICAgICAgICBhc3luYyBhdXRob3JpemUoY3JlZGVudGlhbHMpIHtcclxuICAgICAgICAgICAgICB0cnl7XHJcbiAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXIgPSBhd2FpdCBheGlvcy5wb3N0KFwiaHR0cDovL2xvY2FsaG9zdDo4MDgwL2FwaS9sb2dpblwiLFxyXG4gICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBlbWFpbDogY3JlZGVudGlhbHMuZW1haWwsXHJcbiAgICAgICAgICAgICAgICAgICAgICBwYXNzd29yZDogY3JlZGVudGlhbHMucGFzc3dvcmRcclxuICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczp7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgYWNjZXB0OiAnKi8qJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAnY29udGVudC10eXBlJzogJ2FwcGxpY2F0aW9uL2pzb24nXHJcbiAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIH0pLnRoZW4ocmVzID0+IHJlcy5kYXRhKVxyXG4gICAgICAgICAgICAgICAgICBpZiAodXNlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB1c2VyXHJcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdlbWFpbCBvciBwYXNzd29yZCBpbnZhbGlkJyArICcmZW1haWw9JyArIGNyZWRlbnRpYWxzLmVtYWlsKVxyXG4gICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgfWNhdGNoKGUpe1xyXG4gICAgICAgICAgICAgICAgICBjb25zdCBlcnJvciA9IGUubWVzc2FnZVxyXG4gICAgICAgICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoZXJyb3IgKyAnJmVtYWlsPScgKyBjcmVkZW50aWFscy5lbWFpbClcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgXSxcclxuICAgIHBhZ2VzOiB7XHJcbiAgICAgICAgc2lnbkluOiAnL2F1dGgvc2lnbmluJyxcclxuICAgICAgICBzaWduT3V0OiAnL2F1dGgvc2lnbmluJyxcclxuICAgICAgICBlcnJvcjogJy9hdXRoL3NpZ25pbidcclxuICAgIH0sXHJcbiAgICBjYWxsYmFja3M6IHtcclxuICAgICAgYXN5bmMgc2lnbkluKHVzZXIsIGFjY291bnQsIHByb2ZpbGUpIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZVxyXG4gICAgICB9LFxyXG4gICAgICBhc3luYyByZWRpcmVjdCh1cmwsIGJhc2VVcmwpIHtcclxuICAgICAgICByZXR1cm4gYmFzZVVybFxyXG4gICAgICB9LFxyXG4gICAgICBhc3luYyBzZXNzaW9uKHNlc3Npb24sIHRva2VuKSB7XHJcbiAgICAgICAgaWYodG9rZW4/LmFjY2Vzc1Rva2VuKXtcclxuICAgICAgICAgIHNlc3Npb24uYWNjZXNzVG9rZW4gPSB0b2tlbi5hY2Nlc3NUb2tlblxyXG4gICAgICAgICAgc2Vzc2lvbi51c2VyID0gdG9rZW4udXNlclxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gc2Vzc2lvblxyXG4gICAgICB9LFxyXG4gICAgICBhc3luYyBqd3QodG9rZW4sIHVzZXIsIGFjY291bnQsIHByb2ZpbGUsIGlzTmV3VXNlcikge1xyXG4gICAgICAgIGlmKHVzZXI/LmFjY2Vzc1Rva2VuKXtcclxuICAgICAgICAgIHRva2VuLmFjY2Vzc1Rva2VuID0gdXNlci5hY2Nlc3NUb2tlblxyXG4gICAgICAgICAgdG9rZW4udXNlciA9IHVzZXJcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRva2VuXHJcbiAgICAgIH1cclxuICAgIH0sXHJcbiAgICBsb2dnZXI6IHtcclxuICAgICAgICBlcnJvcihjb2RlLCAuLi5tZXNzYWdlKSB7XHJcbiAgICAgICAgICBjb25zb2xlLmVycm9yKGNvZGUsIG1lc3NhZ2UpXHJcbiAgICAgICAgfSxcclxuICAgICAgICB3YXJuKGNvZGUsIC4uLm1lc3NhZ2UpIHtcclxuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoY29kZSwgbWVzc2FnZSlcclxuICAgICAgICB9LFxyXG4gICAgICAgIGRlYnVnKGNvZGUsIC4uLm1lc3NhZ2UpIHtcclxuICAgICAgICAgIGNvbnNvbGUuZXJyb3IoY29kZSwgbWVzc2FnZSlcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pXHJcblxyXG4iLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJheGlvc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0LWF1dGhcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC1hdXRoL3Byb3ZpZGVyc1wiKTsiXSwic291cmNlUm9vdCI6IiJ9