module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./pages/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/master/breadcrumbs.js":
/*!******************************************!*\
  !*** ./components/master/breadcrumbs.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _material_ui_core_Breadcrumbs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-ui/core/Breadcrumbs */ "@material-ui/core/Breadcrumbs");
/* harmony import */ var _material_ui_core_Breadcrumbs__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Breadcrumbs__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Typography */ "@material-ui/core/Typography");
/* harmony import */ var _material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Link */ "@material-ui/core/Link");
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/icons/NavigateNext */ "@material-ui/icons/NavigateNext");
/* harmony import */ var _material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_NavigateNext__WEBPACK_IMPORTED_MODULE_4__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\components\\master\\breadcrumbs.js";





function BreadCrumb() {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
    className: "py-2 ml-4",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Breadcrumbs__WEBPACK_IMPORTED_MODULE_1___default.a, {
      separator: "\u203A",
      "aria-label": "breadcrumb",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_3___default.a, {
        color: "inherit",
        href: "/",
        children: "Home"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 10,
        columnNumber: 17
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Typography__WEBPACK_IMPORTED_MODULE_2___default.a, {
        color: "textPrimary",
        children: "Account"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 13,
        columnNumber: 17
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 9,
      columnNumber: 13
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 8,
    columnNumber: 9
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (BreadCrumb);

/***/ }),

/***/ "./components/master/functionnav.js":
/*!******************************************!*\
  !*** ./components/master/functionnav.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/styles */ "@material-ui/core/styles");
/* harmony import */ var _material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\components\\master\\functionnav.js";


const useStyles = Object(_material_ui_core_styles__WEBPACK_IMPORTED_MODULE_2__["makeStyles"])({
  root: {
    backgroundColor: "transparent",
    display: 'flex',
    justifyContent: 'center'
  }
});

function FunctionNav({
  children
}) {
  const classes = useStyles();
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
    className: "sticky bottom-0 left-1 bg-gray-100 border",
    children: children
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 19,
    columnNumber: 9
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (FunctionNav);

/***/ }),

/***/ "./components/master/mainmenu.js":
/*!***************************************!*\
  !*** ./components/master/mainmenu.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/core/Paper */ "@material-ui/core/Paper");
/* harmony import */ var _material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @material-ui/core/Tabs */ "@material-ui/core/Tabs");
/* harmony import */ var _material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/Tab */ "@material-ui/core/Tab");
/* harmony import */ var _material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_icons_PeopleOutlineRounded__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/icons/PeopleOutlineRounded */ "@material-ui/icons/PeopleOutlineRounded");
/* harmony import */ var _material_ui_icons_PeopleOutlineRounded__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_PeopleOutlineRounded__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _material_ui_icons_RoomServiceRounded__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @material-ui/icons/RoomServiceRounded */ "@material-ui/icons/RoomServiceRounded");
/* harmony import */ var _material_ui_icons_RoomServiceRounded__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_RoomServiceRounded__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _material_ui_icons_MeetingRoomRounded__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @material-ui/icons/MeetingRoomRounded */ "@material-ui/icons/MeetingRoomRounded");
/* harmony import */ var _material_ui_icons_MeetingRoomRounded__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_MeetingRoomRounded__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _material_ui_icons_StarRounded__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @material-ui/icons/StarRounded */ "@material-ui/icons/StarRounded");
/* harmony import */ var _material_ui_icons_StarRounded__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_StarRounded__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _material_ui_icons_RedeemRounded__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @material-ui/icons/RedeemRounded */ "@material-ui/icons/RedeemRounded");
/* harmony import */ var _material_ui_icons_RedeemRounded__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_RedeemRounded__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _material_ui_icons_LocalOfferRounded__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @material-ui/icons/LocalOfferRounded */ "@material-ui/icons/LocalOfferRounded");
/* harmony import */ var _material_ui_icons_LocalOfferRounded__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_LocalOfferRounded__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _material_ui_icons_Assessment__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @material-ui/icons/Assessment */ "@material-ui/icons/Assessment");
/* harmony import */ var _material_ui_icons_Assessment__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Assessment__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var _material_ui_core_Card__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @material-ui/core/Card */ "@material-ui/core/Card");
/* harmony import */ var _material_ui_core_Card__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Card__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var _material_ui_icons_Person__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @material-ui/icons/Person */ "@material-ui/icons/Person");
/* harmony import */ var _material_ui_icons_Person__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_Person__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! next-auth/client */ "next-auth/client");
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(next_auth_client__WEBPACK_IMPORTED_MODULE_15__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\components\\master\\mainmenu.js";
















function MainMenu() {
  var _session$user;

  const [session] = Object(next_auth_client__WEBPACK_IMPORTED_MODULE_15__["useSession"])();
  const [value, setValue] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState();
  const router = Object(next_router__WEBPACK_IMPORTED_MODULE_14__["useRouter"])();
  const {
    tab
  } = router.query;
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Card__WEBPACK_IMPORTED_MODULE_12___default.a, {
    variant: "outlined",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Paper__WEBPACK_IMPORTED_MODULE_2___default.a, {
      square: true,
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tabs__WEBPACK_IMPORTED_MODULE_3___default.a, {
        value: parseInt(tab ? tab : '0'),
        textColor: "primary",
        indicatorColor: "primary",
        "aria-label": "disabled tabs example",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/profile",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_Person__WEBPACK_IMPORTED_MODULE_13___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 32,
              columnNumber: 77
            }, this),
            label: "welcome, " + (session === null || session === void 0 ? void 0 : (_session$user = session.user) === null || _session$user === void 0 ? void 0 : _session$user.fullName),
            className: "text-indigo-600"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 32,
            columnNumber: 66
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/account?tab=1",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Account",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_PeopleOutlineRounded__WEBPACK_IMPORTED_MODULE_5___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 33,
              columnNumber: 99
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 72
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/booking?tab=2",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Booking",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_RoomServiceRounded__WEBPACK_IMPORTED_MODULE_6___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 34,
              columnNumber: 99
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 34,
            columnNumber: 72
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 34,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/room?tab=3",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Room",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_MeetingRoomRounded__WEBPACK_IMPORTED_MODULE_7___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 35,
              columnNumber: 93
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 35,
            columnNumber: 69
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 35,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/combo?tab=4",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Combo",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_LocalOfferRounded__WEBPACK_IMPORTED_MODULE_10___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 36,
              columnNumber: 95
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 36,
            columnNumber: 70
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 36,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/promotion?tab=5",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Promotion",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_RedeemRounded__WEBPACK_IMPORTED_MODULE_9___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 37,
              columnNumber: 103
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 37,
            columnNumber: 74
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 37,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/event?tab=6",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Event",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_StarRounded__WEBPACK_IMPORTED_MODULE_8___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 38,
              columnNumber: 95
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 38,
            columnNumber: 70
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 38,
          columnNumber: 13
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("a", {
          href: "/report?tab=7",
          className: "hover:text-indigo-600",
          children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Tab__WEBPACK_IMPORTED_MODULE_4___default.a, {
            label: "Report",
            icon: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_Assessment__WEBPACK_IMPORTED_MODULE_11___default.a, {}, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 39,
              columnNumber: 97
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 39,
            columnNumber: 71
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 39,
          columnNumber: 13
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 11
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 9
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 24,
    columnNumber: 9
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (MainMenu);

/***/ }),

/***/ "./components/master/navbar.js":
/*!*************************************!*\
  !*** ./components/master/navbar.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _material_ui_icons_PetsRounded__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-ui/icons/PetsRounded */ "@material-ui/icons/PetsRounded");
/* harmony import */ var _material_ui_icons_PetsRounded__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_material_ui_icons_PetsRounded__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next-auth/client */ "next-auth/client");
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_auth_client__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @material-ui/core/Link */ "@material-ui/core/Link");
/* harmony import */ var _material_ui_core_Link__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @material-ui/core */ "@material-ui/core");
/* harmony import */ var _material_ui_core__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_material_ui_core__WEBPACK_IMPORTED_MODULE_5__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\components\\master\\navbar.js";






function Navbar() {
  // let [isShowMenu, setIsShowMenu] = useState(false);
  const logoutHandle = e => {
    e.preventDefault();
    Object(next_auth_client__WEBPACK_IMPORTED_MODULE_3__["signOut"])();
  };

  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("nav", {
    class: "flex items-center justify-between flex-wrap bg-green-500 px-6 py-1",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_core_Link__WEBPACK_IMPORTED_MODULE_4___default.a, {
      href: "/",
      color: "inherit",
      underline: "none",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        class: "flex items-center flex-shrink-0 text-white mr-6",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_material_ui_icons_PetsRounded__WEBPACK_IMPORTED_MODULE_2___default.a, {
          className: "mr-2"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 21,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("span", {
          class: "font-semibold text-xl tracking-tight",
          children: "Crazy House"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 22,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 20,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 19,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      class: "block lg:hidden",
      children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("button", {
        class: "flex items-center px-3 py-2 border rounded text-green-200 border-teal-400 hover:text-white hover:border-white",
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("svg", {
          class: "fill-current h-3 w-3",
          viewBox: "0 0 20 20",
          xmlns: "http://www.w3.org/2000/svg",
          children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
            children: "Menu"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 32,
            columnNumber: 13
          }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("path", {
            d: "M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 27,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 26,
        columnNumber: 9
      }, this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 25,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 18,
    columnNumber: 5
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (Navbar);

/***/ }),

/***/ "./components/report/BarChartReporter.js":
/*!***********************************************!*\
  !*** ./components/report/BarChartReporter.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return BarChartReporter; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var recharts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! recharts */ "recharts");
/* harmony import */ var recharts__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(recharts__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\components\\report\\BarChartReporter.js";


function BarChartReporter({
  data,
  color,
  title
}) {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["ResponsiveContainer"], {
    width: 500,
    height: 400,
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["BarChart"], {
      width: 500,
      height: 300,
      data: data,
      margin: {
        top: 5,
        right: 30,
        left: 20,
        bottom: 5
      },
      barSize: 20,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["XAxis"], {
        dataKey: "name",
        scale: "point",
        padding: {
          left: 10,
          right: 10
        },
        label: true
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 19,
        columnNumber: 13
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["YAxis"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 20,
        columnNumber: 13
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["Tooltip"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 21,
        columnNumber: 13
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["Legend"], {
        content: title
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 13
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["CartesianGrid"], {
        strokeDasharray: "3 3"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 23,
        columnNumber: 13
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["Bar"], {
        dataKey: "value",
        fill: color,
        background: {
          fill: '#eee'
        }
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 24,
        columnNumber: 13
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 7,
      columnNumber: 11
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 6,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./components/report/LineChartReporter.js":
/*!************************************************!*\
  !*** ./components/report/LineChartReporter.js ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return LineChartReporter; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var recharts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! recharts */ "recharts");
/* harmony import */ var recharts__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(recharts__WEBPACK_IMPORTED_MODULE_1__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\components\\report\\LineChartReporter.js";

const data = [{
  name: 'T1',
  uv: 4000,
  pv: 2400,
  amt: 2400
}, {
  name: 'T2',
  uv: 3000,
  pv: 1398,
  amt: 2210
}, {
  name: 'T3',
  uv: 2000,
  pv: 9800,
  amt: 2290
}, {
  name: 'T4',
  uv: 2780,
  pv: 3908,
  amt: 2000
}, {
  name: 'T5',
  uv: 1890,
  pv: 4800,
  amt: 2181
}, {
  name: 'T6',
  uv: 2390,
  pv: 3800,
  amt: 2500
}, {
  name: 'T7',
  uv: 3490,
  pv: 4300,
  amt: 2100
}, {
  name: 'T8',
  uv: 1890,
  pv: 4800,
  amt: 2181
}, {
  name: 'T9',
  uv: 2390,
  pv: 3800,
  amt: 2500
}, {
  name: 'T10',
  uv: 3490,
  pv: 4300,
  amt: 2100
}, {
  name: 'T11',
  uv: 2390,
  pv: 3800,
  amt: 2500
}, {
  name: 'T12',
  uv: 3490,
  pv: 4300,
  amt: 2100
}];
function LineChartReporter({
  data
}) {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_1__["ResponsiveContainer"], {
    width: 500,
    height: 400,
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_1__["LineChart"], {
      width: 300,
      height: 300,
      data: data,
      margin: {
        top: 5,
        right: 30,
        left: 20,
        bottom: 5
      },
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_1__["CartesianGrid"], {
        strokeDasharray: "3 3"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 93,
        columnNumber: 13
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_1__["XAxis"], {
        dataKey: "total"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 94,
        columnNumber: 13
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_1__["YAxis"], {
        dataKey: 2021
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 95,
        columnNumber: 13
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_1__["Tooltip"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 96,
        columnNumber: 13
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_1__["Legend"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 97,
        columnNumber: 13
      }, this), data.map(room => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_1__["Line"], {
        type: "monotone",
        dataKey: 'room ' + room.roomId,
        stroke: "#82ca9d"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 98,
        columnNumber: 32
      }, this))]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 82,
      columnNumber: 13
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 81,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./components/report/PieChartReporter.js":
/*!***********************************************!*\
  !*** ./components/report/PieChartReporter.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return PieChartReporter; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var recharts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! recharts */ "recharts");
/* harmony import */ var recharts__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(recharts__WEBPACK_IMPORTED_MODULE_2__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\components\\report\\PieChartReporter.js";

 // const data = [
//   { name: 'Group A', value: 400 },
//   { name: 'Group B', value: 300 },
//   { name: 'Group C', value: 300 },
//   { name: 'Group D', value: 200 },
// ];

const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
const RADIAN = Math.PI / 180;

const renderCustomizedLabel = ({
  cx,
  cy,
  midAngle,
  innerRadius,
  outerRadius,
  percent,
  index
}) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("text", {
    x: x,
    y: y,
    fill: "white",
    textAnchor: x > cx ? 'start' : 'end',
    dominantBaseline: "central",
    children: `${(percent * 100).toFixed(0)}%`
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 20,
    columnNumber: 5
  }, undefined);
};

function PieChartReporter({
  data
}) {
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["ResponsiveContainer"], {
    width: 400,
    height: 400,
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["PieChart"], {
      width: 730,
      height: 400,
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["Pie"], {
        data: data,
        cx: "50%",
        cy: "50%",
        labelLine: false // label={renderCustomizedLabel}
        ,
        outerRadius: 80,
        fill: "#8884d8",
        dataKey: "value",
        nameKey: "name",
        label: true,
        children: data.map((entry, index) => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["Cell"], {
          fill: COLORS[index % COLORS.length]
        }, `cell-${index}`, false, {
          fileName: _jsxFileName,
          lineNumber: 43,
          columnNumber: 15
        }, this))
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["Tooltip"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 46,
        columnNumber: 11
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(recharts__WEBPACK_IMPORTED_MODULE_2__["Legend"], {
        verticalAlign: "top",
        height: 36
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 47,
        columnNumber: 11
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 29,
      columnNumber: 9
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 28,
    columnNumber: 9
  }, this);
}

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default, getServerSideProps */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Home; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getServerSideProps", function() { return getServerSideProps; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_master_navbar__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/master/navbar */ "./components/master/navbar.js");
/* harmony import */ var _components_master_breadcrumbs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/master/breadcrumbs */ "./components/master/breadcrumbs.js");
/* harmony import */ var _components_master_mainmenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/master/mainmenu */ "./components/master/mainmenu.js");
/* harmony import */ var _components_master_functionnav__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/master/functionnav */ "./components/master/functionnav.js");
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next-auth/client */ "next-auth/client");
/* harmony import */ var next_auth_client__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_auth_client__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _components_report_LineChartReporter__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../components/report/LineChartReporter */ "./components/report/LineChartReporter.js");
/* harmony import */ var _components_report_PieChartReporter__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/report/PieChartReporter */ "./components/report/PieChartReporter.js");
/* harmony import */ var _components_report_BarChartReporter__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/report/BarChartReporter */ "./components/report/BarChartReporter.js");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! swr */ "swr");
/* harmony import */ var swr__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(swr__WEBPACK_IMPORTED_MODULE_10__);

var _jsxFileName = "D:\\tmp\\assignment\\crazyhouse\\crazyweb\\pages\\index.js";




 // import jwt from 'next-auth/jwt'






function Home() {
  const session = Object(next_auth_client__WEBPACK_IMPORTED_MODULE_6__["useSession"])();
  const {
    data: revenueByYear,
    error: err
  } = swr__WEBPACK_IMPORTED_MODULE_10___default()(['http://localhost:8080/api/booking/report/year', session.accessToken]);
  const {
    data: revenueByMonth,
    error: errr
  } = swr__WEBPACK_IMPORTED_MODULE_10___default()(['http://localhost:8080/api/booking/report/month', session.accessToken]);
  const currDate = new Date();
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
    className: "",
    children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(next_head__WEBPACK_IMPORTED_MODULE_1___default.a, {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("title", {
        children: "Crazy Web"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("link", {
        rel: "icon",
        href: "/favicon.ico"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 23,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 21,
      columnNumber: 7
    }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("main", {
      className: "h-screen",
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_master_navbar__WEBPACK_IMPORTED_MODULE_2__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_master_breadcrumbs__WEBPACK_IMPORTED_MODULE_3__["default"], {
        className: "bg-gray-400"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 28,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_master_mainmenu__WEBPACK_IMPORTED_MODULE_4__["default"], {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 29,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        className: "mt-4 flex justify-center",
        children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_report_BarChartReporter__WEBPACK_IMPORTED_MODULE_9__["default"], {
          data: revenueByMonth,
          color: "#0088FE",
          title: () => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
            className: "text-center text-blue-500 font-bold text-lg",
            children: ["Doanh thu th\xE1ng ", currDate.getUTCMonth()]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 32,
            columnNumber: 80
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 11
        }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_report_BarChartReporter__WEBPACK_IMPORTED_MODULE_9__["default"], {
          data: revenueByYear,
          color: "#FF8042",
          title: () => /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("h4", {
            className: "text-center text-yellow-500 font-bold text-lg",
            children: ["Doanh thu n\u0103m ", currDate.getFullYear()]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 80
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 30,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 26,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 20,
    columnNumber: 5
  }, this);
}
async function getServerSideProps(context) {
  const _session = await Object(next_auth_client__WEBPACK_IMPORTED_MODULE_6__["getSession"])(context);

  if (!_session) {
    return {
      redirect: {
        destination: '/auth/signin',
        permanent: false
      }
    };
  }

  return {
    props: {
      session: _session
    }
  };
}

/***/ }),

/***/ "@material-ui/core":
/*!************************************!*\
  !*** external "@material-ui/core" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core");

/***/ }),

/***/ "@material-ui/core/Breadcrumbs":
/*!************************************************!*\
  !*** external "@material-ui/core/Breadcrumbs" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Breadcrumbs");

/***/ }),

/***/ "@material-ui/core/Card":
/*!*****************************************!*\
  !*** external "@material-ui/core/Card" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Card");

/***/ }),

/***/ "@material-ui/core/Link":
/*!*****************************************!*\
  !*** external "@material-ui/core/Link" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Link");

/***/ }),

/***/ "@material-ui/core/Paper":
/*!******************************************!*\
  !*** external "@material-ui/core/Paper" ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Paper");

/***/ }),

/***/ "@material-ui/core/Tab":
/*!****************************************!*\
  !*** external "@material-ui/core/Tab" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Tab");

/***/ }),

/***/ "@material-ui/core/Tabs":
/*!*****************************************!*\
  !*** external "@material-ui/core/Tabs" ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Tabs");

/***/ }),

/***/ "@material-ui/core/Typography":
/*!***********************************************!*\
  !*** external "@material-ui/core/Typography" ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/Typography");

/***/ }),

/***/ "@material-ui/core/styles":
/*!*******************************************!*\
  !*** external "@material-ui/core/styles" ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/core/styles");

/***/ }),

/***/ "@material-ui/icons/Assessment":
/*!************************************************!*\
  !*** external "@material-ui/icons/Assessment" ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Assessment");

/***/ }),

/***/ "@material-ui/icons/LocalOfferRounded":
/*!*******************************************************!*\
  !*** external "@material-ui/icons/LocalOfferRounded" ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/LocalOfferRounded");

/***/ }),

/***/ "@material-ui/icons/MeetingRoomRounded":
/*!********************************************************!*\
  !*** external "@material-ui/icons/MeetingRoomRounded" ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/MeetingRoomRounded");

/***/ }),

/***/ "@material-ui/icons/NavigateNext":
/*!**************************************************!*\
  !*** external "@material-ui/icons/NavigateNext" ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/NavigateNext");

/***/ }),

/***/ "@material-ui/icons/PeopleOutlineRounded":
/*!**********************************************************!*\
  !*** external "@material-ui/icons/PeopleOutlineRounded" ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/PeopleOutlineRounded");

/***/ }),

/***/ "@material-ui/icons/Person":
/*!********************************************!*\
  !*** external "@material-ui/icons/Person" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/Person");

/***/ }),

/***/ "@material-ui/icons/PetsRounded":
/*!*************************************************!*\
  !*** external "@material-ui/icons/PetsRounded" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/PetsRounded");

/***/ }),

/***/ "@material-ui/icons/RedeemRounded":
/*!***************************************************!*\
  !*** external "@material-ui/icons/RedeemRounded" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/RedeemRounded");

/***/ }),

/***/ "@material-ui/icons/RoomServiceRounded":
/*!********************************************************!*\
  !*** external "@material-ui/icons/RoomServiceRounded" ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/RoomServiceRounded");

/***/ }),

/***/ "@material-ui/icons/StarRounded":
/*!*************************************************!*\
  !*** external "@material-ui/icons/StarRounded" ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@material-ui/icons/StarRounded");

/***/ }),

/***/ "next-auth/client":
/*!***********************************!*\
  !*** external "next-auth/client" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next-auth/client");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react/jsx-dev-runtime");

/***/ }),

/***/ "recharts":
/*!***************************!*\
  !*** external "recharts" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("recharts");

/***/ }),

/***/ "swr":
/*!**********************!*\
  !*** external "swr" ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("swr");

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9tYXN0ZXIvYnJlYWRjcnVtYnMuanMiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9tYXN0ZXIvZnVuY3Rpb25uYXYuanMiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9tYXN0ZXIvbWFpbm1lbnUuanMiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9tYXN0ZXIvbmF2YmFyLmpzIiwid2VicGFjazovLy8uL2NvbXBvbmVudHMvcmVwb3J0L0JhckNoYXJ0UmVwb3J0ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9yZXBvcnQvTGluZUNoYXJ0UmVwb3J0ZXIuanMiLCJ3ZWJwYWNrOi8vLy4vY29tcG9uZW50cy9yZXBvcnQvUGllQ2hhcnRSZXBvcnRlci5qcyIsIndlYnBhY2s6Ly8vLi9wYWdlcy9pbmRleC5qcyIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9jb3JlL0JyZWFkY3J1bWJzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2NvcmUvQ2FyZFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9jb3JlL0xpbmtcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZS9QYXBlclwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9jb3JlL1RhYlwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9jb3JlL1RhYnNcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvY29yZS9UeXBvZ3JhcGh5XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2NvcmUvc3R5bGVzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2ljb25zL0Fzc2Vzc21lbnRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvTG9jYWxPZmZlclJvdW5kZWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvTWVldGluZ1Jvb21Sb3VuZGVkXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwiQG1hdGVyaWFsLXVpL2ljb25zL05hdmlnYXRlTmV4dFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9pY29ucy9QZW9wbGVPdXRsaW5lUm91bmRlZFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9pY29ucy9QZXJzb25cIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvUGV0c1JvdW5kZWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvUmVkZWVtUm91bmRlZFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcIkBtYXRlcmlhbC11aS9pY29ucy9Sb29tU2VydmljZVJvdW5kZWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJAbWF0ZXJpYWwtdWkvaWNvbnMvU3RhclJvdW5kZWRcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJuZXh0LWF1dGgvY2xpZW50XCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9oZWFkXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwibmV4dC9yb3V0ZXJcIiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIiwid2VicGFjazovLy9leHRlcm5hbCBcInJlY2hhcnRzXCIiLCJ3ZWJwYWNrOi8vL2V4dGVybmFsIFwic3dyXCIiXSwibmFtZXMiOlsiQnJlYWRDcnVtYiIsInVzZVN0eWxlcyIsIm1ha2VTdHlsZXMiLCJyb290IiwiYmFja2dyb3VuZENvbG9yIiwiZGlzcGxheSIsImp1c3RpZnlDb250ZW50IiwiRnVuY3Rpb25OYXYiLCJjaGlsZHJlbiIsImNsYXNzZXMiLCJNYWluTWVudSIsInNlc3Npb24iLCJ1c2VTZXNzaW9uIiwidmFsdWUiLCJzZXRWYWx1ZSIsIlJlYWN0IiwidXNlU3RhdGUiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJ0YWIiLCJxdWVyeSIsInBhcnNlSW50IiwidXNlciIsImZ1bGxOYW1lIiwiTmF2YmFyIiwibG9nb3V0SGFuZGxlIiwiZSIsInByZXZlbnREZWZhdWx0Iiwic2lnbk91dCIsIkJhckNoYXJ0UmVwb3J0ZXIiLCJkYXRhIiwiY29sb3IiLCJ0aXRsZSIsInRvcCIsInJpZ2h0IiwibGVmdCIsImJvdHRvbSIsImZpbGwiLCJuYW1lIiwidXYiLCJwdiIsImFtdCIsIkxpbmVDaGFydFJlcG9ydGVyIiwibWFwIiwicm9vbSIsInJvb21JZCIsIkNPTE9SUyIsIlJBRElBTiIsIk1hdGgiLCJQSSIsInJlbmRlckN1c3RvbWl6ZWRMYWJlbCIsImN4IiwiY3kiLCJtaWRBbmdsZSIsImlubmVyUmFkaXVzIiwib3V0ZXJSYWRpdXMiLCJwZXJjZW50IiwiaW5kZXgiLCJyYWRpdXMiLCJ4IiwiY29zIiwieSIsInNpbiIsInRvRml4ZWQiLCJQaWVDaGFydFJlcG9ydGVyIiwiZW50cnkiLCJsZW5ndGgiLCJIb21lIiwicmV2ZW51ZUJ5WWVhciIsImVycm9yIiwiZXJyIiwidXNlU1dSIiwiYWNjZXNzVG9rZW4iLCJyZXZlbnVlQnlNb250aCIsImVycnIiLCJjdXJyRGF0ZSIsIkRhdGUiLCJnZXRVVENNb250aCIsImdldEZ1bGxZZWFyIiwiZ2V0U2VydmVyU2lkZVByb3BzIiwiY29udGV4dCIsIl9zZXNzaW9uIiwiZ2V0U2Vzc2lvbiIsInJlZGlyZWN0IiwiZGVzdGluYXRpb24iLCJwZXJtYW5lbnQiLCJwcm9wcyJdLCJtYXBwaW5ncyI6Ijs7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLElBQUk7UUFDSjtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4RkE7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsU0FBU0EsVUFBVCxHQUFxQjtBQUNqQixzQkFDSTtBQUFLLGFBQVMsRUFBQyxXQUFmO0FBQUEsMkJBQ0kscUVBQUMsb0VBQUQ7QUFBYSxlQUFTLEVBQUMsUUFBdkI7QUFBMkIsb0JBQVcsWUFBdEM7QUFBQSw4QkFDSSxxRUFBQyw2REFBRDtBQUFNLGFBQUssRUFBQyxTQUFaO0FBQXNCLFlBQUksRUFBQyxHQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURKLGVBSUkscUVBQUMsbUVBQUQ7QUFBWSxhQUFLLEVBQUMsYUFBbEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FKSjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFESjtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREo7QUFVSDs7QUFFY0EseUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDbEJBO0FBQ0E7QUFHQSxNQUFNQyxTQUFTLEdBQUdDLDJFQUFVLENBQUM7QUFDekJDLE1BQUksRUFBRTtBQUNKQyxtQkFBZSxFQUFFLGFBRGI7QUFFSkMsV0FBTyxFQUFFLE1BRkw7QUFHSkMsa0JBQWMsRUFBRTtBQUhaO0FBRG1CLENBQUQsQ0FBNUI7O0FBUUEsU0FBU0MsV0FBVCxDQUFxQjtBQUFDQztBQUFELENBQXJCLEVBQWlDO0FBRTdCLFFBQU1DLE9BQU8sR0FBR1IsU0FBUyxFQUF6QjtBQUdBLHNCQUNJO0FBQUssYUFBUyxFQUFDLDJDQUFmO0FBQUEsY0FDS087QUFETDtBQUFBO0FBQUE7QUFBQTtBQUFBLFVBREo7QUFLSDs7QUFFY0QsMEVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN4QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFNBQVNHLFFBQVQsR0FBb0I7QUFBQTs7QUFDaEIsUUFBTSxDQUFFQyxPQUFGLElBQWFDLG9FQUFVLEVBQTdCO0FBQ0EsUUFBTSxDQUFDQyxLQUFELEVBQVFDLFFBQVIsSUFBb0JDLDRDQUFLLENBQUNDLFFBQU4sRUFBMUI7QUFDQSxRQUFNQyxNQUFNLEdBQUdDLDhEQUFTLEVBQXhCO0FBQ0EsUUFBTTtBQUFDQztBQUFELE1BQVFGLE1BQU0sQ0FBQ0csS0FBckI7QUFFQSxzQkFDSSxxRUFBQyw4REFBRDtBQUFNLFdBQU8sRUFBQyxVQUFkO0FBQUEsMkJBQ0EscUVBQUMsOERBQUQ7QUFBTyxZQUFNLE1BQWI7QUFBQSw2QkFDRSxxRUFBQyw2REFBRDtBQUNFLGFBQUssRUFBRUMsUUFBUSxDQUFDRixHQUFHLEdBQUdBLEdBQUgsR0FBUyxHQUFiLENBRGpCO0FBRUUsaUJBQVMsRUFBQyxTQUZaO0FBR0Usc0JBQWMsRUFBQyxTQUhqQjtBQUlFLHNCQUFXLHVCQUpiO0FBQUEsZ0NBTUU7QUFBRyxjQUFJLEVBQUMsVUFBUjtBQUFtQixtQkFBUyxFQUFDLHVCQUE3QjtBQUFBLGlDQUFxRCxxRUFBQyw0REFBRDtBQUFLLGdCQUFJLGVBQUUscUVBQUMsaUVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFBWDtBQUEyQixpQkFBSyxFQUFFLGVBQWNSLE9BQWQsYUFBY0EsT0FBZCx3Q0FBY0EsT0FBTyxDQUFFVyxJQUF2QixrREFBYyxjQUFlQyxRQUE3QixDQUFsQztBQUF5RSxxQkFBUyxFQUFDO0FBQW5GO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBckQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFORixlQU9FO0FBQUcsY0FBSSxFQUFDLGdCQUFSO0FBQXlCLG1CQUFTLEVBQUMsdUJBQW5DO0FBQUEsaUNBQTJELHFFQUFDLDREQUFEO0FBQUssaUJBQUssRUFBQyxTQUFYO0FBQXFCLGdCQUFJLGVBQUUscUVBQUMsOEVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTNEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBUEYsZUFRRTtBQUFHLGNBQUksRUFBQyxnQkFBUjtBQUF5QixtQkFBUyxFQUFDLHVCQUFuQztBQUFBLGlDQUEyRCxxRUFBQyw0REFBRDtBQUFLLGlCQUFLLEVBQUMsU0FBWDtBQUFxQixnQkFBSSxlQUFFLHFFQUFDLDRFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBM0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUEzRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVJGLGVBU0U7QUFBRyxjQUFJLEVBQUMsYUFBUjtBQUFzQixtQkFBUyxFQUFDLHVCQUFoQztBQUFBLGlDQUF3RCxxRUFBQyw0REFBRDtBQUFLLGlCQUFLLEVBQUMsTUFBWDtBQUFrQixnQkFBSSxlQUFFLHFFQUFDLDRFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBeEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF4RDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVRGLGVBVUU7QUFBRyxjQUFJLEVBQUMsY0FBUjtBQUF1QixtQkFBUyxFQUFDLHVCQUFqQztBQUFBLGlDQUF5RCxxRUFBQyw0REFBRDtBQUFLLGlCQUFLLEVBQUMsT0FBWDtBQUFtQixnQkFBSSxlQUFFLHFFQUFDLDRFQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBekI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF6RDtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQVZGLGVBV0U7QUFBRyxjQUFJLEVBQUMsa0JBQVI7QUFBMkIsbUJBQVMsRUFBQyx1QkFBckM7QUFBQSxpQ0FBNkQscUVBQUMsNERBQUQ7QUFBSyxpQkFBSyxFQUFDLFdBQVg7QUFBdUIsZ0JBQUksZUFBRSxxRUFBQyx1RUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTdCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBN0Q7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFYRixlQVlFO0FBQUcsY0FBSSxFQUFDLGNBQVI7QUFBdUIsbUJBQVMsRUFBQyx1QkFBakM7QUFBQSxpQ0FBeUQscUVBQUMsNERBQUQ7QUFBSyxpQkFBSyxFQUFDLE9BQVg7QUFBbUIsZ0JBQUksZUFBRSxxRUFBQyxxRUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQXpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBekQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFaRixlQWFFO0FBQUcsY0FBSSxFQUFDLGVBQVI7QUFBd0IsbUJBQVMsRUFBQyx1QkFBbEM7QUFBQSxpQ0FBMEQscUVBQUMsNERBQUQ7QUFBSyxpQkFBSyxFQUFDLFFBQVg7QUFBb0IsZ0JBQUksZUFBRSxxRUFBQyxxRUFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTFCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBMUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFiRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURKO0FBc0JIOztBQUVjYix1RUFBZixFOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUM5Q0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFJQSxTQUFTYyxNQUFULEdBQWtCO0FBQ2hCO0FBRUEsUUFBTUMsWUFBWSxHQUFJQyxDQUFELElBQU87QUFDMUJBLEtBQUMsQ0FBQ0MsY0FBRjtBQUNBQyxvRUFBTztBQUNSLEdBSEQ7O0FBS0Esc0JBQ0U7QUFBSyxTQUFLLEVBQUMsb0VBQVg7QUFBQSw0QkFDRSxxRUFBQyw2REFBRDtBQUFNLFVBQUksRUFBQyxHQUFYO0FBQWUsV0FBSyxFQUFDLFNBQXJCO0FBQStCLGVBQVMsRUFBQyxNQUF6QztBQUFBLDZCQUNFO0FBQUssYUFBSyxFQUFDLGlEQUFYO0FBQUEsZ0NBQ0UscUVBQUMscUVBQUQ7QUFBaUIsbUJBQVMsRUFBQztBQUEzQjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBRUU7QUFBTSxlQUFLLEVBQUMsc0NBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQURGLGVBT0U7QUFBSyxXQUFLLEVBQUMsaUJBQVg7QUFBQSw2QkFDRTtBQUFRLGFBQUssRUFBQywrR0FBZDtBQUFBLCtCQUNFO0FBQ0UsZUFBSyxFQUFDLHNCQURSO0FBRUUsaUJBQU8sRUFBQyxXQUZWO0FBR0UsZUFBSyxFQUFDLDRCQUhSO0FBQUEsa0NBS0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBTEYsZUFNRTtBQUFNLGFBQUMsRUFBQztBQUFSO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBTkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFQRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQTRHRDs7QUFFY0oscUVBQWYsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzlIQTtBQUNBO0FBRWUsU0FBU0ssZ0JBQVQsQ0FBMEI7QUFBQ0MsTUFBRDtBQUFPQyxPQUFQO0FBQWNDO0FBQWQsQ0FBMUIsRUFBK0M7QUFDMUQsc0JBQ0kscUVBQUMsNERBQUQ7QUFBcUIsU0FBSyxFQUFFLEdBQTVCO0FBQWlDLFVBQU0sRUFBRSxHQUF6QztBQUFBLDJCQUNFLHFFQUFDLGlEQUFEO0FBQ0UsV0FBSyxFQUFFLEdBRFQ7QUFFRSxZQUFNLEVBQUUsR0FGVjtBQUdFLFVBQUksRUFBRUYsSUFIUjtBQUlFLFlBQU0sRUFBRTtBQUNORyxXQUFHLEVBQUUsQ0FEQztBQUVOQyxhQUFLLEVBQUUsRUFGRDtBQUdOQyxZQUFJLEVBQUUsRUFIQTtBQUlOQyxjQUFNLEVBQUU7QUFKRixPQUpWO0FBVUUsYUFBTyxFQUFFLEVBVlg7QUFBQSw4QkFZRSxxRUFBQyw4Q0FBRDtBQUFPLGVBQU8sRUFBQyxNQUFmO0FBQXNCLGFBQUssRUFBQyxPQUE1QjtBQUFvQyxlQUFPLEVBQUU7QUFBRUQsY0FBSSxFQUFFLEVBQVI7QUFBWUQsZUFBSyxFQUFFO0FBQW5CLFNBQTdDO0FBQXNFLGFBQUs7QUFBM0U7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVpGLGVBYUUscUVBQUMsOENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWJGLGVBY0UscUVBQUMsZ0RBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWRGLGVBZUUscUVBQUMsK0NBQUQ7QUFBUSxlQUFPLEVBQUVGO0FBQWpCO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FmRixlQWdCRSxxRUFBQyxzREFBRDtBQUFlLHVCQUFlLEVBQUM7QUFBL0I7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWhCRixlQWlCRSxxRUFBQyw0Q0FBRDtBQUFLLGVBQU8sRUFBQyxPQUFiO0FBQXFCLFlBQUksRUFBRUQsS0FBM0I7QUFBa0Msa0JBQVUsRUFBRTtBQUFFTSxjQUFJLEVBQUU7QUFBUjtBQUE5QztBQUFBO0FBQUE7QUFBQTtBQUFBLGNBakJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQXVCSCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNCRDtBQUVBLE1BQU1QLElBQUksR0FBRyxDQUNUO0FBQ0VRLE1BQUksRUFBRSxJQURSO0FBRUVDLElBQUUsRUFBRSxJQUZOO0FBR0VDLElBQUUsRUFBRSxJQUhOO0FBSUVDLEtBQUcsRUFBRTtBQUpQLENBRFMsRUFPVDtBQUNFSCxNQUFJLEVBQUUsSUFEUjtBQUVFQyxJQUFFLEVBQUUsSUFGTjtBQUdFQyxJQUFFLEVBQUUsSUFITjtBQUlFQyxLQUFHLEVBQUU7QUFKUCxDQVBTLEVBYVQ7QUFDRUgsTUFBSSxFQUFFLElBRFI7QUFFRUMsSUFBRSxFQUFFLElBRk47QUFHRUMsSUFBRSxFQUFFLElBSE47QUFJRUMsS0FBRyxFQUFFO0FBSlAsQ0FiUyxFQW1CVDtBQUNFSCxNQUFJLEVBQUUsSUFEUjtBQUVFQyxJQUFFLEVBQUUsSUFGTjtBQUdFQyxJQUFFLEVBQUUsSUFITjtBQUlFQyxLQUFHLEVBQUU7QUFKUCxDQW5CUyxFQXlCVDtBQUNFSCxNQUFJLEVBQUUsSUFEUjtBQUVFQyxJQUFFLEVBQUUsSUFGTjtBQUdFQyxJQUFFLEVBQUUsSUFITjtBQUlFQyxLQUFHLEVBQUU7QUFKUCxDQXpCUyxFQStCVDtBQUNFSCxNQUFJLEVBQUUsSUFEUjtBQUVFQyxJQUFFLEVBQUUsSUFGTjtBQUdFQyxJQUFFLEVBQUUsSUFITjtBQUlFQyxLQUFHLEVBQUU7QUFKUCxDQS9CUyxFQXFDVDtBQUNFSCxNQUFJLEVBQUUsSUFEUjtBQUVFQyxJQUFFLEVBQUUsSUFGTjtBQUdFQyxJQUFFLEVBQUUsSUFITjtBQUlFQyxLQUFHLEVBQUU7QUFKUCxDQXJDUyxFQTJDVDtBQUNFSCxNQUFJLEVBQUUsSUFEUjtBQUVFQyxJQUFFLEVBQUUsSUFGTjtBQUdFQyxJQUFFLEVBQUUsSUFITjtBQUlFQyxLQUFHLEVBQUU7QUFKUCxDQTNDUyxFQWlEVDtBQUNFSCxNQUFJLEVBQUUsSUFEUjtBQUVFQyxJQUFFLEVBQUUsSUFGTjtBQUdFQyxJQUFFLEVBQUUsSUFITjtBQUlFQyxLQUFHLEVBQUU7QUFKUCxDQWpEUyxFQXVEVDtBQUNFSCxNQUFJLEVBQUUsS0FEUjtBQUVFQyxJQUFFLEVBQUUsSUFGTjtBQUdFQyxJQUFFLEVBQUUsSUFITjtBQUlFQyxLQUFHLEVBQUU7QUFKUCxDQXZEUyxFQTZEVDtBQUNJSCxNQUFJLEVBQUUsS0FEVjtBQUVJQyxJQUFFLEVBQUUsSUFGUjtBQUdJQyxJQUFFLEVBQUUsSUFIUjtBQUlJQyxLQUFHLEVBQUU7QUFKVCxDQTdEUyxFQW1FUDtBQUNFSCxNQUFJLEVBQUUsS0FEUjtBQUVFQyxJQUFFLEVBQUUsSUFGTjtBQUdFQyxJQUFFLEVBQUUsSUFITjtBQUlFQyxLQUFHLEVBQUU7QUFKUCxDQW5FTyxDQUFiO0FBMkVlLFNBQVNDLGlCQUFULENBQTJCO0FBQUNaO0FBQUQsQ0FBM0IsRUFBa0M7QUFFN0Msc0JBQ0kscUVBQUMsNERBQUQ7QUFBcUIsU0FBSyxFQUFFLEdBQTVCO0FBQWlDLFVBQU0sRUFBRSxHQUF6QztBQUFBLDJCQUNJLHFFQUFDLGtEQUFEO0FBQ0EsV0FBSyxFQUFFLEdBRFA7QUFFQSxZQUFNLEVBQUUsR0FGUjtBQUdBLFVBQUksRUFBRUEsSUFITjtBQUlBLFlBQU0sRUFBRTtBQUNKRyxXQUFHLEVBQUUsQ0FERDtBQUVKQyxhQUFLLEVBQUUsRUFGSDtBQUdKQyxZQUFJLEVBQUUsRUFIRjtBQUlKQyxjQUFNLEVBQUU7QUFKSixPQUpSO0FBQUEsOEJBV0EscUVBQUMsc0RBQUQ7QUFBZSx1QkFBZSxFQUFDO0FBQS9CO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FYQSxlQVlBLHFFQUFDLDhDQUFEO0FBQU8sZUFBTyxFQUFDO0FBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQVpBLGVBYUEscUVBQUMsOENBQUQ7QUFBTyxlQUFPLEVBQUc7QUFBakI7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWJBLGVBY0EscUVBQUMsZ0RBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWRBLGVBZUEscUVBQUMsK0NBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWZBLEVBZ0JDTixJQUFJLENBQUNhLEdBQUwsQ0FBVUMsSUFBRCxpQkFBUyxxRUFBQyw2Q0FBRDtBQUFNLFlBQUksRUFBQyxVQUFYO0FBQXNCLGVBQU8sRUFBRSxVQUFVQSxJQUFJLENBQUNDLE1BQTlDO0FBQXNELGNBQU0sRUFBQztBQUE3RDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBQWxCLENBaEJEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURKO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFESjtBQXdCSCxDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDdkdEO0NBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLE1BQU1DLE1BQU0sR0FBRyxDQUFDLFNBQUQsRUFBWSxTQUFaLEVBQXVCLFNBQXZCLEVBQWtDLFNBQWxDLENBQWY7QUFFQSxNQUFNQyxNQUFNLEdBQUdDLElBQUksQ0FBQ0MsRUFBTCxHQUFVLEdBQXpCOztBQUNBLE1BQU1DLHFCQUFxQixHQUFHLENBQUM7QUFBRUMsSUFBRjtBQUFNQyxJQUFOO0FBQVVDLFVBQVY7QUFBb0JDLGFBQXBCO0FBQWlDQyxhQUFqQztBQUE4Q0MsU0FBOUM7QUFBdURDO0FBQXZELENBQUQsS0FBb0U7QUFDaEcsUUFBTUMsTUFBTSxHQUFHSixXQUFXLEdBQUcsQ0FBQ0MsV0FBVyxHQUFHRCxXQUFmLElBQThCLEdBQTNEO0FBQ0EsUUFBTUssQ0FBQyxHQUFHUixFQUFFLEdBQUdPLE1BQU0sR0FBR1YsSUFBSSxDQUFDWSxHQUFMLENBQVMsQ0FBQ1AsUUFBRCxHQUFZTixNQUFyQixDQUF4QjtBQUNBLFFBQU1jLENBQUMsR0FBR1QsRUFBRSxHQUFHTSxNQUFNLEdBQUdWLElBQUksQ0FBQ2MsR0FBTCxDQUFTLENBQUNULFFBQUQsR0FBWU4sTUFBckIsQ0FBeEI7QUFFQSxzQkFDRTtBQUFNLEtBQUMsRUFBRVksQ0FBVDtBQUFZLEtBQUMsRUFBRUUsQ0FBZjtBQUFrQixRQUFJLEVBQUMsT0FBdkI7QUFBK0IsY0FBVSxFQUFFRixDQUFDLEdBQUdSLEVBQUosR0FBUyxPQUFULEdBQW1CLEtBQTlEO0FBQXFFLG9CQUFnQixFQUFDLFNBQXRGO0FBQUEsY0FDSSxHQUFFLENBQUNLLE9BQU8sR0FBRyxHQUFYLEVBQWdCTyxPQUFoQixDQUF3QixDQUF4QixDQUEyQjtBQURqQztBQUFBO0FBQUE7QUFBQTtBQUFBLGVBREY7QUFLRCxDQVZEOztBQVdlLFNBQVNDLGdCQUFULENBQTBCO0FBQUNsQztBQUFELENBQTFCLEVBQWlDO0FBQzVDLHNCQUVJLHFFQUFDLDREQUFEO0FBQXFCLFNBQUssRUFBRSxHQUE1QjtBQUFpQyxVQUFNLEVBQUUsR0FBekM7QUFBQSwyQkFDQSxxRUFBQyxpREFBRDtBQUFVLFdBQUssRUFBRSxHQUFqQjtBQUFzQixZQUFNLEVBQUUsR0FBOUI7QUFBQSw4QkFDRSxxRUFBQyw0Q0FBRDtBQUNFLFlBQUksRUFBRUEsSUFEUjtBQUVFLFVBQUUsRUFBQyxLQUZMO0FBR0UsVUFBRSxFQUFDLEtBSEw7QUFJRSxpQkFBUyxFQUFFLEtBSmIsQ0FLRTtBQUxGO0FBTUUsbUJBQVcsRUFBRSxFQU5mO0FBT0UsWUFBSSxFQUFDLFNBUFA7QUFRRSxlQUFPLEVBQUMsT0FSVjtBQVNFLGVBQU8sRUFBQyxNQVRWO0FBVUUsYUFBSyxNQVZQO0FBQUEsa0JBWUdBLElBQUksQ0FBQ2EsR0FBTCxDQUFTLENBQUNzQixLQUFELEVBQVFSLEtBQVIsa0JBQ1IscUVBQUMsNkNBQUQ7QUFBNEIsY0FBSSxFQUFFWCxNQUFNLENBQUNXLEtBQUssR0FBR1gsTUFBTSxDQUFDb0IsTUFBaEI7QUFBeEMsV0FBWSxRQUFPVCxLQUFNLEVBQXpCO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBREQ7QUFaSDtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFpQkUscUVBQUMsZ0RBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQWpCRixlQWtCRSxxRUFBQywrQ0FBRDtBQUFRLHFCQUFhLEVBQUMsS0FBdEI7QUFBNEIsY0FBTSxFQUFFO0FBQXBDO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FsQkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQUZKO0FBMkJILEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDcEREO0FBQ0E7QUFDQTtBQUNBO0NBRUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdlLFNBQVNVLElBQVQsR0FBZ0I7QUFDN0IsUUFBTXhELE9BQU8sR0FBR0MsbUVBQVUsRUFBMUI7QUFDQSxRQUFNO0FBQUVrQixRQUFJLEVBQUVzQyxhQUFSO0FBQXNCQyxTQUFLLEVBQUVDO0FBQTdCLE1BQXFDQywyQ0FBTSxDQUFDLENBQUMsK0NBQUQsRUFBa0Q1RCxPQUFPLENBQUM2RCxXQUExRCxDQUFELENBQWpEO0FBQ0EsUUFBTTtBQUFFMUMsUUFBSSxFQUFFMkMsY0FBUjtBQUF1QkosU0FBSyxFQUFFSztBQUE5QixNQUF1Q0gsMkNBQU0sQ0FBQyxDQUFDLGdEQUFELEVBQW1ENUQsT0FBTyxDQUFDNkQsV0FBM0QsQ0FBRCxDQUFuRDtBQUNBLFFBQU1HLFFBQVEsR0FBRyxJQUFJQyxJQUFKLEVBQWpCO0FBQ0Esc0JBQ0U7QUFBSyxhQUFTLEVBQUMsRUFBZjtBQUFBLDRCQUNFLHFFQUFDLGdEQUFEO0FBQUEsOEJBQ0U7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FERixlQUVFO0FBQU0sV0FBRyxFQUFDLE1BQVY7QUFBaUIsWUFBSSxFQUFDO0FBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFERixlQU1FO0FBQU0sZUFBUyxFQUFDLFVBQWhCO0FBQUEsOEJBQ0UscUVBQUMsaUVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQURGLGVBRUUscUVBQUMsc0VBQUQ7QUFBWSxpQkFBUyxFQUFDO0FBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FGRixlQUdFLHFFQUFDLG1FQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FIRixlQUlFO0FBQUssaUJBQVMsRUFBQywwQkFBZjtBQUFBLGdDQUVFLHFFQUFDLDJFQUFEO0FBQWtCLGNBQUksRUFBRUgsY0FBeEI7QUFBd0MsZUFBSyxFQUFDLFNBQTlDO0FBQXdELGVBQUssRUFBRSxtQkFBTTtBQUFJLHFCQUFTLEVBQUMsNkNBQWQ7QUFBQSw4Q0FBNkVFLFFBQVEsQ0FBQ0UsV0FBVCxFQUE3RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBckU7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFGRixlQUdFLHFFQUFDLDJFQUFEO0FBQW1CLGNBQUksRUFBRVQsYUFBekI7QUFBd0MsZUFBSyxFQUFDLFNBQTlDO0FBQXdELGVBQUssRUFBRSxtQkFBTTtBQUFJLHFCQUFTLEVBQUMsK0NBQWQ7QUFBQSw4Q0FBNkVPLFFBQVEsQ0FBQ0csV0FBVCxFQUE3RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBckU7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FKRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUFORjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsVUFERjtBQXVCRDtBQUVNLGVBQWVDLGtCQUFmLENBQWtDQyxPQUFsQyxFQUEyQztBQUNoRCxRQUFNQyxRQUFRLEdBQUcsTUFBTUMsbUVBQVUsQ0FBQ0YsT0FBRCxDQUFqQzs7QUFDQSxNQUFJLENBQUNDLFFBQUwsRUFBZTtBQUNiLFdBQU87QUFDTEUsY0FBUSxFQUFFO0FBQ1JDLG1CQUFXLEVBQUUsY0FETDtBQUVSQyxpQkFBUyxFQUFFO0FBRkg7QUFETCxLQUFQO0FBTUQ7O0FBQ0QsU0FBTztBQUNMQyxTQUFLLEVBQUU7QUFDTDNFLGFBQU8sRUFBRXNFO0FBREo7QUFERixHQUFQO0FBS0QsQzs7Ozs7Ozs7Ozs7QUMxREQsOEM7Ozs7Ozs7Ozs7O0FDQUEsMEQ7Ozs7Ozs7Ozs7O0FDQUEsbUQ7Ozs7Ozs7Ozs7O0FDQUEsbUQ7Ozs7Ozs7Ozs7O0FDQUEsb0Q7Ozs7Ozs7Ozs7O0FDQUEsa0Q7Ozs7Ozs7Ozs7O0FDQUEsbUQ7Ozs7Ozs7Ozs7O0FDQUEseUQ7Ozs7Ozs7Ozs7O0FDQUEscUQ7Ozs7Ozs7Ozs7O0FDQUEsMEQ7Ozs7Ozs7Ozs7O0FDQUEsaUU7Ozs7Ozs7Ozs7O0FDQUEsa0U7Ozs7Ozs7Ozs7O0FDQUEsNEQ7Ozs7Ozs7Ozs7O0FDQUEsb0U7Ozs7Ozs7Ozs7O0FDQUEsc0Q7Ozs7Ozs7Ozs7O0FDQUEsMkQ7Ozs7Ozs7Ozs7O0FDQUEsNkQ7Ozs7Ozs7Ozs7O0FDQUEsa0U7Ozs7Ozs7Ozs7O0FDQUEsMkQ7Ozs7Ozs7Ozs7O0FDQUEsNkM7Ozs7Ozs7Ozs7O0FDQUEsc0M7Ozs7Ozs7Ozs7O0FDQUEsd0M7Ozs7Ozs7Ozs7O0FDQUEsa0M7Ozs7Ozs7Ozs7O0FDQUEsa0Q7Ozs7Ozs7Ozs7O0FDQUEscUM7Ozs7Ozs7Ozs7O0FDQUEsZ0MiLCJmaWxlIjoicGFnZXMvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHJlcXVpcmUoJy4uL3Nzci1tb2R1bGUtY2FjaGUuanMnKTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0dmFyIHRocmV3ID0gdHJ1ZTtcbiBcdFx0dHJ5IHtcbiBcdFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcbiBcdFx0XHR0aHJldyA9IGZhbHNlO1xuIFx0XHR9IGZpbmFsbHkge1xuIFx0XHRcdGlmKHRocmV3KSBkZWxldGUgaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdH1cblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3BhZ2VzL2luZGV4LmpzXCIpO1xuIiwiaW1wb3J0IEJyZWFkY3J1bWJzIGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlL0JyZWFkY3J1bWJzJztcclxuaW1wb3J0IFR5cG9ncmFwaHkgZnJvbSAnQG1hdGVyaWFsLXVpL2NvcmUvVHlwb2dyYXBoeSc7XHJcbmltcG9ydCBMaW5rIGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlL0xpbmsnO1xyXG5pbXBvcnQgTmF2aWdhdGVOZXh0SWNvbiBmcm9tICdAbWF0ZXJpYWwtdWkvaWNvbnMvTmF2aWdhdGVOZXh0J1xyXG5cclxuZnVuY3Rpb24gQnJlYWRDcnVtYigpe1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInB5LTIgbWwtNFwiPlxyXG4gICAgICAgICAgICA8QnJlYWRjcnVtYnMgc2VwYXJhdG9yPVwi4oC6XCIgYXJpYS1sYWJlbD1cImJyZWFkY3J1bWJcIj5cclxuICAgICAgICAgICAgICAgIDxMaW5rIGNvbG9yPVwiaW5oZXJpdFwiIGhyZWY9XCIvXCI+XHJcbiAgICAgICAgICAgICAgICBIb21lXHJcbiAgICAgICAgICAgICAgICA8L0xpbms+XHJcbiAgICAgICAgICAgICAgICA8VHlwb2dyYXBoeSBjb2xvcj1cInRleHRQcmltYXJ5XCI+QWNjb3VudDwvVHlwb2dyYXBoeT5cclxuICAgICAgICAgICAgPC9CcmVhZGNydW1icz5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIClcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgQnJlYWRDcnVtYiIsImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IG1ha2VTdHlsZXMgfSBmcm9tICdAbWF0ZXJpYWwtdWkvY29yZS9zdHlsZXMnO1xyXG5cclxuXHJcbmNvbnN0IHVzZVN0eWxlcyA9IG1ha2VTdHlsZXMoe1xyXG4gICAgcm9vdDoge1xyXG4gICAgICBiYWNrZ3JvdW5kQ29sb3I6IFwidHJhbnNwYXJlbnRcIixcclxuICAgICAgZGlzcGxheTogJ2ZsZXgnLFxyXG4gICAgICBqdXN0aWZ5Q29udGVudDogJ2NlbnRlcidcclxuICAgIH0sXHJcbiAgfSk7XHJcblxyXG5mdW5jdGlvbiBGdW5jdGlvbk5hdih7Y2hpbGRyZW59KSB7XHJcblxyXG4gICAgY29uc3QgY2xhc3NlcyA9IHVzZVN0eWxlcygpO1xyXG4gICAgXHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInN0aWNreSBib3R0b20tMCBsZWZ0LTEgYmctZ3JheS0xMDAgYm9yZGVyXCI+XHJcbiAgICAgICAgICAgIHtjaGlsZHJlbn1cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgKTtcclxufVxyXG5cclxuZXhwb3J0IGRlZmF1bHQgRnVuY3Rpb25OYXYiLCJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5pbXBvcnQgUGFwZXIgZnJvbSAnQG1hdGVyaWFsLXVpL2NvcmUvUGFwZXInO1xyXG5pbXBvcnQgVGFicyBmcm9tICdAbWF0ZXJpYWwtdWkvY29yZS9UYWJzJztcclxuaW1wb3J0IFRhYiBmcm9tICdAbWF0ZXJpYWwtdWkvY29yZS9UYWInO1xyXG5pbXBvcnQgUGVvcGxlT3V0bGluZVJvdW5kZWRJY29uIGZyb20gJ0BtYXRlcmlhbC11aS9pY29ucy9QZW9wbGVPdXRsaW5lUm91bmRlZCc7XHJcbmltcG9ydCBSb29tU2VydmljZVJvdW5kZWRJY29uIGZyb20gJ0BtYXRlcmlhbC11aS9pY29ucy9Sb29tU2VydmljZVJvdW5kZWQnO1xyXG5pbXBvcnQgTWVldGluZ1Jvb21Sb3VuZGVkSWNvbiBmcm9tICdAbWF0ZXJpYWwtdWkvaWNvbnMvTWVldGluZ1Jvb21Sb3VuZGVkJztcclxuaW1wb3J0IFN0YXJSb3VuZGVkSWNvbiBmcm9tICdAbWF0ZXJpYWwtdWkvaWNvbnMvU3RhclJvdW5kZWQnO1xyXG5pbXBvcnQgUmVkZWVtUm91bmRlZEljb24gZnJvbSAnQG1hdGVyaWFsLXVpL2ljb25zL1JlZGVlbVJvdW5kZWQnO1xyXG5pbXBvcnQgTG9jYWxPZmZlclJvdW5kZWRJY29uIGZyb20gJ0BtYXRlcmlhbC11aS9pY29ucy9Mb2NhbE9mZmVyUm91bmRlZCc7XHJcbmltcG9ydCBBc3Nlc3NtZW50SWNvbiBmcm9tICdAbWF0ZXJpYWwtdWkvaWNvbnMvQXNzZXNzbWVudCc7XHJcbmltcG9ydCBDYXJkIGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlL0NhcmQnO1xyXG5pbXBvcnQgUGVyc29uSWNvbiBmcm9tICdAbWF0ZXJpYWwtdWkvaWNvbnMvUGVyc29uJztcclxuaW1wb3J0IHsgdXNlUm91dGVyIH0gZnJvbSAnbmV4dC9yb3V0ZXInXHJcbmltcG9ydCB7IHVzZVNlc3Npb24gfSBmcm9tICduZXh0LWF1dGgvY2xpZW50J1xyXG5cclxuZnVuY3Rpb24gTWFpbk1lbnUoKSB7XHJcbiAgICBjb25zdCBbIHNlc3Npb25dID0gdXNlU2Vzc2lvbigpXHJcbiAgICBjb25zdCBbdmFsdWUsIHNldFZhbHVlXSA9IFJlYWN0LnVzZVN0YXRlKCk7XHJcbiAgICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKVxyXG4gICAgY29uc3Qge3RhYn0gPSByb3V0ZXIucXVlcnlcclxuXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxDYXJkIHZhcmlhbnQ9XCJvdXRsaW5lZFwiPlxyXG4gICAgICAgIDxQYXBlciBzcXVhcmU+XHJcbiAgICAgICAgICA8VGFic1xyXG4gICAgICAgICAgICB2YWx1ZT17cGFyc2VJbnQodGFiID8gdGFiIDogJzAnKX1cclxuICAgICAgICAgICAgdGV4dENvbG9yPVwicHJpbWFyeVwiXHJcbiAgICAgICAgICAgIGluZGljYXRvckNvbG9yPVwicHJpbWFyeVwiXHJcbiAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJkaXNhYmxlZCB0YWJzIGV4YW1wbGVcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL3Byb2ZpbGVcIiBjbGFzc05hbWU9XCJob3Zlcjp0ZXh0LWluZGlnby02MDBcIj48VGFiIGljb249ezxQZXJzb25JY29uIC8+fSBsYWJlbD17XCJ3ZWxjb21lLCBcIiArIHNlc3Npb24/LnVzZXI/LmZ1bGxOYW1lfSBjbGFzc05hbWU9XCJ0ZXh0LWluZGlnby02MDBcIi8+PC9hPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL2FjY291bnQ/dGFiPTFcIiBjbGFzc05hbWU9XCJob3Zlcjp0ZXh0LWluZGlnby02MDBcIj48VGFiIGxhYmVsPVwiQWNjb3VudFwiIGljb249ezxQZW9wbGVPdXRsaW5lUm91bmRlZEljb24gLz59Lz48L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIvYm9va2luZz90YWI9MlwiIGNsYXNzTmFtZT1cImhvdmVyOnRleHQtaW5kaWdvLTYwMFwiPjxUYWIgbGFiZWw9XCJCb29raW5nXCIgaWNvbj17PFJvb21TZXJ2aWNlUm91bmRlZEljb24gLz59Lz48L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIvcm9vbT90YWI9M1wiIGNsYXNzTmFtZT1cImhvdmVyOnRleHQtaW5kaWdvLTYwMFwiPjxUYWIgbGFiZWw9XCJSb29tXCIgaWNvbj17PE1lZXRpbmdSb29tUm91bmRlZEljb24gLz59IC8+PC9hPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL2NvbWJvP3RhYj00XCIgY2xhc3NOYW1lPVwiaG92ZXI6dGV4dC1pbmRpZ28tNjAwXCI+PFRhYiBsYWJlbD1cIkNvbWJvXCIgaWNvbj17PExvY2FsT2ZmZXJSb3VuZGVkSWNvbiAvPn0vPjwvYT5cclxuICAgICAgICAgICAgPGEgaHJlZj1cIi9wcm9tb3Rpb24/dGFiPTVcIiBjbGFzc05hbWU9XCJob3Zlcjp0ZXh0LWluZGlnby02MDBcIj48VGFiIGxhYmVsPVwiUHJvbW90aW9uXCIgaWNvbj17PFJlZGVlbVJvdW5kZWRJY29uIC8+fS8+PC9hPlxyXG4gICAgICAgICAgICA8YSBocmVmPVwiL2V2ZW50P3RhYj02XCIgY2xhc3NOYW1lPVwiaG92ZXI6dGV4dC1pbmRpZ28tNjAwXCI+PFRhYiBsYWJlbD1cIkV2ZW50XCIgaWNvbj17PFN0YXJSb3VuZGVkSWNvbiAvPn0gLz48L2E+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCIvcmVwb3J0P3RhYj03XCIgY2xhc3NOYW1lPVwiaG92ZXI6dGV4dC1pbmRpZ28tNjAwXCI+PFRhYiBsYWJlbD1cIlJlcG9ydFwiIGljb249ezxBc3Nlc3NtZW50SWNvbiAvPn0gLz48L2E+XHJcbiAgICAgICAgICA8L1RhYnM+XHJcbiAgICAgICAgPC9QYXBlcj5cclxuICAgICAgICA8L0NhcmQ+XHJcbiAgICAgICk7XHJcblxyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBNYWluTWVudSIsImltcG9ydCB7IHVzZVN0YXRlIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBQZXRzUm91bmRlZEljb24gZnJvbSAnQG1hdGVyaWFsLXVpL2ljb25zL1BldHNSb3VuZGVkJ1xyXG5pbXBvcnQgeyBzaWduT3V0IH0gZnJvbSAnbmV4dC1hdXRoL2NsaWVudCdcclxuaW1wb3J0IExpbmsgZnJvbSAnQG1hdGVyaWFsLXVpL2NvcmUvTGluayc7XHJcbmltcG9ydCB7IEJ1dHRvbiB9IGZyb20gJ0BtYXRlcmlhbC11aS9jb3JlJztcclxuXHJcblxyXG5cclxuZnVuY3Rpb24gTmF2YmFyKCkge1xyXG4gIC8vIGxldCBbaXNTaG93TWVudSwgc2V0SXNTaG93TWVudV0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcblxyXG4gIGNvbnN0IGxvZ291dEhhbmRsZSA9IChlKSA9PiB7XHJcbiAgICBlLnByZXZlbnREZWZhdWx0KClcclxuICAgIHNpZ25PdXQoKVxyXG4gIH1cclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxuYXYgY2xhc3M9XCJmbGV4IGl0ZW1zLWNlbnRlciBqdXN0aWZ5LWJldHdlZW4gZmxleC13cmFwIGJnLWdyZWVuLTUwMCBweC02IHB5LTFcIj5cclxuICAgICAgPExpbmsgaHJlZj1cIi9cIiBjb2xvcj1cImluaGVyaXRcIiB1bmRlcmxpbmU9XCJub25lXCI+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImZsZXggaXRlbXMtY2VudGVyIGZsZXgtc2hyaW5rLTAgdGV4dC13aGl0ZSBtci02XCI+XHJcbiAgICAgICAgICA8UGV0c1JvdW5kZWRJY29uIGNsYXNzTmFtZT1cIm1yLTJcIi8+XHJcbiAgICAgICAgICA8c3BhbiBjbGFzcz1cImZvbnQtc2VtaWJvbGQgdGV4dC14bCB0cmFja2luZy10aWdodFwiPkNyYXp5IEhvdXNlPC9zcGFuPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICA8L0xpbms+XHJcbiAgICAgIDxkaXYgY2xhc3M9XCJibG9jayBsZzpoaWRkZW5cIj5cclxuICAgICAgICA8YnV0dG9uIGNsYXNzPVwiZmxleCBpdGVtcy1jZW50ZXIgcHgtMyBweS0yIGJvcmRlciByb3VuZGVkIHRleHQtZ3JlZW4tMjAwIGJvcmRlci10ZWFsLTQwMCBob3Zlcjp0ZXh0LXdoaXRlIGhvdmVyOmJvcmRlci13aGl0ZVwiPlxyXG4gICAgICAgICAgPHN2Z1xyXG4gICAgICAgICAgICBjbGFzcz1cImZpbGwtY3VycmVudCBoLTMgdy0zXCJcclxuICAgICAgICAgICAgdmlld0JveD1cIjAgMCAyMCAyMFwiXHJcbiAgICAgICAgICAgIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8dGl0bGU+TWVudTwvdGl0bGU+XHJcbiAgICAgICAgICAgIDxwYXRoIGQ9XCJNMCAzaDIwdjJIMFYzem0wIDZoMjB2MkgwVjl6bTAgNmgyMHYySDB2LTJ6XCIgLz5cclxuICAgICAgICAgIDwvc3ZnPlxyXG4gICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICA8L2Rpdj5cclxuICAgICAgey8qIDxkaXYgY2xhc3M9XCJ3LWZ1bGwgYmxvY2sgZmxleC1ncm93IGxnOmZsZXggbGc6aXRlbXMtY2VudGVyIGxnOnctYXV0b1wiPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJmbGV4LWdyb3dcIj48L2Rpdj4gKi99XHJcbiAgICAgICAgey8qIDxkaXYgY2xhc3M9XCJhYnNvbHV0ZSBpbnNldC15LTAgcmlnaHQtMCBmbGV4IGl0ZW1zLWNlbnRlciBwci0yIHNtOnN0YXRpYyBzbTppbnNldC1hdXRvIHNtOm1sLTYgc206cHItMFwiPiAqL31cclxuICAgICAgICAgIHsvKiA8YnV0dG9uXHJcbiAgICAgICAgICAgIGNsYXNzPVwicC0xIGJvcmRlci0yIGJvcmRlci10cmFuc3BhcmVudCB0ZXh0LXdoaXRlIHJvdW5kZWQtZnVsbCBob3Zlcjp0ZXh0LXdoaXRlIGZvY3VzOm91dGxpbmUtbm9uZSBmb2N1czp0ZXh0LXdoaXRlIGZvY3VzOmJnLWdyYXktNzAwIHRyYW5zaXRpb24gZHVyYXRpb24tMTUwIGVhc2UtaW4tb3V0XCJcclxuICAgICAgICAgICAgYXJpYS1sYWJlbD1cIk5vdGlmaWNhdGlvbnNcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8c3ZnXHJcbiAgICAgICAgICAgICAgY2xhc3M9XCJoLTYgdy02XCJcclxuICAgICAgICAgICAgICB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXCJcclxuICAgICAgICAgICAgICBmaWxsPVwibm9uZVwiXHJcbiAgICAgICAgICAgICAgdmlld0JveD1cIjAgMCAyNCAyNFwiXHJcbiAgICAgICAgICAgICAgc3Ryb2tlPVwiY3VycmVudENvbG9yXCJcclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgIDxwYXRoXHJcbiAgICAgICAgICAgICAgICBzdHJva2UtbGluZWNhcD1cInJvdW5kXCJcclxuICAgICAgICAgICAgICAgIHN0cm9rZS1saW5lam9pbj1cInJvdW5kXCJcclxuICAgICAgICAgICAgICAgIHN0cm9rZS13aWR0aD1cIjJcIlxyXG4gICAgICAgICAgICAgICAgZD1cIk0xNSAxN2g1bC0xLjQwNS0xLjQwNUEyLjAzMiAyLjAzMiAwIDAxMTggMTQuMTU4VjExYTYuMDAyIDYuMDAyIDAgMDAtNC01LjY1OVY1YTIgMiAwIDEwLTQgMHYuMzQxQzcuNjcgNi4xNjUgNiA4LjM4OCA2IDExdjMuMTU5YzAgLjUzOC0uMjE0IDEuMDU1LS41OTUgMS40MzZMNCAxN2g1bTYgMHYxYTMgMyAwIDExLTYgMHYtMW02IDBIOVwiXHJcbiAgICAgICAgICAgICAgLz5cclxuICAgICAgICAgICAgPC9zdmc+XHJcbiAgICAgICAgICA8L2J1dHRvbj4gKi99XHJcblxyXG4gICAgICAgICAgey8qIDxkaXYgY2xhc3M9XCJtbC0zIHJlbGF0aXZlXCI+ICovfVxyXG4gICAgICAgICAgICB7LyogPGRpdj4gKi99XHJcbiAgICAgICAgICAgIHsvKiA8QnV0dG9uIGNvbG9yPVwid2hpdGVcIiAgb25DbGljaz17KGUpID0+IGxvZ291dEhhbmRsZShlKX0+U2lnbiBvdXQ8L0J1dHRvbj4gKi99XHJcbiAgICAgICAgICAgIHsvKiA8YVxyXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJibG9jayBweC00IHB5LTIgdGV4dC1tZCBsZWFkaW5nLTUgdGV4dC1ncmF5LTcwMCAgaG92ZXI6dGV4dC13aGl0ZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgcm9sZT1cIm1lbnVpdGVtXCJcclxuICAgICAgICAgICAgICAgICAgICBvbkNsaWNrPXsoZSkgPT4gbG9nb3V0SGFuZGxlKGUpfVxyXG4gICAgICAgICAgICAgICAgICA+XHJcbiAgICAgICAgICAgICAgICAgIFNpZ24gb3V0ICBcclxuICAgICAgICAgICAgPC9hPiAqL31cclxuICAgICAgICAgICAgICB7LyogPGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJmbGV4IHRleHQtc20gYm9yZGVyLTIgYm9yZGVyLXRyYW5zcGFyZW50IHJvdW5kZWQtZnVsbCBob3Zlcjpib3JkZXItd2hpdGUgZm9jdXM6b3V0bGluZS1ub25lIGZvY3VzOmJvcmRlci13aGl0ZSB0cmFuc2l0aW9uIGR1cmF0aW9uLTE1MCBlYXNlLWluLW91dFwiXHJcbiAgICAgICAgICAgICAgICBpZD1cInVzZXItbWVudVwiXHJcbiAgICAgICAgICAgICAgICBvbkZvY3VzPXsoKSA9PiBzZXRJc1Nob3dNZW51KHRydWUpfVxyXG4gICAgICAgICAgICAgICAgLy8gb25CbHVyPXsoKSA9PiBzZXRJc1Nob3dNZW51KGZhbHNlKX1cclxuICAgICAgICAgICAgICAgIGFyaWEtbGFiZWw9XCJVc2VyIG1lbnVcIlxyXG4gICAgICAgICAgICAgICAgYXJpYS1oYXNwb3B1cD1cInRydWVcIlxyXG4gICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgIDxpbWdcclxuICAgICAgICAgICAgICAgICAgY2xhc3M9XCJoLTggdy04IHJvdW5kZWQtZnVsbFwiXHJcbiAgICAgICAgICAgICAgICAgIHNyYz1cImh0dHBzOi8vaW1hZ2VzLnVuc3BsYXNoLmNvbS9waG90by0xNDcyMDk5NjQ1Nzg1LTU2NThhYmY0ZmY0ZT9peGxpYj1yYi0xLjIuMSZpeGlkPWV5SmhjSEJmYVdRaU9qRXlNRGQ5JmF1dG89Zm9ybWF0JmZpdD1mYWNlYXJlYSZmYWNlcGFkPTImdz0yNTYmaD0yNTYmcT04MFwiXHJcbiAgICAgICAgICAgICAgICAgIGFsdD1cIlwiXHJcbiAgICAgICAgICAgICAgICAvPlxyXG4gICAgICAgICAgICAgIDwvYnV0dG9uPiAqL31cclxuICAgICAgICAgICAgey8qIDwvZGl2PiAqL31cclxuXHJcbiAgICAgICAgICAgIHsvKiB7aXNTaG93TWVudSAmJiAoXHJcbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm9yaWdpbi10b3AtcmlnaHQgYWJzb2x1dGUgcmlnaHQtMCAgdy00OCByb3VuZGVkLW1kIHNoYWRvdy1sZyB6LTEwXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwicHktMSByb3VuZGVkLW1kIGJnLXdoaXRlIHNoYWRvdy14c1wiXHJcbiAgICAgICAgICAgICAgICAgIHJvbGU9XCJtZW51XCJcclxuICAgICAgICAgICAgICAgICAgYXJpYS1vcmllbnRhdGlvbj1cInZlcnRpY2FsXCJcclxuICAgICAgICAgICAgICAgICAgYXJpYS1sYWJlbGxlZGJ5PVwidXNlci1tZW51XCJcclxuICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgPGFcclxuICAgICAgICAgICAgICAgICAgICBocmVmPVwiI1wiXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJibG9jayBweC00IHB5LTIgdGV4dC1zbSBsZWFkaW5nLTUgdGV4dC1ncmF5LTcwMCBob3Zlcjp0ZXh0LXdoaXRlIGhvdmVyOmJnLWdyZWVuLTMwMCBmb2N1czpvdXRsaW5lLW5vbmUgZm9jdXM6YmctZ3JheS0xMDAgdHJhbnNpdGlvbiBkdXJhdGlvbi0xNTAgZWFzZS1pbi1vdXRcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU9XCJtZW51aXRlbVwiXHJcbiAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICBZb3VyIFByb2ZpbGVcclxuICAgICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICAgICAgICA8YVxyXG4gICAgICAgICAgICAgICAgICAgIGhyZWY9XCIjXCJcclxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImJsb2NrIHB4LTQgcHktMiB0ZXh0LXNtIGxlYWRpbmctNSB0ZXh0LWdyYXktNzAwICBob3Zlcjp0ZXh0LXdoaXRlIGhvdmVyOmJnLWdyZWVuLTMwMCBmb2N1czpvdXRsaW5lLW5vbmUgZm9jdXM6YmctZ3JheS0xMDAgdHJhbnNpdGlvbiBkdXJhdGlvbi0xNTAgZWFzZS1pbi1vdXRcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU9XCJtZW51aXRlbVwiXHJcbiAgICAgICAgICAgICAgICAgID5cclxuICAgICAgICAgICAgICAgICAgICBTZXR0aW5nc1xyXG4gICAgICAgICAgICAgICAgICA8L2E+XHJcbiAgICAgICAgICAgICAgICAgIDxhXHJcbiAgICAgICAgICAgICAgICAgICAgaHJlZj1cIiNcIlxyXG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImJsb2NrIHB4LTQgcHktMiB0ZXh0LXNtIGxlYWRpbmctNSB0ZXh0LWdyYXktNzAwICBob3Zlcjp0ZXh0LXdoaXRlIGhvdmVyOmJnLWdyZWVuLTMwMCBmb2N1czpvdXRsaW5lLW5vbmUgZm9jdXM6YmctZ3JheS0xMDAgdHJhbnNpdGlvbiBkdXJhdGlvbi0xNTAgZWFzZS1pbi1vdXRcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU9XCJtZW51aXRlbVwiXHJcbiAgICAgICAgICAgICAgICAgICAgb25DbGljaz17KGUpID0+IGxvZ291dEhhbmRsZShlKX1cclxuICAgICAgICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICAgICAgICAgIFNpZ24gb3V0XHJcbiAgICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICApfSAqL31cclxuICAgICAgICAgIHsvKiA8L2Rpdj4gKi99XHJcbiAgICAgICAgey8qIDwvZGl2PlxyXG4gICAgICA8L2Rpdj4gKi99XHJcbiAgICA8L25hdj5cclxuICApO1xyXG59XHJcblxyXG5leHBvcnQgZGVmYXVsdCBOYXZiYXI7IiwiaW1wb3J0IFJlYWN0LCB7IFB1cmVDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IEJhckNoYXJ0LCBCYXIsIFhBeGlzLCBZQXhpcywgQ2FydGVzaWFuR3JpZCwgVG9vbHRpcCwgTGVnZW5kLCBSZXNwb25zaXZlQ29udGFpbmVyIH0gZnJvbSAncmVjaGFydHMnO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gQmFyQ2hhcnRSZXBvcnRlcih7ZGF0YSwgY29sb3IsIHRpdGxlfSl7XHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxSZXNwb25zaXZlQ29udGFpbmVyIHdpZHRoPXs1MDB9IGhlaWdodD17NDAwfT5cclxuICAgICAgICAgIDxCYXJDaGFydFxyXG4gICAgICAgICAgICB3aWR0aD17NTAwfVxyXG4gICAgICAgICAgICBoZWlnaHQ9ezMwMH1cclxuICAgICAgICAgICAgZGF0YT17ZGF0YX1cclxuICAgICAgICAgICAgbWFyZ2luPXt7XHJcbiAgICAgICAgICAgICAgdG9wOiA1LFxyXG4gICAgICAgICAgICAgIHJpZ2h0OiAzMCxcclxuICAgICAgICAgICAgICBsZWZ0OiAyMCxcclxuICAgICAgICAgICAgICBib3R0b206IDUsXHJcbiAgICAgICAgICAgIH19XHJcbiAgICAgICAgICAgIGJhclNpemU9ezIwfVxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8WEF4aXMgZGF0YUtleT1cIm5hbWVcIiBzY2FsZT1cInBvaW50XCIgcGFkZGluZz17eyBsZWZ0OiAxMCwgcmlnaHQ6IDEwIH19IGxhYmVsLz5cclxuICAgICAgICAgICAgPFlBeGlzIC8+XHJcbiAgICAgICAgICAgIDxUb29sdGlwIC8+XHJcbiAgICAgICAgICAgIDxMZWdlbmQgY29udGVudD17dGl0bGV9Lz5cclxuICAgICAgICAgICAgPENhcnRlc2lhbkdyaWQgc3Ryb2tlRGFzaGFycmF5PVwiMyAzXCIgLz5cclxuICAgICAgICAgICAgPEJhciBkYXRhS2V5PVwidmFsdWVcIiBmaWxsPXtjb2xvcn0gYmFja2dyb3VuZD17eyBmaWxsOiAnI2VlZScgfX0gLz5cclxuICAgICAgICAgIDwvQmFyQ2hhcnQ+XHJcbiAgICAgICAgPC9SZXNwb25zaXZlQ29udGFpbmVyPlxyXG4gICAgICApO1xyXG59IiwiaW1wb3J0IHsgTGluZUNoYXJ0LCBMaW5lLCBYQXhpcywgWUF4aXMsIENhcnRlc2lhbkdyaWQsIFRvb2x0aXAsIExlZ2VuZCwgUmVzcG9uc2l2ZUNvbnRhaW5lciB9IGZyb20gJ3JlY2hhcnRzJztcclxuXHJcbmNvbnN0IGRhdGEgPSBbXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6ICdUMScsXHJcbiAgICAgIHV2OiA0MDAwLFxyXG4gICAgICBwdjogMjQwMCxcclxuICAgICAgYW10OiAyNDAwLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgbmFtZTogJ1QyJyxcclxuICAgICAgdXY6IDMwMDAsXHJcbiAgICAgIHB2OiAxMzk4LFxyXG4gICAgICBhbXQ6IDIyMTAsXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBuYW1lOiAnVDMnLFxyXG4gICAgICB1djogMjAwMCxcclxuICAgICAgcHY6IDk4MDAsXHJcbiAgICAgIGFtdDogMjI5MCxcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6ICdUNCcsXHJcbiAgICAgIHV2OiAyNzgwLFxyXG4gICAgICBwdjogMzkwOCxcclxuICAgICAgYW10OiAyMDAwLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgbmFtZTogJ1Q1JyxcclxuICAgICAgdXY6IDE4OTAsXHJcbiAgICAgIHB2OiA0ODAwLFxyXG4gICAgICBhbXQ6IDIxODEsXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBuYW1lOiAnVDYnLFxyXG4gICAgICB1djogMjM5MCxcclxuICAgICAgcHY6IDM4MDAsXHJcbiAgICAgIGFtdDogMjUwMCxcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6ICdUNycsXHJcbiAgICAgIHV2OiAzNDkwLFxyXG4gICAgICBwdjogNDMwMCxcclxuICAgICAgYW10OiAyMTAwLFxyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgbmFtZTogJ1Q4JyxcclxuICAgICAgdXY6IDE4OTAsXHJcbiAgICAgIHB2OiA0ODAwLFxyXG4gICAgICBhbXQ6IDIxODEsXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICBuYW1lOiAnVDknLFxyXG4gICAgICB1djogMjM5MCxcclxuICAgICAgcHY6IDM4MDAsXHJcbiAgICAgIGFtdDogMjUwMCxcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIG5hbWU6ICdUMTAnLFxyXG4gICAgICB1djogMzQ5MCxcclxuICAgICAgcHY6IDQzMDAsXHJcbiAgICAgIGFtdDogMjEwMCxcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgICAgbmFtZTogJ1QxMScsXHJcbiAgICAgICAgdXY6IDIzOTAsXHJcbiAgICAgICAgcHY6IDM4MDAsXHJcbiAgICAgICAgYW10OiAyNTAwLFxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgbmFtZTogJ1QxMicsXHJcbiAgICAgICAgdXY6IDM0OTAsXHJcbiAgICAgICAgcHY6IDQzMDAsXHJcbiAgICAgICAgYW10OiAyMTAwLFxyXG4gICAgICB9LFxyXG4gIF07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBMaW5lQ2hhcnRSZXBvcnRlcih7ZGF0YX0pe1xyXG4gICAgXHJcbiAgICByZXR1cm4gKFxyXG4gICAgICAgIDxSZXNwb25zaXZlQ29udGFpbmVyIHdpZHRoPXs1MDB9IGhlaWdodD17NDAwfT5cclxuICAgICAgICAgICAgPExpbmVDaGFydFxyXG4gICAgICAgICAgICB3aWR0aD17MzAwfVxyXG4gICAgICAgICAgICBoZWlnaHQ9ezMwMH1cclxuICAgICAgICAgICAgZGF0YT17ZGF0YX1cclxuICAgICAgICAgICAgbWFyZ2luPXt7XHJcbiAgICAgICAgICAgICAgICB0b3A6IDUsXHJcbiAgICAgICAgICAgICAgICByaWdodDogMzAsXHJcbiAgICAgICAgICAgICAgICBsZWZ0OiAyMCxcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogNSxcclxuICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgPlxyXG4gICAgICAgICAgICA8Q2FydGVzaWFuR3JpZCBzdHJva2VEYXNoYXJyYXk9XCIzIDNcIiAvPlxyXG4gICAgICAgICAgICA8WEF4aXMgZGF0YUtleT1cInRvdGFsXCIvPlxyXG4gICAgICAgICAgICA8WUF4aXMgZGF0YUtleT0gezIwMjF9IC8+XHJcbiAgICAgICAgICAgIDxUb29sdGlwIC8+XHJcbiAgICAgICAgICAgIDxMZWdlbmQgLz5cclxuICAgICAgICAgICAge2RhdGEubWFwKChyb29tKT0+IDxMaW5lIHR5cGU9XCJtb25vdG9uZVwiIGRhdGFLZXk9eydyb29tICcgKyByb29tLnJvb21JZH0gc3Ryb2tlPVwiIzgyY2E5ZFwiIC8+KX1cclxuICAgICAgICAgICAgey8qIDxMaW5lIHR5cGU9XCJtb25vdG9uZVwiIGRhdGFLZXk9XCJyb29tSWRcIiBuYW1lS2V5PVwicHNkYVwiIHN0cm9rZT1cIiM4ODg0ZDhcIiBhY3RpdmVEb3Q9e3sgcjogOCB9fSAvPiAqL31cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIDwvTGluZUNoYXJ0PlxyXG4gICAgICAgIDwvUmVzcG9uc2l2ZUNvbnRhaW5lcj4gICAgICAgICAgICAgIFxyXG4gICAgKVxyXG59IiwiaW1wb3J0IFJlYWN0LCB7IFB1cmVDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XHJcbmltcG9ydCB7IFBpZUNoYXJ0LCBQaWUsIFRvb2x0aXAsIENlbGwsIFJlc3BvbnNpdmVDb250YWluZXIsIExlZ2VuZCB9IGZyb20gJ3JlY2hhcnRzJztcclxuXHJcbi8vIGNvbnN0IGRhdGEgPSBbXHJcbi8vICAgeyBuYW1lOiAnR3JvdXAgQScsIHZhbHVlOiA0MDAgfSxcclxuLy8gICB7IG5hbWU6ICdHcm91cCBCJywgdmFsdWU6IDMwMCB9LFxyXG4vLyAgIHsgbmFtZTogJ0dyb3VwIEMnLCB2YWx1ZTogMzAwIH0sXHJcbi8vICAgeyBuYW1lOiAnR3JvdXAgRCcsIHZhbHVlOiAyMDAgfSxcclxuLy8gXTtcclxuXHJcbmNvbnN0IENPTE9SUyA9IFsnIzAwODhGRScsICcjMDBDNDlGJywgJyNGRkJCMjgnLCAnI0ZGODA0MiddO1xyXG5cclxuY29uc3QgUkFESUFOID0gTWF0aC5QSSAvIDE4MDtcclxuY29uc3QgcmVuZGVyQ3VzdG9taXplZExhYmVsID0gKHsgY3gsIGN5LCBtaWRBbmdsZSwgaW5uZXJSYWRpdXMsIG91dGVyUmFkaXVzLCBwZXJjZW50LCBpbmRleCB9KSA9PiB7XHJcbiAgY29uc3QgcmFkaXVzID0gaW5uZXJSYWRpdXMgKyAob3V0ZXJSYWRpdXMgLSBpbm5lclJhZGl1cykgKiAwLjU7XHJcbiAgY29uc3QgeCA9IGN4ICsgcmFkaXVzICogTWF0aC5jb3MoLW1pZEFuZ2xlICogUkFESUFOKTtcclxuICBjb25zdCB5ID0gY3kgKyByYWRpdXMgKiBNYXRoLnNpbigtbWlkQW5nbGUgKiBSQURJQU4pO1xyXG5cclxuICByZXR1cm4gKFxyXG4gICAgPHRleHQgeD17eH0geT17eX0gZmlsbD1cIndoaXRlXCIgdGV4dEFuY2hvcj17eCA+IGN4ID8gJ3N0YXJ0JyA6ICdlbmQnfSBkb21pbmFudEJhc2VsaW5lPVwiY2VudHJhbFwiPlxyXG4gICAgICB7YCR7KHBlcmNlbnQgKiAxMDApLnRvRml4ZWQoMCl9JWB9XHJcbiAgICA8L3RleHQ+XHJcbiAgKTtcclxufTtcclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gUGllQ2hhcnRSZXBvcnRlcih7ZGF0YX0pe1xyXG4gICAgcmV0dXJuIChcclxuXHJcbiAgICAgICAgPFJlc3BvbnNpdmVDb250YWluZXIgd2lkdGg9ezQwMH0gaGVpZ2h0PXs0MDB9PlxyXG4gICAgICAgIDxQaWVDaGFydCB3aWR0aD17NzMwfSBoZWlnaHQ9ezQwMH0+XHJcbiAgICAgICAgICA8UGllXHJcbiAgICAgICAgICAgIGRhdGE9e2RhdGF9XHJcbiAgICAgICAgICAgIGN4PVwiNTAlXCJcclxuICAgICAgICAgICAgY3k9XCI1MCVcIlxyXG4gICAgICAgICAgICBsYWJlbExpbmU9e2ZhbHNlfVxyXG4gICAgICAgICAgICAvLyBsYWJlbD17cmVuZGVyQ3VzdG9taXplZExhYmVsfVxyXG4gICAgICAgICAgICBvdXRlclJhZGl1cz17ODB9XHJcbiAgICAgICAgICAgIGZpbGw9XCIjODg4NGQ4XCJcclxuICAgICAgICAgICAgZGF0YUtleT1cInZhbHVlXCJcclxuICAgICAgICAgICAgbmFtZUtleT1cIm5hbWVcIlxyXG4gICAgICAgICAgICBsYWJlbFxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICB7ZGF0YS5tYXAoKGVudHJ5LCBpbmRleCkgPT4gKFxyXG4gICAgICAgICAgICAgIDxDZWxsIGtleT17YGNlbGwtJHtpbmRleH1gfSBmaWxsPXtDT0xPUlNbaW5kZXggJSBDT0xPUlMubGVuZ3RoXX0gLz5cclxuICAgICAgICAgICAgKSl9XHJcbiAgICAgICAgICA8L1BpZT5cclxuICAgICAgICAgIDxUb29sdGlwIC8+XHJcbiAgICAgICAgICA8TGVnZW5kIHZlcnRpY2FsQWxpZ249XCJ0b3BcIiBoZWlnaHQ9ezM2fS8+XHJcbiAgICAgICAgPC9QaWVDaGFydD5cclxuICAgICAgICBcclxuICAgICAgPC9SZXNwb25zaXZlQ29udGFpbmVyPiBcclxuICAgICAgICAgICAgICBcclxuICAgIClcclxufSIsImltcG9ydCBIZWFkIGZyb20gJ25leHQvaGVhZCdcbmltcG9ydCBOYXZCYXIgZnJvbSAnLi4vY29tcG9uZW50cy9tYXN0ZXIvbmF2YmFyJ1xuaW1wb3J0IEJyZWFkQ3J1bWIgZnJvbSAnLi4vY29tcG9uZW50cy9tYXN0ZXIvYnJlYWRjcnVtYnMnXG5pbXBvcnQgTWFpbk1lbnUgZnJvbSAnLi4vY29tcG9uZW50cy9tYXN0ZXIvbWFpbm1lbnUnXG5pbXBvcnQgRnVuY3Rpb25OYXYgZnJvbSAnLi4vY29tcG9uZW50cy9tYXN0ZXIvZnVuY3Rpb25uYXYnXG4vLyBpbXBvcnQgand0IGZyb20gJ25leHQtYXV0aC9qd3QnXG5pbXBvcnQgeyB1c2VTZXNzaW9uLCBnZXRTZXNzaW9uIH0gZnJvbSAnbmV4dC1hdXRoL2NsaWVudCdcbmltcG9ydCBMaW5lQ2hhcnRSZXBvcnRlciBmcm9tICcuLi9jb21wb25lbnRzL3JlcG9ydC9MaW5lQ2hhcnRSZXBvcnRlcidcbmltcG9ydCBQaWVDaGFydFJlcG9ydGVyIGZyb20gJy4uL2NvbXBvbmVudHMvcmVwb3J0L1BpZUNoYXJ0UmVwb3J0ZXInXG5pbXBvcnQgQmFyQ2hhcnRSZXBvcnRlciBmcm9tICcuLi9jb21wb25lbnRzL3JlcG9ydC9CYXJDaGFydFJlcG9ydGVyJ1xuaW1wb3J0IHVzZVNXUiBmcm9tICdzd3InXG5cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSG9tZSgpIHtcbiAgY29uc3Qgc2Vzc2lvbiA9IHVzZVNlc3Npb24oKVxuICBjb25zdCB7IGRhdGE6IHJldmVudWVCeVllYXIsZXJyb3I6IGVyciB9ID0gdXNlU1dSKFsnaHR0cDovL2xvY2FsaG9zdDo4MDgwL2FwaS9ib29raW5nL3JlcG9ydC95ZWFyJywgc2Vzc2lvbi5hY2Nlc3NUb2tlbl0pXG4gIGNvbnN0IHsgZGF0YTogcmV2ZW51ZUJ5TW9udGgsZXJyb3I6IGVycnIgfSA9IHVzZVNXUihbJ2h0dHA6Ly9sb2NhbGhvc3Q6ODA4MC9hcGkvYm9va2luZy9yZXBvcnQvbW9udGgnLCBzZXNzaW9uLmFjY2Vzc1Rva2VuXSlcbiAgY29uc3QgY3VyckRhdGUgPSBuZXcgRGF0ZSgpXG4gIHJldHVybiAoXG4gICAgPGRpdiBjbGFzc05hbWU9XCJcIj5cbiAgICAgIDxIZWFkPlxuICAgICAgICA8dGl0bGU+Q3JhenkgV2ViPC90aXRsZT5cbiAgICAgICAgPGxpbmsgcmVsPVwiaWNvblwiIGhyZWY9XCIvZmF2aWNvbi5pY29cIiAvPlxuICAgICAgPC9IZWFkPlxuXG4gICAgICA8bWFpbiBjbGFzc05hbWU9XCJoLXNjcmVlblwiPlxuICAgICAgICA8TmF2QmFyIC8+XG4gICAgICAgIDxCcmVhZENydW1iIGNsYXNzTmFtZT1cImJnLWdyYXktNDAwXCIvPlxuICAgICAgICA8TWFpbk1lbnUgLz5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtdC00IGZsZXgganVzdGlmeS1jZW50ZXJcIj5cbiAgICAgICAgICB7LyogPExpbmVDaGFydFJlcG9ydGVyIGRhdGE9e3JldmVudWVCeVllYXJ9Lz4gKi99XG4gICAgICAgICAgPEJhckNoYXJ0UmVwb3J0ZXIgZGF0YT17cmV2ZW51ZUJ5TW9udGh9IGNvbG9yPScjMDA4OEZFJyB0aXRsZT17KCkgPT4gPGg0IGNsYXNzTmFtZT1cInRleHQtY2VudGVyIHRleHQtYmx1ZS01MDAgZm9udC1ib2xkIHRleHQtbGdcIj5Eb2FuaCB0aHUgdGjDoW5nIHtjdXJyRGF0ZS5nZXRVVENNb250aCgpfTwvaDQ+fS8+XG4gICAgICAgICAgPEJhckNoYXJ0UmVwb3J0ZXIgIGRhdGE9e3JldmVudWVCeVllYXJ9IGNvbG9yPScjRkY4MDQyJyB0aXRsZT17KCkgPT4gPGg0IGNsYXNzTmFtZT1cInRleHQtY2VudGVyIHRleHQteWVsbG93LTUwMCBmb250LWJvbGQgdGV4dC1sZ1wiPkRvYW5oIHRodSBuxINtIHtjdXJyRGF0ZS5nZXRGdWxsWWVhcigpfTwvaDQ+fS8+ICBcbiAgICAgICAgPC9kaXY+XG5cbiAgICAgIDwvbWFpbj5cbiAgICAgIFxuICAgICAgXG4gICAgPC9kaXY+XG4gICAgXG4gIClcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFNlcnZlclNpZGVQcm9wcyhjb250ZXh0KSB7XG4gIGNvbnN0IF9zZXNzaW9uID0gYXdhaXQgZ2V0U2Vzc2lvbihjb250ZXh0KVxuICBpZiAoIV9zZXNzaW9uKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHJlZGlyZWN0OiB7XG4gICAgICAgIGRlc3RpbmF0aW9uOiAnL2F1dGgvc2lnbmluJyxcbiAgICAgICAgcGVybWFuZW50OiBmYWxzZSxcbiAgICAgIH0sXG4gICAgfVxuICB9XG4gIHJldHVybiB7XG4gICAgcHJvcHM6IHtcbiAgICAgIHNlc3Npb246IF9zZXNzaW9uXG4gICAgfVxuICB9XG59XG5cbiIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9jb3JlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9jb3JlL0JyZWFkY3J1bWJzXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9jb3JlL0NhcmRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2NvcmUvTGlua1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9QYXBlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9UYWJcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2NvcmUvVGFic1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvY29yZS9UeXBvZ3JhcGh5XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9jb3JlL3N0eWxlc1wiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvaWNvbnMvQXNzZXNzbWVudFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvaWNvbnMvTG9jYWxPZmZlclJvdW5kZWRcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiQG1hdGVyaWFsLXVpL2ljb25zL01lZXRpbmdSb29tUm91bmRlZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvaWNvbnMvTmF2aWdhdGVOZXh0XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9pY29ucy9QZW9wbGVPdXRsaW5lUm91bmRlZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvaWNvbnMvUGVyc29uXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9pY29ucy9QZXRzUm91bmRlZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvaWNvbnMvUmVkZWVtUm91bmRlZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJAbWF0ZXJpYWwtdWkvaWNvbnMvUm9vbVNlcnZpY2VSb3VuZGVkXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIkBtYXRlcmlhbC11aS9pY29ucy9TdGFyUm91bmRlZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0LWF1dGgvY2xpZW50XCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvaGVhZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L3JvdXRlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwicmVjaGFydHNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwic3dyXCIpOyJdLCJzb3VyY2VSb290IjoiIn0=